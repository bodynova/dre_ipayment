<?php

$conf = \OxidEsales\Eshop\Core\Registry::getConfig();
$sModulesDir = $conf->getShopMainUrl().'modules/bender/dre_ipayment/';

$sMetadataVersion = '2.0';
/**
 * Module information
 */
$aModule = array(
    'id'           => 'dre_ipayment',
    'title'        => [
        'de' => '<img src="'.$sModulesDir.'out/img/favicon.ico" height="20px" title="" />odynova2ipayment',
        'en' => '<img src="'.$sModulesDir.'out/img/favicon.ico" height="20px" title="" />odynova2ipayment'
    ],
    'description'  => [
        'de' => 'Bodynova 2 Ipayment Connector',
        'en' => 'Bodynova 2 Ipayment Connector'
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'      => '2.0.0',
    'author'       => 'Bodynova GmbH',
    'email'        => 'support@bodynova.de',
    'url'          => 'https://bodynova.de',
    'extend'       => array(

        \OxidEsales\Eshop\Core\Model\BaseModel::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log::class,
        \OxidEsales\Eshop\Core\Model\ListModel::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log_List::class,
        \OxidEsales\Eshop\Core\Base::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment::class,
        \OxidEsales\Eshop\Application\Model\Payment::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Payment::class,
        \OxidEsales\Eshop\Application\Model\PaymentGateway::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_PaymentGateway::class,
        \OxidEsales\Eshop\Application\Model\Order::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Order::class,
        \OxidEsales\Eshop\Application\Component\UtilsComponent::class => \Bender\dre_Ipayment\Application\Component\dre_Ipayment_UtilsComponent::class,


        //'dre_ipayment_utilscomponent' => \Bender\dre_Ipayment\Application\Component\dre_Ipayment_UtilsComponent::class,

        //'dre_ipayment_payment' => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Payment::class,
        //'dre_ipayment_log' => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log::class,
        //'dre_ipayment_log_list' => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log_List::class,


        // \OxidEsales\Eshop\Application\Component\UtilsComponent
        //'oxcmp_utils' => 'dre_ipayment/dre_ipayment_utilsComponent',
        // try neu: \OxidEsales\Eshop\Application\Component\UtilsComponent::class => \Bender\dre_Ipayment\Application\Component\dre_Ipayment_UtilsComponent::class,

        //\OxidEsales\Eshop\Application\Model\Order
        //'oxorder' => 'dre_ipayment/dre_ipayment_order',
        // neu test: \OxidEsales\Eshop\Application\Model\Order::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Order::class,

        //\OxidEsales\Eshop\Application\Model\Payment
        //'oxpayment' => 'dre_ipayment/dre_ipayment_payment',
        // neu test:

        //\OxidEsales\Eshop\Application\Model\PaymentGateway
        //'oxpaymentgateway' => 'dre_ipayment/dre_ipayment_paymentGateway',
        // neu test : \OxidEsales\Eshop\Application\Model\PaymentGateway::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_PaymentGateway::class,

        //\OxidEsales\Eshop\Application\Controller\PaymentController
        //'payment' => 'dre_ipayment/dre_ipayment_payment',
        // try aus: \OxidEsales\Eshop\Core\Model\BaseModel::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Helper::class,
        // Models Verlaufen:
        // az_ipayment => \OxidEsales\Eshop\Core\Base
        // try aus: \OxidEsales\Eshop\Core\Base::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment::class,
        // az_ipayment_log => \OxidEsales\Eshop\Core\Base

        // neu try out : \OxidEsales\Eshop\Core\Model\BaseModel::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log::class,
        // az_ipayment_log_list => \OxidEsales\Eshop\Core\Model\ListModel
        // try aus : \OxidEsales\Eshop\Core\Model\ListModel::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log_List::class,
        // \OxidEsales\Eshop\Core\Model\ListModel::class => \Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log_List::class,
        //neu
        //\OxidEsales\Eshop\Application\Controller\Admin\AdminController::class => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn::class,

    ),
    'controllers' => [
        'dre_ipayment_dyn' => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn::class,
        'dre_ipayment_dyn_list' => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn_List::class,
        'dre_ipayment_dyn_main' => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn_Main::class,
        'dre_ipayment_dyn_config' => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn_Config::class,
        'dre_ipayment_dyn_log' => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn_Log::class,
        'dre_ipayment_admindetailscontroller' => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_AdminDetailsController::class,
        'dre_ipayment_ordercontroller' => \Bender\dre_Ipayment\Application\Controller\dre_Ipayment_OrderController::class,
        'dre_ipayment_paymentcontroller' => \Bender\dre_Ipayment\Application\Controller\dre_Ipayment_PaymentController::class,
        'dre_ipayment_redirect3dsecure' => \Bender\dre_Ipayment\Application\Controller\dre_Ipayment_Redirect3dSecure::class,

        // try neu: \OxidEsales\Eshop\Application\Controller\OrderController::class => \Bender\dre_Ipayment\Application\Controller\dre_Ipayment_OrderController::class,
        // \OxidEsales\Eshop\Application\Controller\PaymentController

        // try neu: \OxidEsales\Eshop\Application\Controller\PaymentController::class => \Bender\dre_Ipayment\Application\Controller\dre_Ipayment_PaymentController::class,
        //\OxidEsales\Eshop\Application\Controller\FrontendController

        // try neu : \OxidEsales\Eshop\Application\Controller\FrontendController::class => \Bender\dre_Ipayment\Application\Controller\dre_Ipayment_Redirect3dSecure::class,
        // \OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController
        // \OxidEsales\Eshop\Application\Controller\Admin\AdminController
        // neu:  \OxidEsales\Eshop\Application\Controller\Admin\AdminController::class => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn::class,
        // \OxidEsales\Eshop\Application\Controller\Admin\ShopConfiguration
        // neu: \OxidEsales\Eshop\Application\Controller\Admin\ShopConfiguration::class => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn_Config::class,
        // \OxidEsales\Eshop\Application\Controller\Admin\AdminListController
        // neu:  \OxidEsales\Eshop\Application\Controller\Admin\AdminListController::class => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn_List::class,
        //\OxidEsales\Eshop\Application\Controller\Admin\AdminListController
        // neu: \OxidEsales\Eshop\Application\Controller\Admin\AdminListController::class => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn_Log::class,
        //\OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController
        // neu: \OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController::class => \Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn_Main::class,
    ],
    'events' => [
        'onActivate' => '\Bender\dre_ipayment\Core\Events::onActivate',
        'onDeactivate' => '\Bender\dre_ipayment\Core\Events::onDeactivate',
    ],
    'templates'   => [
        'dre_ipayment_dyn.tpl'          => 'bender/dre_ipayment/Application/views/admin/tpl/dre_ipayment_dyn.tpl',
        'dre_ipayment_dyn_list.tpl'     => 'bender/dre_ipayment/Application/views/admin/tpl/dre_ipayment_dyn_list.tpl',
        'dre_ipayment_dyn_config.tpl'   => 'bender/dre_ipayment/Application/views/admin/tpl/dre_ipayment_dyn_config.tpl',
        'dre_ipayment_dyn_log.tpl'      => 'bender/dre_ipayment/Application/views/admin/tpl/dre_ipayment_dyn_log.tpl',
        'dre_ipayment_dyn_main.tpl'     => 'bender/dre_ipayment/Application/views/admin/tpl/dre_ipayment_dyn_main.tpl',
        'dre_ipayment_admin_order.tpl'  => 'bender/dre_ipayment/Application/views/admin/tpl/dre_ipayment_admin_order.tpl',
    ],
    'blocks'      => [
        [
            'template' => 'page/checkout/payment.tpl',
            'block' => 'change_payment',
            'file' => '/Application/views/blocks/dre_ipayment_change_payment.tpl'
        ],
        [
            'template' => 'page/checkout/payment.tpl',
            'block' => 'select_payment',
            'file' => '/Application/views/blocks/dre_ipayment_select_payment.tpl'
        ],
        [
            'template' => 'order_list.tpl',
            'block' => 'admin_order_list_item',
            'file' => '/Application/views/blocks/dre_ipayment_admin_order_list_item.tpl'
        ],
        [
            'template' => 'order_overview.tpl',
            'block' => 'admin_order_overview_billingaddress',
            'file' => '/Application/views/blocks/dre_ipayment_admin_order_overview_billingaddress.tpl'
        ],
    ],

);
