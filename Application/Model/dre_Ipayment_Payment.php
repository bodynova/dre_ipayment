<?php

namespace Bender\dre_Ipayment\Application\Model;


class dre_Ipayment_Payment extends dre_Ipayment_Payment_parent { //\OxidEsales\Eshop\Application\Model\Payment { //dre_Ipayment_Payment_parent {
	/**
	 * True if this payment is handled by ipayment, false if not.
	 * @var bool
	 */
	public $blExtPayment;

	/**
	 * Ipayment payment type (e.g. 'cc' or 'elv'), or null if this payment is not handled by ipayment.
	 * @var string
	 */
	public $sPaymentType;

    /**
     * Assigns DB field values to object fields.
     * Also stores sPaymentType and blExtPayment on object for backwards compatibility with old templates.
     * 
     * @extend assign
     *
     * @param array $dbRecord Associative data values array
     *
     * @return null
     */
	public function assign ( $dbRecord ){
		parent::assign( $dbRecord );
		// backwards compatibility:
		$this->sPaymentType = $this->getIpaymentType();
		$this->blExtPayment = $this->sPaymentType ? true : false;
	}

    /**
     * Delete this object from the database, returns true on success.
     * Since the ipayment type for this payment might be stored in dre_object2ipayment,
     * we will delete it from there when a payment object is deleted.
     *
     * @extend delete
     *
     * @param string $sOxid object id (default null means: use this object's id)
     *
     * @return bool true on success, false on failure
     */
	public function delete ( $sOxid = null ){
		$blResult = parent::delete( $sOxid );
		if ( ! $blResult )
			return $blResult;
		
		if ( ! $sOxid )
			$sOxid = $this->getId();
		if ( ! $sOxid )
			return $blResult;
		
		$sDelete = "delete from dre_object2ipayment where oxpaymentid='" . $sOxid . "' and oxshopid='" . $this->getConfig()->getShopId() . "'";
		\OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() )->execute( $sDelete );
		return $blResult;
	}

    /**
     * Function checks whether loaded payment is valid to current basket.
     * This overloaded function will make ipayment payments always valid. They would otherwise fail the
     * check because they do not store payment data in dynamical values for privacy reasons (this would
     * be against ipayment policies).
     * 
     * @extend isValidPayment
     *
     * @param array $aDynvalue dynamical value (in this case oxidcreditcard and oxiddebitnote are checked only)
     * @param string $sShopId id of current shop
     * @param oxuser $oUser the current user
     * @param double $dBasketPrice the current basket price (oBasket->dprice)
     * @param string $sShipSetId the current ship set
     *
     * @return bool true if payment is valid
     */
    public function isValidPayment( $aDynvalue, $sShopId, $oUser, $dBasketPrice, $sShipSetId ){
    	$this->_azCleanUpIpaymentData();

    	if ( ! $this->getId() ) {
    		return false;
        }
    	
        $aIpaymentData = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_aData' );
        if ( $this->useCreditcardDataIframe(true) || is_array( $aIpaymentData ) && $aIpaymentData['paymentid'] == $this->getId() ) {
            return true;
        }

        return parent::isValidPayment( $aDynvalue, $sShopId, $oUser, $dBasketPrice, $sShipSetId );
    }

    /**
     * Cleans up az_ipayment_aData from session if this payment is not handled by ipayment.
     *
     * @return null
     */
    protected function _azCleanUpIpaymentData (){
    	if( ! $this->getIpaymentType() )
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_aData' );
    }

    /**
     * Returns the ipayment payment type (e.g. 'cc' or 'elv') for this payment method.
     *
     * @return string ipayment payment type, or null if this payment is not handled by ipayment
     */
    public function getIpaymentType (){
    	$sSelect = "select oxtype from dre_object2ipayment where oxpaymentid='" . $this->getId() . "' and oxshopid='" . $this->getConfig()->getShopId() . "'";
    	$sType = \OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() )->getOne( $sSelect );
    	if ( $sType && !empty( $sType ) )
    		return $sType;
    	return null;
    }

    /**
     * Sets this payment's ipayment payment type (e.g. 'cc' or 'elv') and stores the changes in the database (table dre_object2ipayment).
     * Invalid payment types will be rejected. You can disable ipayment handling for this payment method by using null for the ipayment
     * payment type.
     *
     * @param string $sIpaymentType the ipayment type to use for this payment, or null if this payment shall not be handled by ipayment
     * 
     * @return null
     */
    public function setIpaymentType ( $sIpaymentType ){
    	if ( !$this->getId() )
    		return;
    	
    	if ( $this->getIpaymentType() ){
    		if ( $sIpaymentType )  // change ipayment type
    		{
    			if ( ! dre_Ipayment::isValidIpaymentType( $sIpaymentType ) )
    				return;
    			$sUpdate = "update dre_object2ipayment set oxtype='" . $sIpaymentType . "' where oxpaymentid='" . $this->getId() . "' and oxshopid='" . $this->getConfig()->getShopId() . "'";
                \OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() )->execute( $sUpdate );
    		}
    		else  // remove ipayment type
    		{
    			$sDelete = "delete from dre_object2ipayment where oxpaymentid='" . $this->getId() . "' and oxshopid='" . $this->getConfig()->getShopId() . "'";
                \OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() )->execute( $sDelete );
    		}
    	}else{
    		if ( $sIpaymentType )  // add ipayment type
    		{
    			if ( ! dre_Ipayment::isValidIpaymentType( $sIpaymentType ) )
    				return;
    			$sInsert = "insert into dre_object2ipayment (oxid,oxpaymentid,oxshopid,oxtype) values('" . \OxidEsales\Eshop\Core\UtilsObject::getInstance()->generateUID() . "','" . $this->getId() . "','" . $this->getConfig()->getShopId() . "','" . $sIpaymentType . "')";
                \OxidEsales\Eshop\Core\DatabaseProvider::getDb(dre_Ipayment::getFetchMode() )->execute( $sInsert );
    		}
    	}
    	
		// backwards compatibility:
    	$this->sPaymentType = $sIpaymentType;
    	$this->blExtPayment = $this->sPaymentType ? true : false;
    }
    
    public function useCreditcardDataIframe($blIsValidCalling = false) {
        $oConfig = $this->getConfig();
        
        if($this->getId() === 'oxidcreditcard' && $this->getIpaymentType() && $oConfig->getConfigParam('az_ipayment_blDataIframe') && ($blIsValidCalling === true || !\OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_blBrokeOutOfIframe' ))) {
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_aData' );
            return true;
        }
        return false;
    }
    
    public function getDynValues() {
        $oConfig = $this->getConfig();

        if($this->getId() === 'oxidcreditcard' && $this->getIpaymentType() && $oConfig->getConfigParam('az_ipayment_blDataIframe')) {
            return array();
        }
        return parent::getDynValues();
    }
    
}
