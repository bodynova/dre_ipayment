<?php

namespace Bender\dre_Ipayment\Application\Model;



class dre_Ipayment_Log_List extends dre_Ipayment_Log_List_parent { //dre_Ipayment_Log_List_parent {
    /**
     * List Object class name
     *
     * @var string
     */
    protected $_sObjectsInListName = 'dre_Ipayment_Log';

    /**
     * Core table name
     *
     * @var string
     */
    protected $_sCoreTable = 'dre_ipayment_logs';

    /**
     * Class constructor.
     * Prepares the list to show dre_ipayment_log objects loaded from dre_ipayment_logs table.
     * 
     * @extend __construct
     */
	public function __construct ()
	{
		$this->_sCoreTable = 'dre_ipayment_logs';
		parent::__construct( dre_Ipayment_Log::class );
	}

	/**
	 * Selects and assigns all ipayment log objects.
	 * 
	 * @return null
	 */
	public function loadIpaymentLogs ()
	{
		$sSelect = "select * from dre_ipayment_logs ";
		$sSelect .= "where oxgwtype = 'ipay' ";
		$sSelect .= "order by oxtime desc";
		
		$this->selectString( $sSelect );
	}

	/**
	 * Selects and assigns ipayment log objects for a specific order.
	 *
	 * @param string $sOrderId the oxid of the order
	 * 
	 * @return null
	 */
	public function loadIpaymentLogsForOrder ( $sOrderId ){
		if ( !$sOrderId )
		{
			$this->clear();
			return;
		}
		
		$sSelect = "select oxstorageid from dre_ipayment_logs where oxorderid='" . $sOrderId . "' and oxstorageid!=''";
		$sStorageId = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(dre_Ipayment::getFetchMode() )->getOne($sSelect);
		
		$sSelect = "select * from dre_ipayment_logs where ";
		if ( $sStorageId )
		    $sSelect .= "(oxorderid = '" . $sOrderId . "' or oxstorageid = '" . $sStorageId . "')";
		else
		    $sSelect .= "oxorderid = '" . $sOrderId . "'";
		$sSelect .= " and oxgwtype = 'ipay' ";
		$sSelect .= "order by oxtime desc";
		
		$this->selectString( $sSelect );
	}
}
