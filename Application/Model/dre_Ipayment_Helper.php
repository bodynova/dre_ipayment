<?php

namespace Bender\dre_Ipayment\Application\Model;

use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Registry;
/*
use OxidEsales\Eshop\Core\Config;
use OxidEsales\Eshop\Core\UtilsView;
use OxidEsales\Eshop\Core\Utils;
use OxidEsales\Eshop\Core\Session;
use OxidEsales\Eshop\Core\UtilsDate;
use OxidEsales\Eshop\Core\UtilsServer;
use OxidEsales\Eshop\Core\Language;
*/

// Registry::get(\OxidEsales\Eshop\Core\Request::class)->getRequestEscapedParameter($name, $defaultValue);

class dre_Ipayment_Helper extends dre_Ipayment_Helper_parent {

    protected static $_blUseRegistry = true;
    //erl
    protected static function _useRegistry() {
        if(self::$_blUseRegistry === null) {
            self::$_blUseRegistry = false;
            if(class_exists('Registry')) {
                $oConf = Registry::getConfig();
                if(method_exists($oConf, 'getRequestParameter')) {
                    self::$_blUseRegistry = true;
                }
            }
        }
        return self::$_blUseRegistry;
    }
    //erl
    public static function fcGetConfig() {
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getConfig();
        }else{
            die('_useRegistry() getConfig() not true!');
            //return Config::getInstance();
        }
    }
    //erl
    public static function fcGetRequest(){
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getRequest();
        }
    }
    //erl
    public static function fcGetLang() {
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getLang();
        } else {
            die('_useRegistry() getLang() not true!');
            //return Language::getInstance();
        }
    }
    //erl
    public static function fcGetSession() {
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getSession();
        } else {
            die('_useRegistry() getSession() not true!');
            //return Session::getInstance();
        }
    }
    //erl
    public static function fcGetUtils() {
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getUtils();
        }else{
            die('_useRegistry() getUtils() not true!');
            //return Utils::getInstance();
        }
    }
    //erl
    public static function fcGetUtilsView() {
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getUtilsView();
        }else{
            die('_useRegistry() getUtilsView() not true!');
            //return UtilsView::getInstance();
        }
    }
    //erl
    public static function fcGetUtilsDate() {
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getUtilsDate();
        }else{
            die('_useRegistry() getUtilsDate() not true!');
            //return UtilsDate::getInstance();
        }
    }

    public static function fcGetUtilsServer() {
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getUtilsServer();
        }else{
            die('_useRegistry() getUtilsServer() not true!');
            //return UtilsServer::getInstance();
        }
    }
    
    public static function fcGetRequestParameter($sParam) {
        if(self::_useRegistry() === true) {
            //Request::getRequestEscapedParameter()
            return \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter($sParam);
        }else{
            die('_useRegistry() fcGetRequestParameter() not true!');
            //return self::fcGetConfig()->getParameter($sParam);
        }
    }
    
    public static function fcSetSessionVar($sKey, $sValue) {
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getSession()->setVariable($sKey, $sValue);
        } else {
            die('_useRegistry() fcSetSessionVar() not true!');
            //return self::fcGetSession()->setVar($sKey, $sValue);
        }
    }
    //erl
    public static function fcGetSessionVar($sKey) {
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getSession()->getVariable($sKey);
        } else {
            die('_useRegistry() fcGetSession() not true!');
            //return self::fcGetSession()->getVar($sKey);
        }
    }
    //erl
    public static function fcDeleteSessionVar($sKey) {
        if(self::_useRegistry() === true) {
            return \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable($sKey);
        } else {
            die('_useRegistry() fcGetSession() not true!');
            //return self::fcGetSession()->deleteVar($sKey);
        }
    }
    //erl
    public static function fcGetShopVersion() {
        $sVersion = self::fcGetConfig()->getActiveShop()->oxshops__oxversion->value;
        $iVersion = (int)str_replace('.', '', $sVersion);
        while ($iVersion < 1000) {
            $iVersion = $iVersion*10;
        }
        return $iVersion;
    }

}