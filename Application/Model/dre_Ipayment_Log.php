<?php

namespace Bender\dre_Ipayment\Application\Model;

use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Application\Model\Order;
use OxidEsales\Eshop\Core\Model\BaseModel;

// oxBase = BaseModel


class dre_Ipayment_Log extends dre_Ipayment_Log_parent {

    const SITUATION_PAYMENT = 'payment';
    const SITUATION_OXPAYMENTGATEWAY = 'oxpaymentgateway';
    const SITUATION_3DSECURE = '3d-secure';
    const SITUATION_BREAK_IFRAME = 'break-iframe';
    const SITUATION_IFRAME = 'iframe';
    const SITUATION_IFRAME_RETURN = 'iframe-return';
    const SITUATION_ADMIN_CAPTURE = 'admin-capture';
    const SITUATION_ADMIN_REVERSE = 'admin-reverse';
    const SITUATION_ADMIN_REFUND = 'admin-refund';

    /**
     * Name of current class (dre_Ipayment_Log).
     * @var string
     */
	protected $_sClassName = 'dre_Ipayment_Log';

	/**
	 * Cache order's payment description (lazy loading).
	 *
	 * @var string
	 */
	protected $_sPaymentDesc = null;

	/**
	 * Cache order user's email address (lazy loading).
	 *
	 * @var string
	 */
	protected $_sUserEmailAddress = null;

	/**
	 * Class constructor.
	 * Prepares this object to use dre_ipayment_logs table for data.
	 * 
	 * @extend __construct
	 */
	public function __construct () {
		parent::__construct();
		$this->init( 'dre_ipayment_logs' );
	}

	/**
     * Assigns DB field values to object fields.
     * Since the payment total is stored in the smallest currency unit in the database, we need to convert it after loading.
     * The error text / transaction details field is stored in serialized form, so this function unserializes it after loading.
     * 
     * @extend assign
     *
     * @param array $dbRecord Associative data values array
     *
     * @return null
	 */
	public function assign ( $dbRecord )
	{
		parent::assign( $dbRecord );
		
		// oxamount is stored as cents (not euro) in the database, convert it:
	    $oIpayment = oxNew( dre_Ipayment::class );
	    if ( $this->dre_ipayment_logs__oxrevision->value < 4700 ) {
	        $this->dre_ipayment_logs__oxamount = new Field( $this->dre_ipayment_logs__oxamount->value / 100 );
	    }
	    else {
		    $this->dre_ipayment_logs__oxamount = new Field( $oIpayment->getPriceFromIpaymentPrice( $this->dre_ipayment_logs__oxamount->value, $this->dre_ipayment_logs__oxcurr->value ) );
	    }
		//Note: don't ask me why, but this is in rawValue, not value, even though it was saved as value...!?
		$this->dre_ipayment_logs__oxerrtext = new Field( unserialize( $this->dre_ipayment_logs__oxerrtext->rawValue ) );
	}

    /**
     * Save this object to database, insert or update as needed.
     * Since the payment total is stored in the smallest currency unit in the database, we need to convert it before saving (and revert it afterwards).
     * The error text / transaction details field is automatically serialized before saving (and reverted afterwards).
     *
     * @extend save
     * 
     * @return mixed the object id of the saved object, or false if saving failed
     */
	public function save (){
	    $oIpayment = oxNew( dre_Ipayment::class );
		// oxamount is stored as cents (not euro) in the database, so convert it before saving and revert it afterwards:
		$dOldAmount = $this->dre_ipayment_logs__oxamount->value;
		$sOldErrText = $this->dre_ipayment_logs__oxerrtext->value;
		$this->dre_ipayment_logs__oxamount = new Field( $oIpayment->getIpaymentPrice( $this->dre_ipayment_logs__oxamount->value, $this->dre_ipayment_logs__oxcurr->value ) );
		$this->dre_ipayment_logs__oxerrtext = new Field( serialize( addslashes( $this->dre_ipayment_logs__oxerrtext->value ) ) );
		$this->dre_ipayment_logs__oxrevision = new Field( $oIpayment->getModuleRevision() );
		
		$blResult = parent::save();
		
		$this->dre_ipayment_logs__oxamount = new Field( $dOldAmount );
		$this->dre_ipayment_logs__oxerrtext = new Field( $sOldErrText );
		
		return $blResult;
	}

    /**
     * Insert this object into the database if ipayment logging has been enabled (through the admin interface).
     *
     * @extend _insert
     * 
     * @return bool true if the object has been inserted, false otherwise
     */
	protected function _insert ()
	{
		if ( !$this->getConfig()->getConfigParam( 'az_ipayment_blLogPaymentActions' ) ) {
			return false;
		}
		return parent::_insert();
	}

	/**
	 * Convenience function to add an ipayment log entry.
	 * If ipayment logging is enabled, then this will create a new log object from the data passed
	 * as arguments and store it in the dre_ipayment_logs database table.
	 *
	 * @param float $dAmount order total (amount to be paid through ipayment)
	 * @param mixed $oOrder either an oxorder object, or a string with an order id
	 * @param string $sTransId ipayment transaction id
	 * @param string $sStorageId ipayment storage id
	 * @param string $sErrorCode error code
	 * @param string $sErrorText error text or transaction details
	 * @param string $sSituation situation in which the logging occurs (see dre_ipayment_log::SITUATION_* constants)
	 * @param string $sTransactionType ipayment transaction type
	 * 
	 * @return dre_ipayment_log the newly created log entry or null if no log could be written (e.g. because ipayment logging has been disabled)
	 */
    public static function log( $dAmount, $oOrder, $sTransId, $sStorageId, $sErrorCode, $sErrorText, $sSituation, $sTransactionType = "", $sAuth = "" ){
    	$oLog = oxNew(dre_Ipayment_Log::class);
    	
    	$oConfig = $oLog->getConfig();
        
        if ( is_object($oOrder) ) {
        	$sOrderId = $oOrder->oxorder__oxid->value;
        }
        else
        {
        	$sOrderId = $oOrder;
        	if ( $sOrderId ){
	        	$oOrder = oxNew(Order::class);
        		$oOrder->load( $sOrderId );
        	}
        }
        
        $oLog->dre_ipayment_logs__oxtime = new Field( strftime( '%Y-%m-%d %H:%M:%S' ) );
        $oLog->dre_ipayment_logs__oxshopid = new Field( $oConfig->getShopId() );
        $oLog->dre_ipayment_logs__oxorderid = new Field( $sOrderId );
        $oLog->dre_ipayment_logs__oxtransid = new Field( $sTransId );
	
///////////
	    /**
	     * new by Andre:
	     */
	    $oLog->dre_ipayment_logs__authcode = new Field( $sAuth );
///////////
	    
	    if ( !$sStorageId )
        {
            $aIpayment = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_aData' );
            if ( is_array( $aIpayment ) ) {
                $sStorageId = $aIpayment['storage_id'];
            }
        }
        $oLog->dre_ipayment_logs__oxstorageid = new Field( $sStorageId );
        $oLog->dre_ipayment_logs__oxerrcode = new Field( (int) $sErrorCode );
        $oLog->dre_ipayment_logs__oxgwtype = new Field( 'ipay' );
        $oLog->dre_ipayment_logs__oxsituation = new Field( $sSituation );

        if ( ! $oLog->isAdmin() )
        {
            $oUser = $oLog->getUser();
            $oLog->dre_ipayment_logs__oxuid = new Field( $oUser->oxuser__oxid->value );    
        }else if( $oOrder ) {
            $oLog->dre_ipayment_logs__oxuid = new Field( $oOrder->oxorder__oxuserid->value );
        }

        // if value came from ipayment it has no decimal separator
        if ( is_string( $dAmount ) ) {
            $dAmount = str_replace( ',', '.', $dAmount );
        }
        $dAmount = (double)$dAmount;
        if ( $dAmount ) {
            $oLog->dre_ipayment_logs__oxamount = new Field( sprintf( '%.02f', $dAmount ) );
        }

        $sLogMsg = "ACTIVE CLASS: " . \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'cl' ) . " (ipayment)\n";
        $sLogMsg .= "TRANSACTION TYPE: " . strtolower( $sTransactionType ) . "\n";
        $sLogMsg .= "RESULT: $sErrorText\n";
        $sLogMsg = rtrim( $sLogMsg );
        $oLog->dre_ipayment_logs__oxerrtext = new Field( $sLogMsg );
	
//////////
	    /**
	     * new by Andre:
	     */
	    $oLog->dre_ipayment_logs__authcode = new Field( $sAuth );
//////////
	    
        if ( $dAmount ) {
            if ( $oOrder ) {
                $oLog->dre_ipayment_logs__oxcurr = new Field( $oOrder->oxorder__oxcurrency->value );
            }
            if ( !$oLog->dre_ipayment_logs__oxcurr->value ) {
                $oLog->dre_ipayment_logs__oxcurr = new Field( $oConfig->getActShopCurrencyObject()->name );
            }
        }

        // order/payment session id
        $oLog->dre_ipayment_logs__oxsessid = new Field(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable( "az_ipayment_sSessionId" ) );
        if ( !$oLog->dre_ipayment_logs__oxsessid->value && $oOrder ) {
            $oLog->dre_ipayment_logs__oxsessid = new Field( \OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() )->getOne( "select dre_ipayment_logs.oxsessid from dre_ipayment_logs where dre_ipayment_logs.oxorderid='" . $oOrder->oxorder__oxid->value . "'" ) );
        }
        if ( !$oLog->dre_ipayment_logs__oxsessid->value )
        {
            $oLog->dre_ipayment_logs__oxsessid = new Field( \OxidEsales\Eshop\Core\UtilsObject::getInstance()->generateUID() );
            \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( "az_ipayment_sSessionId", $oLog->dre_ipayment_logs__oxsessid->value );
        }

        if ( is_object( $oOrder ) && $oOrder->oxorder__oxpayid->value ) {
            $oLog->dre_ipayment_logs__oxpayid = new Field( $oOrder->oxorder__oxpayid->value );
        }
        else {
            $oLog->dre_ipayment_logs__oxpayid = new Field(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable( "paymentid" ) );
        }
		
        if ( $oLog->save() ) {
        	return $oLog;
        }else{
        	return null;
        }
    }
    
    public static function debugLog($sMessage) {
        error_log(date('[Y-m-d H:i:s] ').$sMessage."\n", 3, getShopBasePath().'log/dre_ipayment_soap_exceptions.log');
    }

    /**
     * Returns the payment description of the payment method of this log log entry's order.
     *
     * @return string the payment description of this log entry's order's payment method, or null if none could be found
     */
	public function getPaymentDesc ()
	{
		if ( $this->_sPaymentDesc === null && $this->dre_ipayment_logs__oxpayid->value )
			$this->_sPaymentDesc = \OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() )->getOne( "SELECT oxdesc FROM " . getViewName( 'oxpayments' ) . " WHERE oxid = '" . $this->dre_ipayment_logs__oxpayid->value . "'" );
		return $this->_sPaymentDesc;
	}

	/**
	 * Returns the email address of the user that made this log entry's order.
	 *
	 * @return string the email address of this log entry's order, or null if none could be found
	 */
	public function getUserEmailAddress (){
		if ( $this->_sUserEmailAddress === null && $this->dre_ipayment_logs__oxuid->value )
			$this->_sUserEmailAddress = \OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() )->getOne( "SELECT oxusername from " . getViewName( 'oxuser' ) . " WHERE oxid = '" . $this->dre_ipayment_logs__oxuid->value . "'" );
		return $this->_sUserEmailAddress;
	}

	/**
	 * Returns the order number of this log entry's order.
	 * 
	 * @return int the order number of this log entry's order, or null if no matching order could be found
	 */
	public function getOrderNr (){
	    $sOrderId = $this->dre_ipayment_logs__oxorderid->value;
	    if ( !$sOrderId ) {
            return null;
        }
	    $sOrderNr = \OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() )->getOne( "select oxordernr from " . getViewName( 'oxorder' ) . " where oxid='" . $sOrderId . "'" );
	    if ( !$sOrderNr ) {
            return null;
        }
	    return $sOrderNr;
	}

	/**
	 * Returns the situation description (translated).
	 * 
	 * @return string situation
	 */
	public function getSituation (){
	    switch ( $this->dre_ipayment_logs__oxsituation->value )
	    {
	        case self::SITUATION_PAYMENT :
	            return \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_SITUATION_PAYMENT' );
	        case self::SITUATION_OXPAYMENTGATEWAY :
	            return \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_SITUATION_OXPAYMENTGATEWAY' );
	        case self::SITUATION_3DSECURE :
	            return \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_SITUATION_3DSECURE' );
	        case self::SITUATION_BREAK_IFRAME :
	            return \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_SITUATION_BREAK_IFRAME' );
            case self::SITUATION_IFRAME :
                return \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_SITUATION_IFRAME' );
            case self::SITUATION_IFRAME_RETURN :
                return \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_SITUATION_IFRAME_RETURN' );
	        case self::SITUATION_ADMIN_CAPTURE :
	            return \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_SITUATION_ADMIN_CAPTURE' );
	        case self::SITUATION_ADMIN_REVERSE :
	            return \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_SITUATION_ADMIN_REVERSE' );
	        case self::SITUATION_ADMIN_REFUND :
	            return \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_SITUATION_ADMIN_REFUND' );
	    }
	    return $this->dre_ipayment_logs__oxsituation->value;
	}

	/**
	 * Returns the formated amount.
	 * 
	 * @return string amount formated for the order currency, or an empty string if the amount is 0.
	 */
	public function getFAmount (){
	    if ( !$this->dre_ipayment_logs__oxamount->value ) {
	        return '';
	    }
	    
	    $oConfig = $this->getConfig();
	    $oCurrency = $oConfig->getCurrencyObject( $this->dre_ipayment_logs__oxcurr->value );
	    return \OxidEsales\Eshop\Core\Registry::getLang()->formatCurrency( $this->dre_ipayment_logs__oxamount->value, $oCurrency );
	}

	/**
	 * Returns the timestamp as a date/time formated for the date/time settings.
	 * 
	 * @return formated timestamp
	 */
	public function getDateTime (){
	    return date( \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'fullDateFormat' ), strtotime( $this->dre_ipayment_logs__oxtime->value ) );
	}
}
