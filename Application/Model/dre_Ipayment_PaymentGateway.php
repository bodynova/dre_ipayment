<?php

namespace Bender\dre_Ipayment\Application\Model;

use Bender\dre_Ipayment\Application\Model;
use OxidEsales\Eshop\Core\Exception;
use OxidEsales\Eshop\Core\Field;


class dre_Ipayment_PaymentGateway extends dre_Ipayment_PaymentGateway_parent { //dre_Ipayment_PaymentGateway_parent {
    /**
     * ipayment client
     * @var dre_Ipayment
     */
    protected $_oIpayment;

    /**
     * Default payment transaction type
     * @var string
     */
    protected $_sTransactionType = "AUTH";

    /**
     * Check for fraud attack or not ("FALSE"/"TRUE"")
     * @var string
     */
    protected $_sCheckFraud = "FALSE";

    /**
     * Current action method name
     * @var string
     */
    protected $_sActionMethod = null;

    /**
     * Perform admin action authorization or not (true/false)
     * @var bool
     */
    protected $_blAdminAuthorize = false;

    // %%PRO_ONLY_BEGIN%% ####################################################################################################
    protected $_az_ipayment_sSituation = null;

    // %%PRO_ONLY_END%% ######################################################################################################
    /**
     * Creates, connects and returns an ipayment webservice client.
     *
     * @return dre_Ipayment ipayment webservice client
     */
    protected function _azGetIpayment ()
    {
        if (is_object( $this->_oIpayment)){
            return $this->_oIpayment;
        }
        
    	if (!\OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam('az_ipayment_blActive')){
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_aData' );
    		return null;
    	}
        
    	try{
    	    $this->_oIpayment = oxNew( dre_Ipayment::class );
    	    $this->_oIpayment->connect();
    	}catch ( Exception $e ){
            $this->_sLastError   = "CONNECTION ERROR: " . $e->getMessage();
            $this->_iLastErrorNo = -1;
            throw $e;
        }
        return $this->_oIpayment;
    }

    protected function _handleIframeRedirect() {
        \OxidEsales\Eshop\Core\Registry::getSession()->setVariable('az_ipayment_blDataIframe', true);
        $sReturnUrl = str_replace('&amp;', '&', $this->getConfig()->getShopSecureHomeUrl() ). 'cl=dre_Ipayment_Redirect3dSecure&stoken='. \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter('stoken').'&sDeliveryAddressMD5='. \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter('sDeliveryAddressMD5');
        \OxidEsales\Eshop\Core\Registry::getUtils()->redirect($sReturnUrl, false);
    }
    
    /**
     * Executes payment gateway functionality.
     * 
     * @extend executePayment
     * 
     * @param double $dAmount order price
     * @param object $oOrder order object
     * 
     * @return bool
     */
    public function executePayment ( $dAmount, & $oOrder ){
        $this->_iLastErrorNo = null;
        $this->_sLastError = null;

        if ( ! $this->getConfig()->getConfigParam( 'az_ipayment_blActive' ) ) {
    		return parent::executePayment( $dAmount, $oOrder );
        }
    	
		try{
            $oIpayment = $this->_azGetIpayment();
    	}catch ( Exception $e ){
    	    // do nothing, if only the connection failed we can check that later
    	}
    	if ( !$oIpayment ) {
    		return parent::executePayment( $dAmount, $oOrder );
    	}
    	
		// %%PRO_ONLY_BEGIN%% ####################################################################################################
        if ( !$this->_az_ipayment_sSituation ) {
            $this->_az_ipayment_sSituation = dre_Ipayment_Log::SITUATION_OXPAYMENTGATEWAY;
        }
        
        // %%PRO_ONLY_END%% ######################################################################################################
    	// only process orders paid via ipayment:
		if ( $oOrder && $oOrder->oxorder__oxpaymenttype->value ){
			$oPayment = oxNew(\OxidEsales\Eshop\Application\Model\Payment::class);
			$oPayment->load( $oOrder->oxorder__oxpaymenttype->value );
		}

        if($oPayment->useCreditcardDataIframe()) {
            $this->_handleIframeRedirect();
        }

		if ( !$this->isAdmin() && ( !$oOrder || !$oPayment || !$oPayment->getIpaymentType() || $oPayment->useCreditcardDataIframe() ) ) {
    		return parent::executePayment( $dAmount, $oOrder );
		}

		if ( !$oIpayment->isConnected() ) {
    	    return false;  // payment failed
		}

        $aIpayment = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_aData' );
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_aData' );
        
        $this->_azPrepareOrder( $oOrder, $aIpayment );
        
        if ( $this->isAdmin() ) {
            $soxpayid = $oPayment->getIpaymentType();
        }
        else {
            $soxpayid = $aIpayment['trx_paymenttyp'];
        }
        
        // choosing right method to call
        if ( $this->_sTransactionType == 'AUTH' ) {
            $this->_sTransactionType = $oIpayment->getAuthorizationMethod( $soxpayid );
        }
        
        $sAction = $this->_sTransactionType;
        
        // check if we have 3D-Secure return data for the order:
        $a3dSecureData = $this->_azGet3dSecureData( $oOrder );
        if ( $a3dSecureData ) {
            if($this->_isFatchip3DSecureTest()) {
	        	$oOrder->oxorder__oxtransstatus = new Field( $this->_azGetStatus( 'SUCCESS' ) );
                $oOrder->oxorder__oxpaid = new Field( strftime( '%Y-%m-%d %H:%M:%S' ) );
                $oOrder->oxorder__oxtransid = new Field( '1-0815' );
                $oOrder->save();

                // clear situation for next action:
                $this->_az_ipayment_sSituation = null;

                \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_sSessionId' );
                return true;
            }
            
            try {
                $oResponse = $oIpayment->getPaymentAuthenticationReturn( $a3dSecureData );
            }catch( Exception $e ){
                // %%PRO_ONLY_BEGIN%% ####################################################################################################
                if ( method_exists( $e, 'getConnectionError' ) ) {
                    $sConnectionError = $e->getConnectionError();
                }
                else {
                    $sConnectionError = '';
                }
                dre_Ipayment_Log::log( $oOrder->getTotalOrderSum(), $oOrder, null, null, 0, "Exception during 3D-Secure return: " . $e->getMessage() . "\nError: " . $sConnectionError, dre_Ipayment_Log::SITUATION_3DSECURE, $this->_sTransactionType );
                // %%PRO_ONLY_END%% ######################################################################################################
                return false;
            }
        }else{
            $oResponse = $this->_azPerformAction( $sAction, $aIpayment, $dAmount, $oOrder );
        }
        
        if ( !$this->_azProcessResponse( $oResponse, $sAction, $aIpayment, $dAmount, $oOrder ) ) {
        	return false;
        }

        $oOrder->save();

        // %%PRO_ONLY_BEGIN%% ####################################################################################################
        $sStorageId = null;
        if ( $oResponse && $oResponse->successDetails ) {
            $sStorageId = $oResponse->successDetails->retStorageId;
        }
        $sLogMsg = 'SUCCESS';
   		if ( $this->getConfig()->getShopConfVar( 'az_ipayment_blLogParams' ) ) {
   		    $sLogMsg .= $this->_oIpayment->collectResponseData( $oResponse );
   		}
        dre_Ipayment_Log::log( $dAmount, $oOrder, $oIpayment->getTransactionNumberFromResponse( $oResponse ), $sStorageId, 'NO ERROR', $sLogMsg, $this->_az_ipayment_sSituation, $this->_sTransactionType ,$oIpayment->getAuthCodeFromResponse($oResponse));
        
        // clear situation for next action:
        $this->_az_ipayment_sSituation = null;

        // %%PRO_ONLY_END%% ######################################################################################################

        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_sSessionId' );

        return true;
    }

    /**
     * Returns 3D-Secure return data (MD and PaRes) if it is available for the current order.
     * This data is loaded from the az_ipayment_redirectdata table by the az_ipayment_redirect_id parameter.
     * 
     * @param oxOrder $oOrder order object
     * @return array 3D-Secure redirect data, or null if no 3D-Secure redirect data is available for this order
     */
    protected function _azGet3dSecureData( $oOrder ){
        $sRedirectDataId = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'az_ipayment_redirect_id' );
        if ( !$sRedirectDataId ) {
            return null;
        }
        
        // load 3D-Secure data (MD, PaRes) from database:
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() );
        $sQuery = "SELECT azredirectdata FROM dre_ipayment_redirectdata WHERE oxid = " . $oDb->quote( $sRedirectDataId );
        $sSerializedRedirectData = $oDb->getOne( $sQuery );
        if ( !$sSerializedRedirectData ) {
            return null;
        }
        
        try {
            $aRedirectData = unserialize( $sSerializedRedirectData );
            if ( is_array( $aRedirectData ) ) {
                $sMd = $aRedirectData[ 'MD' ];
                $sPaRes = $aRedirectData[ 'PaRes' ];
                
                // remove stored MD and PaRes:
                $sQuery = "DELETE FROM dre_ipayment_redirectdata WHERE oxid = " . $oDb->quote( $sRedirectDataId );
                $oDb->execute( $sQuery );
                
                // return MD and PaRes:
                return array( 'MD' => $sMd, 'PaRes' => $sPaRes );
            }
        }catch ( Exception $oException ){
            // %%PRO_ONLY_BEGIN%% ####################################################################################################
            dre_Ipayment_Log::log( $oOrder->getTotalOrderSum(), $oOrder, null, null, 0, "Could not unserialize data for 3D-Secure order\nredirect data id: " . $sRedirectDataId, dre_Ipayment_Log::SITUATION_3DSECURE );
            // %%PRO_ONLY_END%% ######################################################################################################
        }
        
        return null;
    }

    /**
     * Prepares an order for processing via ipayment.
     * Sets the oxxid status and sets the transstatus to ERROR until the payment has been resolved successfully.
     *
     * @param oxOrder $oOrder the order object to prepare
     * @param array $aIpayment ipayment payment data
     * 
     * @return null
     */
    protected function _azPrepareOrder ( & $oOrder, $aIpayment ){
        // order processed manually or automatically?
		if ( !$this->isAdmin() && $this->getConfig()->getConfigParam('az_ipayment_blProcInAdmin') ){
            $oOrder->oxorder__oxxid = new Field( 1, Field::T_RAW );
		}
        $oOrder->oxorder__oxtransstatus = new Field( "ERROR" );
        if ( !$this->isAdmin() ){
            // if admin performs actions - this data is already stored in order object
            $oOrder->oxorder__oxpayid = new Field( $aIpayment["paymentid"] );
            if ( !$oOrder->oxorder__oxpaymentid->value ) {
                $oOrder->oxorder__oxpaymentid = new Field($aIpayment["paymentid"]);
            }
            $oOrder->oxorder__oxpident = new Field( "ipay" );
        }
        $oOrder->save();
    }

    /**
     * Performs an ipayment payment action (e.g. pre-authorize, authorize, capture, reverse, refund).
     *
     * @param string $sAction the action to perform (i.e. the webservice function to call)
     * @param array $aIpayment ipayment payment data
     * @param double $dAmount order amount
     * @param oxOrder $oOrder order object
     * 
     * @return mixed result from the webservice call
     */
    protected function _azPerformAction ( $sAction, $aIpayment, $dAmount, & $oOrder ){
    	$oConfig = $this->getConfig();
    	
    	$this->_iLastErrorNo = 0;
    	if ( !$sAction ){
        	$this->_sLastError   = "FATAL ERROR: UNKNOWN ACTION METHOD " . $aIpayment["trx_paymenttyp"];
            $this->_iLastErrorNo = -1;
            // %%PRO_ONLY_BEGIN%% ####################################################################################################
            dre_Ipayment_Log::log( $dAmount, $oOrder, '', $aIpayment['storage_id'], $this->_iLastErrorNo, $this->_sLastError, $this->_az_ipayment_sSituation, $this->_sTransactionType );
            // %%PRO_ONLY_END%% ######################################################################################################
            return null;
        }
        
        // --- collecing data for SOAP method execution ---

        // ipayment account data
        $aAccountData = array();
        $aAccountData["accountId"] = $oConfig->getConfigParam('az_ipayment_sAccount');        
        $aAccountData["trxuserId"] = $oConfig->getConfigParam('az_ipayment_sUser');
        $aAccountData["trxpassword"] = $oConfig->getConfigParam('az_ipayment_sPassword');
        if ( $this->_blAdminAuthorize && $oConfig->getConfigParam('az_ipayment_sAdminPassword') ) {
            $aAccountData["adminactionpassword"] = $oConfig->getConfigParam('az_ipayment_sAdminPassword');
        }

        // order transaction data
        $aTransactionData = array();
        $aTransactionData["trxAmount"] = $dAmount = $this->_azGetOrderPrice( $dAmount, $oOrder );
        $aTransactionData["trxCurrency"] = $oOrder->oxorder__oxcurrency->value;
        $aTransactionData['invoiceText'] = $this->_azGetInvoiceText( $oOrder );

        // additional parameters
        $aOptions = array();
        $sIpAddress = $this->_azGetIpAddress( $oOrder );
        if ( $sIpAddress )
            $aOptions[ 'fromIp' ] = $sIpAddress;
        
		// parameters used by ipayment to identify the module:
		$aOptions[ 'clientData' ] = array();
		$aOptions[ 'clientData' ][ 'clientName' ] = dre_Ipayment::getClientName();
		$aOptions[ 'clientData' ][ 'clientVersion' ] = dre_Ipayment::getClientVersion();
		$aOptions[ 'otherOptions' ] = array();
		$aOptions[ 'otherOptions' ][] = array( 'key' => 'ppcbedc', 'value' => dre_Ipayment::getIpaymentPpcbedc() );
		
		$sLanguageCode = dre_Ipayment::getLanguageCode();
		if ( $sLanguageCode ) {
            $aOptions['errorLang'] = $sLanguageCode;
        }
		
        // --- executing SOAP methods ---
        try{
        	if ( $this->_blAdminAuthorize ) {
        	    $oResponse = $this->_azGetIpayment()->call( $sAction, array( $aAccountData, $oOrder->oxorder__oxtransid->value, $aTransactionData, $aOptions ) );
        	}else{
                // customer payment data
                $aPaymentData = array();
                $aParams = array( $aAccountData, $aPaymentData, $aTransactionData, $aOptions );
                $aParamVariables = array( 'fromDatastorageId' => $aIpayment['storage_id'] );
                $oResponse = $this->_azGetIpayment()->call( $sAction, $aParams, $aParamVariables );
            }
            return $oResponse;
        }catch ( Exception $e ){
            return null;
        }
    }

    /**
     * Returns an invoice text for an ipayment transaction.
     *
     * @param oxOrder $oOrder order object
     * 
     * @return string invoice text
     */
    protected function _azGetInvoiceText ( $oOrder )
    {
       	$oConfig = $this->getConfig();
    	$sInvoiceText = $oConfig->getConfigParam( 'az_ipayment_sInvoiceText' );
    	if ( $sInvoiceText ) {
            $sInvoiceText = trim($sInvoiceText);
        }
        if ( empty( $sInvoiceText ) ) {
            $sInvoiceText = "#SHOP# - BestNr. #ORDERNR#";
        }
	    $aReplace = array(
	        '#SHOP#' => $oConfig->getActiveShop()->oxshops__oxname->value,
	        '#ORDERNR#' => $oOrder->oxorder__oxordernr->value,
	        '#CUSTNR#' => '',
	        '#CUSTFIRSTNAME#' => '',
	        '#CUSTLASTNAME#' => '',
	    	'#CUSTEMAIL#' => '',
	    );
		$oUser = oxNew( \OxidEsales\Eshop\Application\Model\User::class);
		if ( $oUser->load( $oOrder->oxorder__oxuserid->value ) ){
		    $aReplace[ '#CUSTNR#' ] = $oUser->oxuser__oxcustnr->value;
		    $aReplace[ '#CUSTFIRSTNAME#' ] = $oUser->oxuser__oxfname->value;
		    $aReplace[ '#CUSTLASTNAME#' ] = $oUser->oxuser__oxlname->value;
		    $aReplace[ '#CUSTEMAIL#' ] = $oUser->oxuser__oxusername->value;
		}
		return str_ireplace( array_keys( $aReplace ), array_values( $aReplace ), $sInvoiceText );
    }

    /**
     * Returns the current IP-address (or the IP-address of the order, if called from the admin interface).
     *
     * @param oxOrder $oOrder order object
     * 
     * @return string IP-address
     */
    protected function _azGetIpAddress ( $oOrder ){
        if ( $this->isAdmin() && $oOrder->oxorder__oxip->value ) {
            return $oOrder->oxorder__oxip->value;
        }else{
            return \OxidEsales\Eshop\Core\Registry::getUtilsServer()->getRemoteAddress();
        }
    }

    protected function _getTestForm() {
        return '<FORM name="payauthForm" method=POST action="#">
                    BODYNOVA 3D-SECURE TEST-MODUS
                    <script type="text/javascript">
                        window.location = "%REDIRECT_RETURN_SCRIPT%?";
                    </script>
                </form>';
    }
    
    protected function _get3dSecureTestObject($oResponse) {
        if($oResponse->status == 'SUCCESS') {
            $oResponse->status = 'REDIRECT';
            $oResponse->redirectDetails = new \stdClass();
            $oResponse->redirectDetails->redirectAction = 'REDIRECT_POSTFORM';
            $oResponse->redirectDetails->redirectData = $this->_getTestForm();
        }
        return $oResponse;
    }

    protected function _isFatchip3DSecureTest() {
        $oConfig = \OxidEsales\Eshop\Core\Registry::getConfig();
        if($oConfig->getConfigParam('az_ipayment_bl3DSecureIframe') && $oConfig->getConfigParam('az_ipayment_sAccount') == '99999') {
            return true;
        }
        return false;
    }
    
    /**
     * Processes a response from an ipayment action (webservice call).
     *
     * @param mixed $oResponse response from webservice call
     * @param string $sAction action/function used for webservice call
     * @param array $aIpayment ipayment data
     * @param double $dAmount order amount
     * @param oxOrder $oOrder order object
     * 
     * @return bool returns true if the response indicates that the action was successful, or false if the action has failed
     */
    protected function _azProcessResponse ( $oResponse, $sAction, $aIpayment, $dAmount, & $oOrder ){
    	if ( !$oResponse->status ) {
            return false;
        }
        if($this->_isFatchip3DSecureTest()) {
            $oResponse = $this->_get3dSecureTestObject($oResponse);
        }        
        $sStatus = strtoupper( $oResponse->status );
        switch ( $sStatus ){
        	case 'ERROR':
                /**
				 * $this->_iLastErrorNo and $this->_sLastError are used by OXID default to show error message on payment.tpl 
				 * when redirected after paymanet error
				 */
                $this->_iLastErrorNo = dre_Ipayment::getPaymentErrorCode( $oResponse->errorDetails->retErrorcode, $oResponse->errorDetails->retFatalerror ? true : false );
                $sLastError = dre_Ipayment::getPaymentErrorText( $oResponse->errorDetails->retErrorcode, $oResponse->errorDetails->retFatalerror ? true : false );

                // don't write this into $this->_sLastError, as it would be preferred over $this->_iLastErrorNo and result in an error no of -1
   	            if ( !$sLastError && $this->getConfig()->getConfigParam( 'az_ipayment_blShowServiceErrors' ) ){
   	                if ( isset( $oResponse->errorDetails->retErrorMsg ) )
   	                    $sLastError = $oResponse->errorDetails->retErrorMsg;
   	                if ( isset( $oResponse->errorDetails->retAdditionalMsg ) )
   	                    $sLastError .= ' ' . $oResponse->errorDetails->retAdditionalMsg;
   	                $this->_sLastError = $sLastError;
   	            }
                \OxidEsales\Eshop\Core\Registry::getSession()->setVariable('az_ipayment_blPaymentError', true );
				
	            // %%PRO_ONLY_BEGIN%% ####################################################################################################
	            $sErrorLogText = $sLastError;
	            if ( dre_Ipayment::ignoreErrorCode( $oResponse->errorDetails->retErrorcode ) ) {
	            	$sErrorLogText .= "\nIGNORING ERROR because of status (transaction already being processed)";
	            }
	            $sErrorLogText .= dre_Ipayment::collectResponseData( $oResponse );
	            dre_Ipayment_Log::log( $dAmount, $oOrder, '', $aIpayment['storage_id'], $this->_iLastErrorNo, $sErrorLogText, $this->_az_ipayment_sSituation, $this->_sTransactionType );
	            // %%PRO_ONLY_END%% ######################################################################################################
	            
	            if ( dre_Ipayment::ignoreErrorCode( $oResponse->errorDetails->retErrorcode ) ) {
	            	return true;
	            }

	            return false;

	        case 'SUCCESS':
	        	$oOrder->oxorder__oxtransstatus = new Field( $this->_azGetStatus( $sStatus ) );
	            // if order is paid then must set paid time
	            if ( $this->_sTransactionType == 'CAPTURE' || $this->_sTransactionType == 'AUTH' || strpos( $sAction, 'authorize' ) !== false ) {
	                $oOrder->oxorder__oxpaid = new Field( strftime( '%Y-%m-%d %H:%M:%S' ) );
	            }
                
                if(isset($oResponse->successDetails->trxPayauthStatus) && $oResponse->successDetails->trxPayauthStatus == 'U') {
                    $oOrder->oxorder__az_3dcheckunavailable = new Field(1);
                }
                
	            $sRetTrxNumber = $this->_azGetIpayment()->getTransactionNumberFromResponse( $oResponse );
	            if ( $sRetTrxNumber ) {
                    $oOrder->oxorder__oxtransid = new Field($sRetTrxNumber);
                }
	            return true;

        	case 'REDIRECT':  // this means that a redirect to a 3D-Secure bank is necessary
        	    if ( ! $oResponse->redirectDetails->redirectData )
        	    {
                    $this->_sLastError = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_3DSECURE_NOT_SUPPORTED' );
                    $this->_iLastErrorNo = -1;
                    // %%PRO_ONLY_BEGIN%% ####################################################################################################
                    $sLogMsg = $this->_sLastError;
               		if ( $this->getConfig()->getShopConfVar( 'az_ipayment_blLogParams' ) ) {
               		    $sLogMsg .= dre_Ipayment::collectResponseData( $oResponse );
               		}
                    dre_Ipayment_Log::log( $dAmount, $oOrder, '', $aIpayment['storage_id'], $this->_iLastErrorNo, $sLogMsg, dre_Ipayment_Log::SITUATION_3DSECURE, $this->_sTransactionType );
                    // %%PRO_ONLY_END%% ######################################################################################################
                    return false;
        	    }

                \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( 'az_ipayment_sOrderId', $oOrder->oxorder__oxid->value);
                \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( 'az_ipayment_sRedirectData', $oResponse->redirectDetails->redirectData);
                \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( 'az_ipayment_aData', $aIpayment);
		        
                // %%PRO_ONLY_BEGIN%% ####################################################################################################
                $sLogMsg = $this->_sLastError;
           		if ( $this->getConfig()->getShopConfVar( 'az_ipayment_blLogParams' ) ) {
           		    $sLogMsg .= dre_Ipayment::collectResponseData( $oResponse );
           		}
		        dre_Ipayment_Log::log( $dAmount, $oOrder, '', $aIpayment['storage_id'], $this->_iLastErrorNo, $sLogMsg, dre_Ipayment_Log::SITUATION_3DSECURE, $this->_sTransactionType );
                // %%PRO_ONLY_END%% ######################################################################################################
                
                // return false to delete the order and redirect to 3D-Secure:
                return false;

        	default:
	            $this->_sLastError   = 'UNKNOWN STATUS: ' . $sStatus;
	            $this->_iLastErrorNo = -1;
	            // %%PRO_ONLY_BEGIN%% ####################################################################################################
                $sLogMsg = $this->_sLastError;
           		if ( $this->getConfig()->getShopConfVar( 'az_ipayment_blLogParams' ) ) {
           		    $sLogMsg .= dre_Ipayment::collectResponseData( $oResponse );
           		}
	            dre_Ipayment_Log::log( $dAmount, $oOrder, '', $aIpayment['storage_id'], $this->_iLastErrorNo, $sLogMsg, $this->_az_ipayment_sSituation, $this->_sTransactionType );
	            // %%PRO_ONLY_END%% ######################################################################################################
	            return false;
        }
    }

    // %%PRO_ONLY_BEGIN%% ####################################################################################################
    /**
     * Books the payment amount of an order (performs an ipayment 'capture' transaction).
     * 
     * @param double $dAmount order price
     * @param object $oOrder order object
     * 
     * @return bool true if successful, false otherwise
     */
    public function captureOrder ( $dAmount, & $oOrder, $sSituation = null ){
        $this->_sTransactionType = "CAPTURE";
        $this->_sActionMethod = "capture";
        $this->_blAdminAuthorize = true;
        if ( $sSituation ) {
            $this->_az_ipayment_sSituation = $sSituation;
        }else{
            $this->_az_ipayment_sSituation = dre_Ipayment_Log::SITUATION_ADMIN_CAPTURE;
        }
        return $this->executePayment( $dAmount, $oOrder );
    }

    /**
     * Cancels an order (performs an ipayment 'reverse' transaction).
     * 
     * @param double $dAmount order price
     * @param object $oOrder order object
     * 
     * @return bool true if successful, false otherwise
     */
    public function cancelOrder ( $dAmount, & $oOrder, $sSituation = null ){
        $this->_sTransactionType = "REVERSE";
        $this->_sActionMethod = "reverse";
        $this->_blAdminAuthorize = true;
        if ( $sSituation ) {
            $this->_az_ipayment_sSituation = $sSituation;
        }else{
            $this->_az_ipayment_sSituation = dre_Ipayment_Log::SITUATION_ADMIN_REVERSE;
        }
        if ( $this->executePayment( $dAmount, $oOrder ) ){
        	$oOrder->oxorder__oxxid = new Field( 2, Field::T_RAW );
            $oOrder->save();
            return true;
        }
        return false;
    }

    /**
     * Refunds the payment amount of an order back to the buyer (performs an ipayment 'refund' transaction).
     * 
     * @param double $dAmount order price
     * @param object $oOrder order object
     * 
     * @return bool true if successful, false otherwise
     */
    public function refundOrder ( $dAmount, & $oOrder, $sSituation = null ){
        $this->_sTransactionType = "REFUND";
        $this->_sActionMethod = "refund";
        $this->_blAdminAuthorize = true;
        if ( $sSituation ) {
            $this->_az_ipayment_sSituation = $sSituation;
        }else{
            $this->_az_ipayment_sSituation = dre_Ipayment_Log::SITUATION_ADMIN_REFUND;
        }
        if ($this->executePayment( $dAmount, $oOrder ) ){
        	$oOrder->oxorder__oxxid = new Field( 3, Field::T_RAW );
        	$oOrder->save();
        	return true;
        }
        return false;
    }
    // %%PRO_ONLY_END%% ######################################################################################################

    /**
     * Returns new order status according to configuration.
     * 
     * @param string $sStatusStr status string
     * 
     * @return string status
     */
    protected function _azGetStatus ( $sStatusStr ){
        /* this has been deprecated for a while, the shop standard values "OK" and "ERROR" should be used:
        if ( $this->getConfig()->getConfigParam('az_ipayment_blProcInAdmin') && !$this->isAdmin() && $sStatusStr == 'SUCCESS' )
            return 'WAITING';
        */
        return $sStatusStr;        
    }

    /**
     * Returns the order price in ipayment format (smallest unit of order/shop currency).
     * 
     * @param integer $dPrice initial order price
     * 
     * @return integer ipayment order price
     */
    protected function _azGetOrderPrice ( $dPrice, $oOrder ){
        $oIpayment = $this->_azGetIpayment();
    	if ( $oOrder && $oOrder->oxorder__oxtotalordersum->value )
        	$dPrice = $oOrder->oxorder__oxtotalordersum->value;
        if ( $oOrder && $oOrder->oxorder__oxcurrency->value )
            return $oIpayment->getIpaymentPrice( $dPrice, $oOrder->oxorder__oxcurrency->value );
        else
            return $oIpayment->getIpaymentPrice( $dPrice );
    }
}
