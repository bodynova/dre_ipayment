<?php

namespace Bender\dre_Ipayment\Application\Model;

use Bender\dre_Ipayment\Core\dre_Ipayment_Soap;
use OxidEsales\Eshop\Core\Base;

class dre_Ipayment extends dre_Ipayment_parent { //Base {
	/**
	 * Official name of this module.
	 * Please don't change this, the ipayment webservice requires this exact string.
	 * @var string
	 */
	protected static $_sModuleName = 'oxid2ipaymentPRO';

	/**
	 * Internal client-name of this module for the ipayment webservice API.
	 * Please don't change this, the ipayment webservice requires this exact string.
	 * @var string
	 */
	protected static $_sModuleClientName = 'Oxid2IpaymentPRO';

	/**
	 * Ipayment ppcbedc code for the ipayment webservice API.
	 * Please don't change this, the ipayment webservice requires this exact string.
	 * @var string
	 */
	protected static $_sPpcbedc = '33a307bfb62e0c3f49d5';

	/**
	 * Version number of this module.
	 * Please update this value when you release a new version of the module.
	 * @var string
	 */
	protected static $_sModuleVersion = '2.4.12';

	/**
	 * Revision number of this module (internal versioning).
	 * Please leave this value as it is, it will be automatically updated on subversion commits/updates.
	 * @var string
	 */
	protected static $_sModuleRevision = '5521';

	/**
	 * URL of the ipayment webservice's WSDL file.
	 * @var string
	 */
    protected $_sIpaymentWsdlUrl;

    /**
     * PHP SoapClient instance used to connect to the ipayment webservice.
     * @var SoapClient
     */
    protected $_oSoapClient = false;

    /**
     * Hold the oxDb fetch-mode, needed for backwards-compatibility
     * 
     * @var int
     */
    protected static $_iFetchMode = null;
    
    /**
     * Class constructor.
     * Creates SOAP connector.
     */
    public function __construct (){
    	$this->_oSoapClient = oxNew(dre_Ipayment_Soap::class);
    }

    /**
     * Checks if a given ipayment type is valid or not. Valid ipayment types are 'cc' and 'elv', or null for
     * payments that are not handled by ipayment.
     * 
     * @param string $sIpaymentType ipayment type ('cc', 'elv' or null)
     * @return bool true: ipayment type is valid, false: ipayment type is not valid
     */
    public static function isValidIpaymentType($sIpaymentType){
    	if($sIpaymentType === 'cc' || $sIpaymentType === 'elv'){
            return true;
        }else{
            return false;
        }
    }

    /**
	 * Returns the name of the ipayment module.
	 * 
	 * @return string the module name
	 */
    public static function getModuleName (){
    	return self::$_sModuleName;
    }

    /**
     * Returns the version number of the ipayment module.
     * The ipayment service uses this to identify the module when calls are made.
     * 
     * @return string ipayment module version
     */
    public static function getModuleVersion (){
        return self::$_sModuleVersion;
    }

    /**
     * Returns the revision number of the ipayment module.
     * This revision number is increased automatically by the version control system.
     * 
     * @return integer ipayment module revision number
     */
    public static function getModuleRevision (){
    	return self::$_sModuleRevision;
    }

    /**
     * Returns a client name in the way the ipayment webservice expects it.
     * This is the OXID shop edition and version number.
     * 
     * @return string client name for the ipayment webservice
     */
    public static function getClientName(){
    	$edition = \OxidEsales\Facts\Facts::getEdition();
    	$version = \OxidEsales\Eshop\Core\ShopVersion::getVersion();
    	return 'Oxid E-Sales 4 ' . $edition . ' ' . $version;
    }

    /**
     * Returns a client version in the way the ipayment webservice expects it.
     * This is the module name, version and release number.
     * 
     * @return string client version for the ipayment webservice
     */
    public static function getClientVersion (){
    	return self::$_sModuleClientName . ' ' . self::getModuleVersion();
    }

    /**
     * Returns the ppcbedc code that the ipayment webservice expects.
     * 
     * @return string ipayment ppcbedc code
     */
    public static function getIpaymentPpcbedc (){
    	return self::$_sPpcbedc;
    }

    /**
     * Goes through response object and formats response string.
     * 
     * @param array $oResponse webservice response data object
     * @param bool $blFirst first record marker
     * 
     * @return string response data
     */
    public static function collectResponseData( $oResponse, $blFirst = true ){
        if ( $blFirst ) {
            $sResult = "\nSOAP RESPONSE:";
        }
        
        if(is_object($oResponse)){
            foreach($oResponse as $sKey => $mValue){
                $sResult .= "\n" . $sKey . ': ';
                if(is_object($mValue)){
                    $sResult .= self::collectResponseData($mValue, false);
                }else{
                    $sResult .= $mValue;
                }
            }
        }else{
            $sResult = $oResponse;
        }
        return $sResult;
    }

    /**
     * Converts the character set encoding of strings, arrays or objects.
     * 
     * @param string $mValue text/object to convert (can be a string, array or object)
     * @param string $sFromCharset charset to convert from (if empty, then the shop charset will be used)
     * @param string $sToCharset charset to convert to (if empty, then the shop charset will be used)
     * @return mixed text/object with converted encoding
     */
    public static function convertCharset( $mValue, $sFromCharset = null, $sToCharset = null ){
        if ( !$sFromCharset ) {
            $sFromCharset = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'charset' );
        }
        $sFromCharset = strtoupper($sFromCharset);
        if (!$sToCharset){
            $sToCharset = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'charset' );
        }
        $sToCharset = strtoupper($sToCharset);
        
        // if the charsets don't differ, then don't convert:
        if($sFromCharset === $sToCharset){
            return $mValue;
        }
        
        if(is_object( $mValue )){
            foreach($mValue as $sKey => $mTmpValue){
                $mValue->$sKey = self::convertCharset( $mTmpValue, $sFromCharset, $sToCharset );
            }
        }elseif(is_array( $mValue )){
            foreach ( $mValue as $sKey => $mTmpValue ) {
                $mValue[ $sKey ] = self::convertCharset( $mTmpValue, $sFromCharset, $sToCharset );
            }
        }else{
            $mValue = iconv($sFromCharset, $sToCharset . '//IGNORE//TRANSLIT', $mValue);
        }
        
        return $mValue;
    }

    /**
     * Connects to the ipayment webservice.
     * 
     * @see az_ipayment_soap::setWsdlUrl()
     * 
     * @param string $sWsdlUrl optional ipayment webservice URL. The default is to use the known official ipayment webservice URL.
     * 
     * @return null
     */
    public function connect( $sWsdlUrl = null ){
        $this->_oSoapClient->setWsdlUrl( $sWsdlUrl ? $sWsdlUrl : $this->_sIpaymentWsdlUrl );
    }

	/**
	 * Checks whether the soap client is connected.
	 * 
	 * @return boolean true if the soap client is connected, false if not connected
	 */
    public function isConnected (){
        return $this->_oSoapClient->isConnected();
    }

    /**
     * Returns the ipayment webservice URL that this object is connected to.
     * 
     * @see dre_Ipayment_Soap::getWsdlUrl()
     * 
     * @return string ipayment webservice URL
     */
    public function getWsdlUrl (){
        return $this->_oSoapClient->getWsdlUrl();
    }

    /**
     * Performs a call to the ipayment webservice.
     * 
     * @see dre_Ipayment_Soap::call()
     * 
     * @param string $sAction the action to perform / function to call
     * @param string $aParams action/function parameters
     * @param array $aVariables additional action/function variables
     * 
     * @return mixed the result of the webservice call
     */
    public function call($sAction, $aParams, $aVariables = array()){
        $aParams = $this->_oSoapClient->applyParamVariables($aParams, $aVariables);
        $aParams = $this->_oSoapClient->addClientParams($aParams);
        return $this->_oSoapClient->call($sAction, $aParams);
    }

    /**
     * Returns the ipayment transaction number from a webservice call response.
     * 
     * @see dre_Ipayment_Soap::getTransactionNumberFromResponse()
     * 
     * @param mixed $oResponse a response from an ipayment webservice call
     * 
     * @return string the transaction number from the response
     */
    public function getTransactionNumberFromResponse($oResponse){
        return $this->_oSoapClient->getTransactionNumberFromResponse( $oResponse );
    }

    /**
     * @param $oResponse
     * @return string the AuthCode from the response
     */
    public function getAuthCodeFromResponse($oResponse){
        return $this->_oSoapClient->getAuthCodeFromResponse( $oResponse );
    }

    /**
     * Returns the actual SOAP method name for payment authorization.
     * 
     * @see az_ipayment_soap::getAuthorizationMethod()
     * 
     * @param string $sPaymentType payment menthod type
     * 
     * @return string webservice call action/function to use for payment authorization
     */
    public function getAuthorizationMethod($sPaymentType){
        $myConfig = \OxidEsales\Eshop\Core\Registry::getConfig();

        if($myConfig->getConfigParam('az_ipayment_blProcInAdmin') && !$this->isAdmin()){
            return $this->_oSoapClient->getAuthorizationMethod("preAuthorize", $sPaymentType);
        }else{
            return $this->_oSoapClient->getAuthorizationMethod("authorize", $sPaymentType);
        }
    }

    /**
     * Returns the number of decimals used in a currency.
     * 
     * @param string $sCurrencyIso3 currency ISO 3-letter code
     * @return int number of decimals (usually 2, but there are currencies without a smaller currency unit)
     */
    public function getCurrencyDecimals($sCurrencyIso3 = null){
        if(!\is_string($sCurrencyIso3) || \strlen($sCurrencyIso3) !== 3){
            $oCur = $this->getConfig()->getActShopCurrencyObject();
            if($oCur && $oCur->name){
                $sCurrencyIso3 = $oCur->name;
            }
        }
        $iDecimals = 2;
        if(is_string($sCurrencyIso3) && strlen($sCurrencyIso3) == 3){
            $sCurrencyIso3 = strtoupper($sCurrencyIso3);
            $sSelect = 'select az_iso3,az_decimals from dre_ipayment_currencies where az_iso3="' . $sCurrencyIso3 . '"';
            $oResultSet = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(self::getFetchMode())->getAll($sSelect);
            if($oResultSet && $oResultSet->fields[ 0 ] === $sCurrencyIso3 &&  \count($oResultSet) > 0){
                $iDecimals = (int)($oResultSet->fields[1]);
            }
        }
        return $iDecimals;
    }

    /**
     * Converts a price into an ipayment amount.
     * 
     * @param double $dPrice        price
     * @param string $sCurrencyIso3 currency ISO 3-letter code
     * @return string ipayment amount (smallest currency unit)
     */
    public function getIpaymentPrice($dPrice, $sCurrencyIso3 = null){
        $iDecimals = $this->getCurrencyDecimals($sCurrencyIso3);
        $dFactor = (double)pow( 10, $iDecimals);
        $dPrice = ((double)$dPrice) * $dFactor;
        $sPrice = number_format($dPrice, 0, '', '');
        return $sPrice;
    }

    /**
     * Converts an ipayment amount into a shop price.
     * 
     * @param double $dIpaymentPrice ipayment amount (smallest currency unit)
     * @param string $sCurrencyIso3  currency ISO 3-letter code
     * @return double price
     */
    public function getPriceFromIpaymentPrice ( $dIpaymentPrice, $sCurrencyIso3 = null )
    {
        $iDecimals = $this->getCurrencyDecimals( $sCurrencyIso3 );
        $dFactor = (double)pow( 10, $iDecimals );
        $dPrice = ((double)$dIpaymentPrice) / $dFactor;
        $sPrice = number_format( $dPrice, $iDecimals, '.', '' );
        return (double)$sPrice;
    }

    /**
     * Performs a 3D-Secure call for payment authentication and returns the result.
     * 
     * @see az_ipayment_soap::paymentAuthenticationReturn()
     * 
     * @param array $a3dSecure 3D-Secure parameters
     * 
     * @return mixed payment authentication result
     */
    public function getPaymentAuthenticationReturn ( $a3dSecure )
    {
    	return $this->_oSoapClient->getPaymentAuthenticationReturn( $a3dSecure );
    }

    /**
     * Check if an error code should be ignored and the order considered successful (e.g. "transaction is already being processed").
     * Error codes 10020 and 12094 indicate a transaction that is already being processed and should be considered successful.
     * 
     * @param int $iIpaymentErrorCode ipayment error code
     * @return boolean true if the order should be considered successful, false if the error should be handled
     */
    public static function ignoreErrorCode( $iIpaymentErrorCode )
    {
        if($iIpaymentErrorCode == 10020 || $iIpaymentErrorCode == 12094){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Converts an ipayment error code into an OXID eShop payment error code.
     * 
     * @param int     $iIpaymentErrorCode   ipayment error code
     * @param boolean $blIpaymentFatalError true if the error is considered fatal by ipayment
     * @return int payment error code
     */
    public static function getPaymentErrorCode ( $iIpaymentErrorCode, $blIpaymentFatalError )
    {
        // %%PRO_ONLY_BEGIN%% ####################################################################################################
        if( $iIpaymentErrorCode > 3) {
            return $iIpaymentErrorCode;
        }
        if($blIpaymentFatalError){
            return 2;  // payment authorization failed (prevents order from being saved and user object from processing order)
        }else{
            return 4801;
        }
        // %%PRO_ONLY_END%% ######################################################################################################
        // %%CE_ONLY_BEGIN%% #####################################################################################################
        if(!$blIpaymentFatalError) {
            return 4801;
        }else {
            return 2;
        }
        // %%CE_ONLY_END%% #######################################################################################################
    }

    /**
     * Returns an error text from an ipayment error result.
     * 
     * @param int     $iIpaymentErrorCode   ipayment error code
     * @param boolean $blIpaymentFatalError true if the error is considered fatal by ipayment
     * @param array   $aIpaymentResult      ipayment result array
     * @return string payment error text
     */
    public static function getPaymentErrorText ( $iIpaymentErrorCode, $blIpaymentFatalError, $aIpaymentResult = null ){
        $sErrorText = null;
        if (\OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam('az_ipayment_blShowServiceErrors') && is_array( $aIpaymentResult ) && isset( $aIpaymentResult['ret_errormsg'] ) )
            $sErrorText = $aIpaymentResult['ret_errormsg'];
        
        $iPaymentError = self::getPaymentErrorCode( $iIpaymentErrorCode, $blIpaymentFatalError );
        // %%PRO_ONLY_BEGIN%% ####################################################################################################
        // check if there is a translated error message for this error:
        switch ( $iPaymentError )
        {
            case 3001 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_AMOUNT' );
                break;
            case 3002 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_CURRENCY' );
                break;
            case 5001 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_CARD_TYPE_MISMATCH' );
                break;
            case 5002 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_CARD_NUMBER' );
                break;
            case 5003 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_EXPIRY_DATE' );
                break;
            case 5004 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_START_DATE' );
                break;
            case 5005 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_ISSUE_NUMBER' );
                break;
            case 5006 :
            case 12007 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_SECURITY_CODE' );
                break;
            case 5007 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_UNSUPPORTED_CARD_TYPE' );
                break;
            case 5008 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_CARD_DATA' );
                break;
            case 5009 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_CARDHOLDER_MISSING' );
                break;
            case 5010 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_ACCOUNTNR' );
                break;
            case 5011 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_BANKCODE' );
                break;
            case 5012 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_IBAN' );
                break;
            case 5013 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_COUNTRY_UNSUPPORTED' );
                break;
            case 5014 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_COUNTRY_UNSUPPORTED' );
                break;
            case 5015 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_BIC' );
                break;
            case 5040 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_INVALID_ADDRESS' );
                break;
            case 5060 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_CARD_NEARLY_EXPIRED' );
                break;
            case 10021 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_PAYMENT_REFUSED' );
                break;
            case 11999 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_MULTIPLE_FAILURES' );
                break;
            case 12002 :
            case 12004 :
            case 12012 :
            case 12043 :
            case 12062 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_CARD_REFUSED' );
                break;
            case 12005 :
                $sTranslatedErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_ERROR_CARD_REFUSED_CHECK' );
                break;
        }
        if ( $sTranslatedErrorText && substr( $sTranslatedErrorText, 0, 17 ) !== 'AZ_IPAYMENT_ERROR')
            $sErrorText = $sTranslatedErrorText;
        
        // %%PRO_ONLY_END%% ######################################################################################################
        if ( !$sErrorText && ($iPaymentError == 4801 || !$blIpaymentFatalError) )
            $sErrorText = \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'AZ_IPAYMENT_INVALID_DATA' );
        
        return $sErrorText;
    }

    /**
     * Returns an ipayment language code (de, en) for the current shop language.
     * 
     * @return string ipayment error code
     */
    public static function getLanguageCode ()
    {
        $sLanguageCode = \OxidEsales\Eshop\Core\Registry::getLang()->getLanguageAbbr();
        if ( $sLanguageCode )
            $sLanguageCode = strtolower( $sLanguageCode );
        // currently only de and en are supported by ipayment:
        if ( $sLanguageCode !== 'de' )
            $sLanguageCode = 'en';
        
        return $sLanguageCode;
    }
    
    /**
     * Return shop-version as integer
     *
     * @return integer
     */
    protected static function _fcGetCurrentVersion() {
        $sVersion = \OxidEsales\Eshop\Core\Registry::getConfig()->getActiveShop()->oxshops__oxversion->value;
        $iVersion = (int)str_replace('.', '', $sVersion);
        while ($iVersion < 1000) {
            $iVersion = $iVersion*10;
        }
        return $iVersion;
    }
    
    /**
     * Return the needed oxDb fetch-mode, needed for backwards-compatibility
     * 
     * @return int
     */
    public static function getFetchMode() {
        if(self::$_iFetchMode === null) {
            $iVersion = self::_fcGetCurrentVersion();
            if($iVersion < 4600) {
                self::$_iFetchMode = false;
            } else {
                self::$_iFetchMode = \OxidEsales\Eshop\Core\DatabaseProvider::FETCH_MODE_NUM;
            }
        }
        return self::$_iFetchMode;
    }
    
}
