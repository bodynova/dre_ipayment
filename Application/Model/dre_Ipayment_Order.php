<?php

namespace Bender\dre_Ipayment\Application\Model;

//use \Bender\dre_Ipayment\Application\Model;

// oxpayment => \OxidEsales\Eshop\Application\Model\Payment
use OxidEsales\Eshop\Application\Model\Payment;
// oxCounter => \OxidEsales\Eshop\Core\Counter
use OxidEsales\Eshop\Core\Counter;
// oxField => \OxidEsales\Eshop\Core\Field
use OxidEsales\Eshop\Core\Field;
// Exception => OxidEsales\EshopCommunity\Core\Exception
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
// oxBasket => \OxidEsales\Eshop\Application\Model\Basket
use OxidEsales\Eshop\Application\Model\Basket;
// oxuserpayment => \OxidEsales\Eshop\Application\Model\UserPayment
use OxidEsales\Eshop\Application\Model\UserPayment;

class dre_Ipayment_Order extends dre_Ipayment_Order_parent  {//\OxidEsales\Eshop\Application\Model\Order {

    /**
     * Get current version number as 4 digit integer e.g. Oxid 4.5.9 is 4590
     * 
     * @return integer
     */
    protected function _fcGetCurrentVersion() {
        $sVersion = \OxidEsales\Eshop\Core\Registry::getConfig()->getActiveShop()->oxshops__oxversion->value;
        $iVersion = (int)str_replace('.', '', $sVersion);
        while ($iVersion < 1000) {
            $iVersion = $iVersion*10;
        }
        return $iVersion;
    }
    
    /**
     * Overrides standard oxid _insert method
     * 
     * Inserts order object information in DB. Returns true on success.
     *
     * @return bool
     */
    protected function _insert() {
        $blInsert = parent::_insert();
        if($this->_fcGetCurrentVersion() < 4700 || !\OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam( 'az_ipayment_blActive' )) {
            return $blInsert;
        }
        if($blInsert){
            if($this && $this->oxorder__oxpaymenttype->value){
                $oPayment = oxNew(Payment::class);
                $oPayment->load($this->oxorder__oxpaymenttype->value);
            }
            if( !$this->isAdmin() && ( !$oPayment || !$oPayment->getIpaymentType())) {
                return $blInsert;
            }

            // setting order number
            if ( !$this->oxorder__oxordernr->value ) {
                $blInsert = $this->_setNumber();
            } else {
                $counter = oxNew(Counter::class);
                $counter->update( $this->_getCounterIdent(), $this->oxorder__oxordernr->value );
            }
        }
        return $blInsert;
    }
    
    public function dreHandleIframeReturn() {
        $sStatus = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter('ret_status');
        $this->oxorder__oxtransstatus = new Field($sStatus);
        if($sStatus === 'SUCCESS') {
            $this->oxorder__oxtransstatus = new Field('OK');
            
            $sTransId = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter('ret_trx_number');
            if($sTransId) {
                $this->oxorder__oxtransid = new Field($sTransId);
            }

            $sType = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter('trx_typ');
            if($sType === 'auth') {
                $this->oxorder__oxpaid = new Field( strftime( '%Y-%m-%d %H:%M:%S' ) );
            }
            $this->oxorder__oxpayid = new Field('oxidcreditcard');
            $this->oxorder__oxpaymentid = new Field('oxidcreditcard');

            // order processed manually or automatically?
            if ( !$this->isAdmin() && \OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam('az_ipayment_blProcInAdmin') ) {
                $this->oxorder__oxxid = new Field( 1, Field::T_RAW );
            }
        }
        $this->save();
    }
    
    public function fcSetOrderStatus($sStatus) {
        // marking as not finished
        $this->_setOrderStatus($sStatus);
    }
    
    /**
     * Updates order transaction status. Faster than saving whole object
     *
     * @param string $sStatus order transaction status
     */
    public function fcSetOrderFolder($sFolder){
        if($sFolder) {
            $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(\OxidEsales\Eshop\Core\DatabaseProvider::FETCH_MODE_ASSOC);
            $sQ = 'update oxorder set oxfolder=' . $oDb->quote($sFolder) . ' where oxid=' . $oDb->quote($this->getId());
            $oDb->execute($sQ);
            //updating order object
            $this->oxorder__oxfolder = new Field($sFolder, Field::T_RAW);
        }
    }
    
    protected function _isDataIframeReturn() {
        $sRetToken = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter('rettoken');
        if($sRetToken && md5($sRetToken) === \OxidEsales\Eshop\Core\Registry::getSession()->getVariable('sFcIsDataIframeReturn')) {
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable('sFcIsDataIframeReturn');
            return true;
        }
        return false;
    }
    
    
    public function finalizeOrder(Basket $oBasket, $oUser, $blRecalculatingOrder = false) {
        if($this->_isDataIframeReturn() === false) {
            return parent::finalizeOrder($oBasket, $oUser, $blRecalculatingOrder);
        }
        $sGetChallenge = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable('sess_challenge');
        $this->load($sGetChallenge);
        // copies user info
        $this->_setUser($oUser);
        // payment information
        $oUserPayment = $this->_setPayment($oBasket->getPaymentId());
        // set folder information, if order is new
        // #M575 in recalculating order case folder must be the same as it was
        if (!$blRecalculatingOrder) {
            $this->_setFolder();
        }
        //saving all order data to DB
        $this->save();

        //removed payment-step
        oxNew(Counter::class)->update($this->_getCounterIdent(), $this->oxorder__oxordernr->value);

        // executing TS protection
        if (!$blRecalculatingOrder && $oBasket->getTsProductId()) {
            $blRet = $this->_executeTsProtection($oBasket);
            if ($blRet !== true) {
                return $blRet;
            }
        }
        // deleting remark info only when order is finished
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable('ordrem');
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable('stsprotection');


        if (!$blRecalculatingOrder) {
            $this->_updateOrderDate();
        }

        // updating order trans status (success status)
        $this->_setOrderStatus('OK');
        // store orderid
        $oBasket->setOrderId($this->getId());
        // updating wish lists
        $this->_updateWishlist($oBasket->getContents(), $oUser);
        // updating users notice list
        $this->_updateNoticeList($oBasket->getContents(), $oUser);
        // marking vouchers as used and sets them to $this->_aVoucherList (will be used in order email)
        // skipping this action in case of order recalculation
        if (!$blRecalculatingOrder) {
            $this->_markVouchers($oBasket, $oUser);
        }
        // send order by email to shop owner and current user
        // skipping this action in case of order recalculation
        if (!$blRecalculatingOrder) {
            $iRet = $this->_sendOrderByEmail($oUser, $oBasket, $oUserPayment);
        }else{
            $iRet = self::ORDER_STATE_OK;
        }
        return $iRet;
    }
    
    protected function _fcIsIpaymentPayment() {
        $sQuery = "SELECT oxid FROM dre_object2ipayment WHERE oxpaymentid = '{$this->oxorder__oxpaymenttype->value}' AND oxshopid = '". \OxidEsales\Eshop\Core\Registry::getConfig()->getShopId()."'";
        $sOxid = \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->getOne($sQuery);
        if($sOxid) {
            return true;
        }
        return false;
    }
    
    /**
     * This is a workaround for a bug in the OXID PayPal module, which would cause a fatal error during checkout for ipayment
     * The _setOrderStatus method in the oepaypaloxorder.php does not check if there is an actual object returned from this method.
     * Because creditcard-data must not be saved by the shop, there simply is not oxuserpayment entry for the oxcreditcard-paymenttype.
     * 
     * @return oxUserPayment
     */
    public function getPaymentType(){
        $mReturn = parent::getPaymentType();
        if(method_exists($this, 'getPayPalOrder') && $this->_fcIsIpaymentPayment() && !$mReturn) {
            $mReturn = oxNew(UserPayment::class);
        }
        return $mReturn;
    }
}