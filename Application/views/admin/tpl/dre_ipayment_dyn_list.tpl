[{include file="headitem.tpl" title="TOOLS_LTITLE"|oxmultilangassign box="list"}]

<script type="text/javascript">
<!--

function ChangeEditBar( sLocation, sPos)
{
    var oSearch = document.getElementById("search");
    oSearch.actedit.value=sPos;
    oSearch.submit();    

    var oTransfer = parent.edit.document.getElementById("transfer");
    oTransfer.cl.value=sLocation;

    //forcing edit frame to reload after submit
    top.forceReloadingEditFrame();
}

window.onLoad = top.reloadEditFrame();

//-->
</script>
<form name="search" id="search" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="actedit" value="[{$actedit}]">
    <input type="hidden" name="cl" value="dre_Ipayment_Dyn_List">
	<input type="hidden" name="oxid" value="x">
</form>

<!--
<div id="liste" align="right">
	<a href="[{oxmultilang ident='AZ_IPAYMENT_DYN_MAIN_HOMEPAGE_URL'}]" target="_blank">
        <img src="[{$oViewConf->getImageUrl()}]/az_ipayment_logo.jpg" alt="ipayment" border="0">
    </a>
</div>
-->

[{include file="pagetabsnippet.tpl" noOXIDCheck="true"}]
	
<script type="text/javascript">
if (parent.parent) 
{   parent.parent.sShopTitle   = "[{$actshopobj->oxshops__oxname->value}]";
    parent.parent.sMenuItem    = "[{oxmultilang ident='AZ_IPAYMENT_MAINMENU'}]";
    parent.parent.sMenuSubItem = "[{oxmultilang ident='AZ_IPAYMENT_SUBMENU'}]";
    parent.parent.sWorkArea    = "[{$_act}]";
    parent.parent.setTitle();
}
</script>

[{include file="bottomitem.tpl"}]
