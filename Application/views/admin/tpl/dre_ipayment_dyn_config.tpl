[{include file="headitem.tpl" title="[ipayment]"}]

<script type="text/javascript">
function _groupExp(el) {
    var _cur = el.parentNode;

    if (_cur.className == "exp") _cur.className = "";
      else _cur.className = "exp";
}
[{* %%PRO_ONLY_BEGIN%% #################################################################################################### *}]

function azAddClass ( oElement, sClass )
{
	if ( !oElement || !sClass )
		return;
	if ( oElement.className.indexOf( sClass ) == -1 )
		oElement.className = oElement.className + " " + sClass;
}

function azRemoveClass ( oElement, sClass )
{
	if ( !oElement || !sClass )
		return;
	if ( oElement.className.indexOf( sClass ) != -1 )
		oElement.className = oElement.className.replace( sClass, "" );
}

function azSetCustomInvoice ( oCheckbox, blEnabled )
{
	var oInput = document.getElementById("azIpaymentInvoiceTextInput");
	var oInfo = document.getElementById("azIpaymentInvoiceInfo");
	
	if ( !oCheckbox || !oInput )
		return;

	if ( oCheckbox.value )
		oInput.value = oCheckbox.value;
	
	if ( blEnabled ) {
		oInput.readOnly = false;
		azRemoveClass( oInput, "disabled" );
		azRemoveClass( oInfo, "disabled" );
	}
	else {
		oInput.readOnly = true;
		azAddClass( oInput, "disabled" );
		azAddClass( oInfo, "disabled" );
	}
}

function azSetLogging ( oCheckbox, blEnabled )
{
	var oInput = document.getElementById("ipaymentLogParams");
	var oLabel = document.getElementById("ipaymentLogParamsLabel");
    var oInput2 = document.getElementById("ipaymentLogSoapExceptions");
    var oLabel2 = document.getElementById("ipaymentLogSoapExceptionsLabel");
	
	if ( !oCheckbox || !oInput )
		return;

	if ( blEnabled ) {
		oInput.readOnly = false;
		oInput.disabled = false;
		oInput2.readOnly = false;
		oInput2.disabled = false;
		azRemoveClass( oInput, "disabled" );
		azRemoveClass( oLabel, "disabled" );
		azRemoveClass( oInput2, "disabled" );
		azRemoveClass( oLabel2, "disabled" );
	}
	else {
		oInput.readOnly = true;
		oInput.disabled = true;
		oInput2.readOnly = true;
		oInput2.disabled = true;
		azAddClass( oInput, "disabled" );
		azAddClass( oLabel, "disabled" );
		azAddClass( oInput2, "disabled" );
		azAddClass( oLabel2, "disabled" );
	}
}
[{* %%PRO_ONLY_END%% ###################################################################################################### *}]
</script>
[{* %%PRO_ONLY_BEGIN%% #################################################################################################### *}]

<style>
input.disabled { color:#bbb; }
label.disabled { color:#bbb; }
div.disabled { display:none; }
</style>
[{* %%PRO_ONLY_END%% ###################################################################################################### *}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="cl" value="dre_Ipayment_Dyn_Config">
</form>


<b>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_SETTINGS"}]</b>
<br><br>

[{if $oView->getTemplateBlockProblems()}]
    <div class="errorbox">
    	<p>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_BLOCKS_PROBLEMS"}]</p>
    	<div style="color:#000;">
        	<table>
        		<tr>
        			<th>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_BLOCKS_MODULE"}]</th>
        			<th>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_BLOCKS_BLOCKNAME"}]</th>
        			<th>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_BLOCKS_TEMPLATE"}]</th>
        			<th>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_BLOCKS_FILE"}]</th>
        			<th>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_BLOCKS_POS"}]</th>
        			<th></th>
        		</tr>
        		[{foreach from=$oView->getTemplateBlockProblems() item=oAzBlock}]
                    <tr>
                        <td>[{$oAzBlock->oxtplblocks__oxmodule->value}]</td>
                        <td style="padding-left:10px;">[{$oAzBlock->oxtplblocks__oxblockname->value}]</td>
                        <td style="padding-left:10px;">[{$oAzBlock->oxtplblocks__oxtemplate->value}]</td>
                        <td style="padding-left:10px;"><span title="[{$oAzBlock->sAzBlockTemplateFile}]">[{$oAzBlock->oxtplblocks__oxfile->value}]</span></td>
                        <td style="padding-left:10px;">[{$oAzBlock->oxtplblocks__oxpos->value}]</td>
                        <td style="padding-left:10px;">
                            <form action="[{$oViewConf->getSelfLink()}]" method="post">
                                [{$oViewConf->getHiddenSid()}]
                                <input type="hidden" name="cl" value="dre_Ipayment_Dyn_Config">
                                <input type="hidden" name="fnc" value="azSaveTemplateBlock">
                                <input type="hidden" name="oxid" value="[{$oxid}]">
                                <input type="hidden" name="aAzBlock[oxtplblocks__oxid]" value="[{$oAzBlock->oxtplblocks__oxid->value}]">
                                <input type="hidden" name="aAzBlock[oxtplblocks__oxactive]" value="1">
                                <input type="hidden" name="aAzBlock[oxtplblocks__oxshopid]" value="[{$oAzBlock->oxtplblocks__oxshopid->value}]">
                                <input type="hidden" name="aAzBlock[oxtplblocks__oxtemplate]" value="[{$oAzBlock->oxtplblocks__oxtemplate->value}]">
                                <input type="hidden" name="aAzBlock[oxtplblocks__oxblockname]" value="[{$oAzBlock->oxtplblocks__oxblockname->value}]">
                                <input type="hidden" name="aAzBlock[oxtplblocks__oxpos]" value="[{$oAzBlock->oxtplblocks__oxpos->value}]">
                                <input type="hidden" name="aAzBlock[oxtplblocks__oxfile]" value="[{$oAzBlock->oxtplblocks__oxfile->value}]">
                                <input type="hidden" name="aAzBlock[oxtplblocks__oxmodule]" value="[{$oAzBlock->oxtplblocks__oxmodule->value}]">
                                <input type="submit" value="[{if $oAzBlock->blAzMissing}][{oxmultilang ident='AZ_IPAYMENT_DYN_CONFIG_BLOCKS_ADD'}][{else}][{oxmultilang ident='AZ_IPAYMENT_DYN_CONFIG_BLOCKS_ACTIVATE'}][{/if}]">
                            </form>
                        </td>
                    </tr>
        		[{/foreach}]
        	</table>
    	</div>
    	<p>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_BLOCKS_PROBLEMS_INFO"}]</p>
    </div>
[{/if }]

<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post">
	[{$oViewConf->getHiddenSid()}]
	<input type="hidden" name="cl" value="dre_Ipayment_Dyn_Config">
	<input type="hidden" name="fnc" value="save">
	<input type="hidden" name="oxid" value="[{$oxid}]">

	<input type=hidden name=confbools[az_ipayment_blActive] value=false />
	<input type=checkbox class="editinput" name="confbools[az_ipayment_blActive]" id="ipaymentActive" value="true" [{if ($confbools.az_ipayment_blActive)}]checked="checked"[{/if}] />
	<label for="ipaymentActive">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_ACTIVE"}]</label>
	<br><br>

    <div class="groupExp">
        <div>
            <a href="#" onclick="_groupExp(this);return false;" class="rc"><b>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_SUPPORT"}]</b></a>
            <dl>
                <dt>
                	<input type="text" class="editinput" size="40" name="confstrs[az_ipayment_sFcSupportKey]" value="[{$confstrs.az_ipayment_sFcSupportKey}]" />
                </dt>
                <dd>
                    [{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_SUPPORTKEY"}]
                    [{oxinputhelp ident="AZ_IPAYMENT_DYN_CONFIG_SUPPORTKEY_HELP"}]
                </dd>
                <div class="spacer"></div>
            </dl>
         </div>
    </div>
    
    <div class="groupExp">
        <div>
            <a href="#" onclick="_groupExp(this);return false;" class="rc"><b>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_CONNECTION"}]</b></a>
            <dl>
                <dt>
                	<input type="text" class="editinput" size="40" name="confstrs[az_ipayment_sAccount]" value="[{$confstrs.az_ipayment_sAccount}]" />
                </dt>
                <dd>
                    [{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_ACCOUNTNUM"}]
                    [{oxinputhelp ident="AZ_IPAYMENT_DYN_CONFIG_CONNECTION_INFO"}]
                </dd>
                <div class="spacer"></div>
                <dt>
                	<input type="text" class="editinput" size="40" name="confstrs[az_ipayment_sUser]" value="[{$confstrs.az_ipayment_sUser}]" />
                </dt>
                <dd>
                    [{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_USER"}]
                </dd>
                <div class="spacer"></div>
                <dt>
                	<input type="text" class="editinput" size="40" name="confstrs[az_ipayment_sPassword]" value="[{$confstrs.az_ipayment_sPassword}]" />
                </dt>
                <dd>
                    [{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_PASSWORD"}]
                </dd>
                <div class="spacer"></div>
                <dt>
                	<input type="text" class="editinput" size="40" name="confstrs[az_ipayment_sAdminPassword]" value="[{$confstrs.az_ipayment_sAdminPassword}]" />
                </dt>
                <dd>
                    [{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_ACTIONPASSWORD"}]
                </dd>
                <div class="spacer"></div>
                <dt>
                	<input type="text" class="editinput" size="40" name="confstrs[az_ipayment_sSecKey]" value="[{$confstrs.az_ipayment_sSecKey}]" />
                </dt>
                <dd>
                    [{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_TRANSACTION_KEY"}]
                </dd>
                <div class="spacer"></div>
            </dl>
         </div>
    </div>

    <div class="groupExp">
        <div>
            <a href="#" onclick="_groupExp(this);return false;" class="rc"><b>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_PAYMENT_METHODS"}]</b></a>
            <dl>
            	[{assign var=oPayments value=$oView->getPayments()}]
				[{* %%PRO_ONLY_BEGIN%% #################################################################################################### *}]
 				[{foreach from=$oPayments item=oPayment}]
					[{assign var="sIpaymentType" value=$oPayment->getIpaymentType()}]
					<dt>
						<select name="ipaymentTypes[[{$oPayment->getId()}]]">
							<option value="" [{if !$sIpaymentType || $sIpaymentType == ""}]selected[{/if}]>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_PAYMENT_NONE"}]</option>
							<option value="cc" [{if $sIpaymentType == "cc"}]selected[{/if}]>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_PAYMENT_CC"}]</option>
							<option value="elv" [{if $sIpaymentType == "elv"}]selected[{/if}]>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_PAYMENT_ELV"}]</option>
						</select>
					</dt>
					<dd>
						[{$oPayment->oxpayments__oxdesc->value}]
					</dd>
	                <div class="spacer"></div>
				[{/foreach}]
				[{* %%PRO_ONLY_END%% ###################################################################################################### *}]
				[{* %%CE_ONLY_BEGIN%% ##################################################################################################### *}]
		        [{assign var="oCreditCard" value=$oPayments.oxidcreditcard}]
		        [{if $oCreditCard}]
    				<dt>
    					<input type="hidden" name="ipaymentTypes[oxidcreditcard]" value="" />
    				    <input type="checkbox" id="azIpaymentCreditCard" name="ipaymentTypes[oxidcreditcard]" value="cc"[{if $oCreditCard->getIpaymentType() == "cc"}] checked[{/if}] />
    				</dt>
    				<dd>
    					<label for="azIpaymentCreditCard">[{$oCreditCard->oxpayments__oxdesc->value}]</label>
    				</dd>
                    <div class="spacer"></div>
                [{/if}]
		        [{assign var="oDebitNote" value=$oPayments.oxiddebitnote}]
		        [{if $oDebitNote}]
    				<dt>
    					<input type="hidden" name="ipaymentTypes[oxiddebitnote]" value="" />
    				    <input type="checkbox" id="azIpaymentDebitNote" name="ipaymentTypes[oxiddebitnote]" value="elv"[{if $oDebitNote->getIpaymentType() == "elv"}] checked[{/if}] />
    				</dt>
    				<dd>
    					<label for="azIpaymentDebitNote">[{$oDebitNote->oxpayments__oxdesc->value}]</label>
    				</dd>
                    <div class="spacer"></div>
                [{/if}]
				[{* %%CE_ONLY_END%% ####################################################################################################### *}]
            </dl>
         </div>
    </div>

    <div class="groupExp">
        <div>
            <a href="#" onclick="_groupExp(this);return false;" class="rc"><b>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_CHECKOUT"}]</b></a>
            <dl>
                <dt>
					<select class="edittext" name=confbools[az_ipayment_blProcInAdmin]>
						<option value="false">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_CAPTURE_AUTO"}]</option>
						<option value="true" [{if ($confbools.az_ipayment_blProcInAdmin)}]selected[{/if}]>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_CAPTURE_MANUAL"}]</option>
					</select>
                </dt>
                <dd>
                    [{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_CAPTURE"}]
                </dd>
                <div class="spacer"></div>
                <dt>
					<input type="hidden" name="confbools[az_ipayment_blShowServiceErrors]" value="false" />
					<input type="checkbox" class="editinput" name="confbools[az_ipayment_blShowServiceErrors]" id="ipaymentShowErrors" value="true" [{if ($confbools.az_ipayment_blShowServiceErrors)}]checked="checked"[{/if}] />
                </dt>
                <dd>
					<label for="ipaymentShowErrors">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_SHOW_ERRORS"}]</label>
                </dd>
                <div class="spacer"></div>
                <dt>
					<input type=hidden name=confbools[az_ipayment_blDataIframe] value=false>
					<input type=checkbox class="editinput" name="confbools[az_ipayment_blDataIframe]" id="ipaymentDataIFrame" value="true" [{if ($confbools.az_ipayment_blDataIframe)}]checked="checked"[{/if}] />
                </dt>
                <dd>
                    <label for="ipaymentDataIFrame">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_DATA_IFRAME"}]</label>
                    [{oxinputhelp ident="AZ_IPAYMENT_DYN_CONFIG_DATA_IFRAME_INFO"}]
                </dd>
                <div class="spacer"></div>
                <dt>
                    <select id="ipaymentIframeFolder" class="folderselect" name="confstrs[az_ipayment_sIframeFolder]">
                        [{foreach from=$afolder key=field item=color}]
                            <option value="[{ $field }]" [{if $confstrs.az_ipayment_sIframeFolder == $field || ($field|oxmultilangassign == $confstrs.az_ipayment_sIframeFolder)}]SELECTED[{/if}] style="color: [{$color}];">[{oxmultilang ident=$field noerror=true}]</option>
                        [{/foreach}]
                    </select>
                </dt>
                <dd>
                    <label for="ipaymentIframeFolder">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_IFRAME_FOLDER"}]</label>
                    [{oxinputhelp ident="AZ_IPAYMENT_DYN_CONFIG_IFRAME_FOLDER_INFO"}]
                </dd>
                <div class="spacer"></div>
                <dt>
					<input type=hidden name=confbools[az_ipayment_bl3DSecureIframe] value=false>
					<input type=checkbox class="editinput" name="confbools[az_ipayment_bl3DSecureIframe]" id="ipayment3DSecureIFrame" value="true" [{if ($confbools.az_ipayment_bl3DSecureIframe)}]checked="checked"[{/if}] />
                </dt>
                <dd>
                    <label for="ipayment3DSecureIFrame">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_3DSECURE_IFRAME"}]</label>
                    [{oxinputhelp ident="AZ_IPAYMENT_DYN_CONFIG_3DSECURE_IFRAME_INFO"}]
                </dd>
                <div class="spacer"></div>
				[{* %%PRO_ONLY_BEGIN%% #################################################################################################### *}]
                <dt>
					<input type="hidden" name="confbools[az_ipayment_blStoreElvData]" value="false" />
					<input type="checkbox" class="editinput" name="confbools[az_ipayment_blStoreElvData]" id="ipaymentStoreElvData" value="true" [{if ($confbools.az_ipayment_blStoreElvData)}]checked="checked"[{/if}] />
                </dt>
                <dd>
                    <label for="ipaymentStoreElvData">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_STORE_ELV_DATA"}]</label>
                </dd>
                <div class="spacer"></div>
                <dt>
					<input type="hidden" name="confbools[az_ipayment_blStoreCcData]" value="false" />
					<input type="checkbox" class="editinput" name="confbools[az_ipayment_blStoreCcData]" id="ipaymentStoreCcData" value="true" [{if ($confbools.az_ipayment_blStoreCcData)}]checked="checked"[{/if}] />
                </dt>
                <dd>
                    <label for="ipaymentStoreCcData">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_STORE_CC_DATA"}]</label>
                    [{oxinputhelp ident="AZ_IPAYMENT_DYN_CONFIG_STORE_CC_DATA_INFO"}]
                </dd>
                <div class="spacer"></div>
                <dt>
                	[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_INVOICETEXT"}]:
                </dt>
                <dd>
				    [{if !$confstrs.az_ipayment_sInvoiceText || $confstrs.az_ipayment_sInvoiceText == "#SHOP# - BestNr. #ORDERNR#"}]
				    	[{assign var=iAzIpaymentInvoiceChoice value=1}]
				    [{elseif $confstrs.az_ipayment_sInvoiceText == "#SHOP#"}]
				    	[{assign var=iAzIpaymentInvoiceChoice value=2}]
				    [{elseif $confstrs.az_ipayment_sInvoiceText == "KdNr. #CUSTNR# - BestNr. #ORDERNR#"}]
				    	[{assign var=iAzIpaymentInvoiceChoice value=3}]
				    [{else }]
				    	[{assign var=iAzIpaymentInvoiceChoice value=0}]
				    [{/if }]
                </dd>
                <div class="spacer"></div>
                <dt>
				    <input type="radio" class="edittext" name="az_ipayment_sInvoiceChoice" value="#SHOP# - BestNr. #ORDERNR#" id="azIpaymentInvoiceTextRadio1" [{if $iAzIpaymentInvoiceChoice == 1}]checked="checked"[{/if}] onchange="azSetCustomInvoice(this,!this.checked);" />
				</dt>
				<dd>
				    <label for="azIpaymentInvoiceTextRadio1">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_INVOICETEXT_SHOP_ORDERNR"}]</label>
				</dd>
                <div class="spacer"></div>
                <dt>
                	<input type="radio" class="edittext" name="az_ipayment_sInvoiceChoice" value="#SHOP#" id="azIpaymentInvoiceTextRadio2" [{if $iAzIpaymentInvoiceChoice == 2}]checked="checked"[{/if}] onchange="azSetCustomInvoice(this,!this.checked);" />
				</dt>
				<dd>
				    <label for="azIpaymentInvoiceTextRadio2">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_INVOICETEXT_SHOP"}]</label>
				</dd>
                <div class="spacer"></div>
                <dt>
                	<input type="radio" class="edittext" name="az_ipayment_sInvoiceChoice" value="KdNr. #CUSTNR# - BestNr. #ORDERNR#" id="azIpaymentInvoiceTextRadio3" [{if $iAzIpaymentInvoiceChoice == 3}]checked="checked"[{/if}] onchange="azSetCustomInvoice(this,!this.checked);" />
				</dt>
				<dd>
				    <label for="azIpaymentInvoiceTextRadio3">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_INVOICETEXT_CUSTNR_ORDERNR"}]</label>
				</dd>
                <div class="spacer"></div>
                <dt>
                	<input type="radio" class="edittext" name="az_ipayment_sInvoiceChoice" value="" id="azIpaymentInvoiceTextRadio4" [{if !$iAzIpaymentInvoiceChoice}]checked="checked"[{/if}] onchange="azSetCustomInvoice(this,this.checked);" />
				</dt>
				<dd>
				    <label for="azIpaymentInvoiceTextRadio4" id="azIpaymentInvoiceTextLabel">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_INVOICETEXT_CUSTOM"}]</label>
				    <input type="text" class="editinput[{if $iAzIpaymentInvoiceChoice}] disabled[{/if }]" id="azIpaymentInvoiceTextInput" name="confstrs[az_ipayment_sInvoiceText]" size="40" value="[{$confstrs.az_ipayment_sInvoiceText}]" [{if $iAzIpaymentInvoiceChoice}]readonly[{/if}] />
				    [{oxinputhelp ident="AZ_IPAYMENT_DYN_CONFIG_INVOICETEXT_CUSTOM_INFO"}]
				</dd>
                <div class="spacer"></div>
				[{* %%PRO_ONLY_END%% ###################################################################################################### *}]
            </dl>
         </div>
    </div>

	[{* %%PRO_ONLY_BEGIN%% ################################################################################################################ *}]
    <div class="groupExp">
        <div>
            <a href="#" onclick="_groupExp(this);return false;" class="rc"><b>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_LOGGING"}]</b></a>
            <dl>
                <dt>
					<input type="hidden" name="confbools[az_ipayment_blLogPaymentActions]" value="false" />
					<input type="checkbox" class="editinput" name="confbools[az_ipayment_blLogPaymentActions]" id="ipaymentLog" value="true" [{if $confbools.az_ipayment_blLogPaymentActions}]checked="checked"[{/if}] onchange="azSetLogging(this,this.checked);" />
                </dt>
                <dd>
                    <label for="ipaymentLog">[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_LOG"}]</label>
                </dd>
                <div class="spacer"></div>
                <dt>
					<input type="hidden" name="confbools[az_ipayment_blLogParams]" value="false" />
					<input type="checkbox" class="editinput[{if !$confbools.az_ipayment_blLogPaymentActions}] disabled[{/if }]" name="confbools[az_ipayment_blLogParams]" id="ipaymentLogParams" value="true" [{if ($confbools.az_ipayment_blLogParams)}]checked="checked"[{/if}][{if !$confbools.az_ipayment_blLogPaymentActions}] readonly disabled[{/if}] />
                </dt>
                <dd>
                    <label for="ipaymentLogParams" id="ipaymentLogParamsLabel"[{if !$confbools.az_ipayment_blLogPaymentActions}] class="disabled"[{/if}]>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_LOG_PARAMS"}]</label>
                    [{oxinputhelp ident="AZ_IPAYMENT_DYN_CONFIG_LOG_PARAMS_INFO"}]
                </dd>
                <div class="spacer"></div>
                <dt>
					<input type="hidden" name="confbools[az_ipayment_blLogSoapExceptions]" value="false" />
					<input type="checkbox" class="editinput[{if !$confbools.az_ipayment_blLogPaymentActions}] disabled[{/if }]" name="confbools[az_ipayment_blLogSoapExceptions]" id="ipaymentLogSoapExceptions" value="true" [{if ($confbools.az_ipayment_blLogSoapExceptions)}]checked="checked"[{/if}][{if !$confbools.az_ipayment_blLogPaymentActions}] readonly disabled[{/if}] />
                </dt>
                <dd>
                    <label for="ipaymentLogSoapExceptions" id="ipaymentLogSoapExceptionsLabel"[{if !$confbools.az_ipayment_blLogPaymentActions}] class="disabled"[{/if}]>[{oxmultilang ident="AZ_IPAYMENT_DYN_CONFIG_LOG_SOAP"}]</label>
                    [{oxinputhelp ident="AZ_IPAYMENT_DYN_CONFIG_LOG_SOAP_INFO"}]
                </dd>
                <div class="spacer"></div>
            </dl>
         </div>
    </div>
	[{* %%PRO_ONLY_END%% ################################################################################################################## *}]


	<br>
	<input type="submit" class="confinput" name="save" value="[{oxmultilang ident='GENERAL_SAVE'}]" />

</form>

[{*}]
<div align="right">
	<a href="http://www.fatchip.de" target="_blank">
		<img alt="powered by FATCHIP" border="0" src="[{$oViewConf->getImageUrl()}]/powered_by_fatchip_png24_grau.png" />
	</a>
</div>
[{*}]
[{include file="bottomnaviitem.tpl" }]
[{include file="bottomitem.tpl"}]
