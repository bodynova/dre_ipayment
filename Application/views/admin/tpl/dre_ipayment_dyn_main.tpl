[{include file="headitem.tpl" title="AZ_IPAYMENT_DYN_MAIN_TITLE"|oxmultilangassign }]
<script type="text/javascript">
<!--
function SetSticker( sStickerId, oObject)
{
    if (oObject.selectedIndex !== -1) {
        oSticker = document.getElementById(sStickerId);
        oSticker.style.display = "";
        oSticker.style.backgroundColor = "#FFFFCC";
        oSticker.style.borderWidth = "1px";
        oSticker.style.borderColor = "#000000";
        oSticker.style.borderStyle = "solid";
        oSticker.innerHTML         = oObject.item(oObject.selectedIndex).innerHTML;
    }
    else
        oSticker.style.display = "none";
}

function Edit( sID, sCl)
{   var oTransfer = document.getElementById("transfer");
    oTransfer.oxid.value = sID;
    oTransfer.cl.value = sCl;
    oTransfer.target = '_parent';
    oTransfer.submit();    
}

function LoadPayment( oObject)
{   oObject = document.getElementById("paymaster");
	if ( oObject != null && oObject.selectedIndex !== -1)
    {   document.myedit2.oxpaymentid.value = oObject.item(oObject.selectedIndex).value;
    	document.myedit2.fnc.value = "";
    	document.myedit2.submit();
    }
}

function popU(evt,currElem)
{
    title = currElem.getAttribute("caption");
    if (title.length === 0){
        return;
    }
	popUpWin = document.getElementById("ipayment_log_popup");
    popUpWin.innerHTML = title;
    popUpWinStyle = popUpWin.style;
    var y = parseInt(evt.clientY) - 20;
    var x = parseInt(evt.clientX) - 30 - parseInt( popUpWinStyle.width );
    popUpWinStyle.top = y + "px";
    popUpWinStyle.left = x + "px";
    if ( popUpWinStyle.visibility !== "visible" ){
        popUpWinStyle.visibility = "visible";
    }
    window.status = "";
}

function popD(currElem)
{
    var popUpWin = document.getElementById("ipayment_log_popup");
    popUpWin = popUpWin.style;
    popUpWin.visibility = "hidden"
}
//-->
</script>

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="cl" value="dre_Ipayment_Dyn_Main">
</form>

<div>
    <!--
	<div style="float:right">
		<img alt="OXID certified Extension" border="0" src="[{$oViewConf->getImageUrl()}]/certified_extension_100px.png" />
	</div>
	-->
	<h1>
		[{oxmultilang ident="AZ_IPAYMENT_DYN_MAIN_TITLE"}]
	</h1>
	<p>
		[{oxmultilang ident="AZ_IPAYMENT_DYN_MAIN_VERSION"}]: [{$oView->getIpaymentVersion()}] ([{oxmultilang ident="AZ_IPAYMENT_DYN_MAIN_REVISION"}] [{$oView->getIpaymentRevision()}])
	</p>
	<p>
		[{oxmultilang ident="AZ_IPAYMENT_DYN_MAIN_INFO"}]
	</p>
	
	<ul>
		<li>
	    	<a href="[{oxmultilang ident='AZ_IPAYMENT_DYN_MAIN_HOMEPAGE_URL'}]" target="_blank">[{oxmultilang ident="AZ_IPAYMENT_DYN_MAIN_HOMEPAGE"}]</a>
	    </li>
	    <!--
        <li>
	    	<a href="[{oxmultilang ident='AZ_IPAYMENT_DYN_MAIN_LOGINPAGE_URL'}]" target="_blank">[{oxmultilang ident="AZ_IPAYMENT_DYN_MAIN_LOGINPAGE"}]</a>
	    </li>
	    <li>
	    	<a href="[{oxmultilang ident='AZ_IPAYMENT_DYN_MAIN_FATCHIP_WIKI_URL'}]" target="_blank">[{oxmultilang ident="AZ_IPAYMENT_DYN_MAIN_FATCHIP_WIKI"}]</a>
	    </li>
	    -->
	</ul>
</div>
[{*}]
<div align="right">
	<a href="http://www.fatchip.de" target="_blank">
		<img alt="powered by FATCHIP" border="0" src="[{$oViewConf->getImageUrl()}]/powered_by_fatchip_png24_grau.png" />
	</a>
</div>
[{*}]
[{include file="bottomnaviitem.tpl" }]
[{include file="bottomitem.tpl"}]
