[{include file="headitem.tpl" title="[ipayment]"}]
<script type="text/javascript">
<!--
function SetSticker( sStickerId, oObject)
{
    if ( oObject.selectedIndex != -1)
    {   oSticker = document.getElementById(sStickerId);
        oSticker.style.display = "";
        oSticker.style.backgroundColor = "#FFFFCC";
        oSticker.style.borderWidth = "1px";
        oSticker.style.borderColor = "#000000";
        oSticker.style.borderStyle = "solid";
        oSticker.innerHTML         = oObject.item(oObject.selectedIndex).innerHTML;
    }
    else
        oSticker.style.display = "none";
}

function LoadPayment( oObject)
{   oObject = document.getElementById("paymaster");
	if ( oObject != null && oObject.selectedIndex != -1)
    {   document.myedit2.oxpaymentid.value = oObject.item(oObject.selectedIndex).value;
    	document.myedit2.fnc.value = "";
    	document.myedit2.submit();
    }
}

function popU(evt,currElem)
{
    title = currElem.getAttribute("caption");
    if (title.length == 0)
        return;
        
	popUpWin = document.getElementById("ipayment_log_popup");
    popUpWin.innerHTML = title;
    popUpWinStyle = popUpWin.style;
    var y = parseInt(evt.clientY) - 60;
    var x = parseInt(evt.clientX) - 30 - parseInt( popUpWinStyle.width );
    popUpWinStyle.top = y + "px";
    popUpWinStyle.left = x + "px";
    if ( popUpWinStyle.visibility !== "visible" )
        popUpWinStyle.visibility = "visible";
    window.status = "";
}
function popD(currElem)
{
    //var popUpWin = document.getElementById("ttpop");
    var popUpWin = document.getElementById("ipayment_log_popup");
    popUpWin = popUpWin.style;
    popUpWin.visibility = "hidden"
}

function azSwitchDisplay(id1, id2)
{
    var elem1 = document.getElementById(id1);
    var elem2 = document.getElementById(id2);
    if (elem1 && elem1.style)
    {
        if (elem1.style.display == "none")
            elem1.style.display = 'inline';
        else
            elem1.style.display = 'none';
    }
    if (elem2 && elem2.style)
    {
        if (elem2.style.display == "none")
            elem2.style.display = 'inline';
        else
            elem2.style.display = 'none';
    }
}

function Edit( sID, sCl)
{   var oTransfer = document.getElementById("transfer");
    oTransfer.oxid.value = sID;
    oTransfer.cl.value = sCl;
    oTransfer.target = '_parent';
    oTransfer.submit();    
}

function azSort ( sSort )
{
    if ( sSort )
    {
        var sOldAdminOrder = document.search.adminorder.value;
        if (document.search.sort.value == sSort)
            document.search.adminorder.value = document.search.adminorder.value == "1" ? "0" : "1";
        else
            document.search.adminorder.value = "0";
    }
    document.search.sort.value = sSort;
    document.search.submit();
}
//-->
</script>
<style type="text/css">
    .tablecell {padding: 0 5px; empty-cells: show;}
</style>

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

<div id="ipayment_log_popup" class="edittext" style="position:absolute;width:250px;visibility:hidden;background-color:#f0f0f0;border:1px solid #c8c8c8;padding:5px;">
</div>

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="cl" value="dre_Ipayment_Dyn_Log">
</form>
<form name="search" id="search" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="cl" value="dre_Ipayment_Dyn_Log">
    <input type="hidden" name="lstrt" value="[{$lstrt}]">
    <input type="hidden" name="sort" value="[{$sort}]">
    <input type="hidden" name="adminorder" value="[{$adminorder}]">
    <input type="hidden" name="actedit" value="[{$actedit}]">
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="fnc" value="">

<div id="liste">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr class="listitem">
		<td valign="top" class="listfilter first" height="20">
			<div class="r1"><div class="b1">
				<input class="listedit" type="text" size="15" maxlength="128" name="where[dre_ipayment_logs[{$nameconcat}]oxtime]" value="[{$where.dre_ipayment_logs.oxtime}]">
			</div></div>
		</td>
		<td valign="top" class="listfilter" height="20">
			<div class="r1"><div class="b1">
				<input class="listedit" type="text" size="15" maxlength="128" name="where[dre_ipayment_logs[{$nameconcat}]oxorderid]" value="[{$where.dre_ipayment_logs.oxorderid}]">
			</div></div>
		</td>
		<td valign="top" class="listfilter" height="20">
			<div class="r1"><div class="b1">
				<input class="listedit" type="text" size="15" maxlength="128" name="where[dre_ipayment_logs[{$nameconcat}]oxtransid]" value="[{$where.dre_ipayment_logs.oxtransid}]">
			</div></div>
		</td>
		<td valign="top" class="listfilter" height="20">
			<div class="r1"><div class="b1">
				<input class="listedit" type="text" size="15" maxlength="128" name="where[dre_ipayment_logs[{$nameconcat}]oxpayid]" value="[{$where.dre_ipayment_logs.oxpayid}]">
			</div></div>
		</td>
		<td valign="top" class="listfilter" height="20">
			<div class="r1"><div class="b1">
				<input class="listedit" type="text" size="20" maxlength="128" name="where[dre_ipayment_logs[{$nameconcat}]oxuid]" value="[{$where.dre_ipayment_logs.oxuid}]">
			</div></div>
		</td>
		<td valign="top" class="listfilter" height="20">
			<div class="r1"><div class="b1">
				<input class="listedit" type="text" size="15" maxlength="128" name="where[dre_ipayment_logs[{$nameconcat}]oxamount]" value="[{$where.dre_ipayment_logs.oxamount}]">
			</div></div>
		</td>
		<td valign="top" class="listfilter" height="20">
			<div class="r1"><div class="b1">
				<input class="listedit" type="text" size="10" maxlength="128" name="where[dre_ipayment_logs[{$nameconcat}]oxsituation]" value="[{$where.dre_ipayment_logs.oxsituation}]">
			</div></div>
		</td>
		<td valign="top" class="listfilter" height="20">
			<div class="r1"><div class="b1">
				<input class="listedit" type="text" size="10" maxlength="128" name="where[dre_ipayment_logs[{$nameconcat}]oxerrcode]" value="[{$where.dre_ipayment_logs.oxerrcode}]">
			</div></div>
		</td>
		<td valign="top" class="listfilter last" height="20">
			<div class="r1"><div class="b1">
				<div style="float:left;">
				<input class="listedit" type="text" size="20" maxlength="128" name="where[dre_ipayment_logs[{$nameconcat}]oxerrtext]" value="[{$where.dre_ipayment_logs.oxerrtext}]">
				</div>
				<div class="find"><input class="listedit" type="submit" name="submitit" value="[{oxmultilang ident="GENERAL_SEARCH"}]" title="[{oxmultilang ident="GENERAL_SEARCH"}]"></div>
			</div></div>
		</td>
	</tr>
	<tr>
		<td class="listheader first" height="15"><a href="Javascript:azSort('dre_ipayment_logs.oxtime');" class="listheader">[{oxmultilang ident="AZ_IPAYMENT_DYN_TIME"}]</a></td>
		<td class="listheader" height="15"><a href="Javascript:azSort('dre_ipayment_logs.oxorderid');" class="listheader">[{oxmultilang ident="AZ_IPAYMENT_DYN_ORDERNR"}]</a></td>
		<td class="listheader" height="15"><a href="Javascript:azSort('dre_ipayment_logs.oxtransid');" class="listheader">[{oxmultilang ident="AZ_IPAYMENT_DYN_TRANSACTIONID"}]</a></td>
		<td class="listheader" height="15"><a href="Javascript:azSort('dre_ipayment_logs.oxpayid');" class="listheader">[{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMENTMETHOD"}]</a></td>
		<td class="listheader" height="15"><a href="Javascript:azSort('dre_ipayment_logs.oxuid');" class="listheader">[{oxmultilang ident="AZ_IPAYMENT_DYN_USEREMAIL"}]</a></td>
		<td class="listheader" height="15"><a href="Javascript:azSort('dre_ipayment_logs.oxamount');" class="listheader">[{oxmultilang ident="AZ_IPAYMENT_DYN_TOTAL"}]</a></td>
		<td class="listheader" height="15"><a href="Javascript:azSort('dre_ipayment_logs.oxsituation');" class="listheader">[{oxmultilang ident="AZ_IPAYMENT_DYN_SITUATION"}]</a></td>
		<td class="listheader" height="15"><a href="Javascript:azSort('dre_ipayment_logs.oxerrcode');" class="listheader">[{oxmultilang ident="AZ_IPAYMENT_DYN_ERRORCODE"}]</a></td>
		<td class="listheader last" height="15"><a href="Javascript:azSort('dre_ipayment_logs.oxerrtext');" class="listheader">[{oxmultilang ident="AZ_IPAYMENT_DYN_ERRORTEXT"}]</a></td>
	</tr>

	<style>
        .tablecell.odd { background-color:#fff; vertical-align:top; }
        .tablecell.even { background-color:#ddf; vertical-align:top; }
    </style>
    [{foreach from=$mylist item=oItem name="dre_ipayment_log_loop"}]
        [{if $sAzLogLineClass == "odd"}]
            [{assign var="sAzLogLineClass" value="even"}]
        [{else}]
            [{assign var="sAzLogLineClass" value="odd"}]
        [{/if}]
        <tr>
            <td class="tablecell [{$sAzLogLineClass}]" style="border-left: 1px solid #c0c0c0;">[{$oItem->getDateTime()}]</td>
            <td class="tablecell [{$sAzLogLineClass}]" style="text-align:right;">[{$oItem->getOrderNr()}]</td>
            <td class="tablecell [{$sAzLogLineClass}]">[{$oItem->dre_ipayment_logs__oxtransid->value}]</td>
            <td class="tablecell [{$sAzLogLineClass}]">[{$oItem->getPaymentDesc()|truncate:12}]</td>
            <td class="tablecell [{$sAzLogLineClass}]">[{$oItem->getUserEmailAddress()}]</td>
            <td class="tablecell [{$sAzLogLineClass}]" style="text-align:right;">[{$oItem->getFAmount()}] [{$oItem->dre_ipayment_logs__oxcurr->value}]</td>
            <td class="tablecell [{$sAzLogLineClass}]">[{$oItem->getSituation()}]</td>
            <td class="tablecell [{$sAzLogLineClass}]" style="text-align:right;">[{$oItem->dre_ipayment_logs__oxerrcode->value}]</td>
            <td class="tablecell [{$sAzLogLineClass}]" style="cursor:pointer; border-right: 1px solid #c0c0c0;" onclick="azSwitchDisplay('dre_ipayment_errortextshort_[{$oItem->dre_ipayment_logs__oxid->value}]','dre_ipayment_errortextlong_[{$oItem->dre_ipayment_logs__oxid->value}]')">
                [{assign var=sAzErrorText value=$oItem->dre_ipayment_logs__oxerrtext->value|regex_replace:"/[\r\t\n]/":"<br>"}]
                <div id="dre_ipayment_errortextshort_[{$oItem->dre_ipayment_logs__oxid->value}]" style="display:inline;" title="[{oxmultilang ident="AZ_IPAYMENT_DYN_CLICK_TO_SHOW"}]">[{$sAzErrorText|truncate:25}]</div>
                <div id="dre_ipayment_errortextlong_[{ $oItem->dre_ipayment_logs__oxid->value }]" style="display:none;" title="[{oxmultilang ident="AZ_IPAYMENT_DYN_CLICK_TO_HIDE"}]">[{$sAzErrorText}]</div>
            </td>
        </tr>
    [{/foreach}]

	[{include file="pagenavisnippet.tpl" colspan="9"}]

</table>
</div>
</form>

<div>
	<ul>
		<li>
			<a href="https://ipayment.de/technik/errorlist.php?pm_id=1&language_id=0" target="_blank">Ipayment Fehlercodes Kreditkartenzahlung</a>
		</li> 
	</ul>
</div>
[{*}]
<div align="right">
	<a href="http://www.fatchip.de" target="_blank">
		<img alt="powered by FATCHIP" border="0" src="[{$oViewConf->getImageUrl()}]/powered_by_fatchip_png24_grau.png" />
	</a>
</div>
[{*}]

[{include file="bottomnaviitem.tpl" }]
[{include file="bottomitem.tpl"}]
