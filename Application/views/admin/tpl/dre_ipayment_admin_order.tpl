[{include file="headitem.tpl" title="[ipayment]"}]

[{if $edit && $payment}]
	[{if $payment->getIpaymentType()}]
		[{assign var=blIsIpayment value=true}]
	[{/if}]
[{/if}]

<script type="text/javascript">
<!--

function CaptureNow()
{
    blCheck = confirm('[{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONBOOKPAYMENT"}]');
    if( blCheck == true)
    {
        var oTransfer = document.getElementById("transfer");
        oTransfer.fnc.value='captureNow';
        oTransfer.submit();
    }
}

function ReverseNow()
{
    blCheck = confirm('[{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONCANCELPAYMENT"}]');
    if( blCheck == true)
    {
        var oTransfer = document.getElementById("transfer");
        oTransfer.fnc.value='reverseNow';
        oTransfer.submit();
    }
}

function ReverseManual()
{
    blCheck = confirm('[{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONCANCELPAYMENTMANUAL"}]');
    if( blCheck == true)
    {
        var oTransfer = document.getElementById("transfer");
        oTransfer.fnc.value='reverseManual';
        oTransfer.submit();
    }
}

function RefundNow()
{
    blCheck = confirm('[{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONPAYMONEYBACK"}]');
    if( blCheck == true)
    {
        var oTransfer = document.getElementById("transfer");
        oTransfer.fnc.value='refundNow';
        oTransfer.submit();
    }
}

function azSwitchDisplay ( id1, id2 )
{
    var elem1 = document.getElementById( id1 );
    var elem2 = document.getElementById( id2 );
    if ( elem1 && elem1.style )
    {
        if ( elem1.style.display == "none" )
            elem1.style.display = 'inline';
        else
            elem1.style.display = 'none';
    }
    if ( elem2 && elem2.style )
    {
        if ( elem2.style.display == "none" )
            elem2.style.display = 'inline';
        else
            elem2.style.display = 'none';
    }
}

function popU(evt,currElem)
{
    title = currElem.getAttribute("caption");
    if (title.length == 0)
        return;
        
	popUpWin = document.getElementById("ipayment_log_popup");
    //oHeightObject = document.getElementById("popheight");
    popUpWin.innerHTML = title;
    popUpWinStyle = popUpWin.style;
    var y = parseInt(evt.clientY) - 20;// - oHeightObject.height;
    var x = parseInt(evt.clientX) - 30 - parseInt( popUpWinStyle.width );
	/*
	if(document.all){
		if ( x > document.body.clientWidth - 150 ){
	        x = parseInt(document.body.clientWidth) - 100;
            y = y - 15;
    	}		
	}
	else{
	    if ( x > self.innerWidth - 100 ){
	        x = parseInt(self.innerWidth) - 100;
	    }
    }
    popUpWinStyle.top  = Math.max(2,y)-20;
    popUpWinStyle.left = Math.max(2,x)-250-30;
    */
    popUpWinStyle.top = y + "px";
    popUpWinStyle.left = x + "px";
    if ( popUpWinStyle.visibility != "visible" )
        popUpWinStyle.visibility = "visible";
    window.status = "";
}
function popD(currElem)
{
    //var popUpWin = document.getElementById("ttpop");
    var popUpWin = document.getElementById("ipayment_log_popup");
    popUpWin = popUpWin.style;
    popUpWin.visibility = "hidden"
}
//-->
</script>

<style type="text/css">
    .tableheader { border: 1px solid #404040; border-right: 0; padding:2px;empty-cells:show;}
    .tablecell { border: 1px solid #c0c0c0; border-right: 0; border-top: 0; padding:2px;empty-cells:show;}
    .cellcounter { text-align: center; font-weight: bold;background-color:#d4d0c8; border: 1px solid #404040; border-top: 0; border-right: 0px; padding:2px;empty-cells:show;}
    .pagenavigation { background-color: transparent;}
</style>
<div id="ipayment_log_popup" class="edittext" style="position:absolute;width:250px;visibility:hidden;background-color:#f0f0f0;border:1px solid #c8c8c8;padding:5px;">
</div>

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{ $oViewConf->getHiddenSid() }]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="cl" value="dre_Ipayment_AdminDetailsController">
    <input type="hidden" name="fnc" value="">
</form>

[{if $blIsIpayment}]
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
      <td valign="top" class="edittext">
        [{if $edit->oxorder__oxtransstatus->value == "ERROR"}]
            <strong>[{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMENTERROR"}]</strong>
            [{if $blIs3dSecure}]
                <br />
                <strong style="color:#a40;">[{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMENTERROR_3DSECURE"}]</strong>
            [{/if }]
        [{/if}]
        [{if $edit->oxorder__oxtransid->value && $edit->oxorder__oxxid->value < 2}]

            [{if $edit->oxorder__oxpaid->value == "0000-00-00 00:00:00"}]
                [{if $edit->oxorder__oxxid->value == 1}]
                <strong>[{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMENTONLYRESERVED"}]</strong><br />
                <ul>
                <li>
                <form action="JavaScript:CaptureNow();">
                    [{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONWANTTODOBOOKING"}]
                    <input type="submit" value="[{oxmultilang ident="AZ_IPAYMENT_DYN_PERFORMBOOKING"}]" />
                </form>
                </li>
                <li>
                <form action="JavaScript:ReverseNow();">
                    [{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONWANTTODOCANCELATION"}]
                    <input type="submit" value="[{oxmultilang ident="AZ_IPAYMENT_DYN_PERFORMCANCELATION"}]" />
                </form>
                </li>
                </ul>
                [{/if}]
            [{else}]
                <strong>[{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMENTHAPPEND"}]: [{$edit->oxorder__oxpaid->value}]</strong><br />
                <ul>
                <li>
                <form action="JavaScript:RefundNow();">
                    [{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONPAYMONEYBACKHERE"}]
                    <input type="submit" value="[{oxmultilang ident="AZ_IPAYMENT_DYN_PERFORMREFUND"}]" />
                </form>
                </li>
                <li>
                <form action="JavaScript:ReverseManual();">
                    <div style="float:left;">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONORDERMANUALRESET"}]<br />
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONORDERMANUALRESET_NOTE"}]
                    </div>
                    <div>
                        <input type="submit" value="[{oxmultilang ident="AZ_IPAYMENT_DYN_PERFORMMANUALRESET"}]" />
                    </div>
                </form>
                </li>
                </ul>
            [{/if}]

            [{if $debugdata}]
                <textarea class="edittext" style="width:90%;height:50%">
                    [{foreach from=$debugdata item=curritem}]
                        [{$curritem}]
                    [{/foreach}]
                </textarea>
            [{/if}]

        [{else}]

            [{if $edit->oxorder__oxxid->value == 2}]
                <strong>[{oxmultilang ident="AZ_IPAYMENT_DYN_ORDERCANCELED"}]</strong><br />
            [{elseif  $edit->oxorder__oxxid->value == 3}]
                <strong>[{oxmultilang ident="AZ_IPAYMENT_DYN_ORDERREFUNDED"}]</strong><br />
            [{/if}]
            [{if $edit->oxorder__oxpaid->value != "0000-00-00 00:00:00"}]
                <ul>
                <li>
                <form action="JavaScript:ReverseManual();">
                    <div style="float:left;">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONORDERMANUALRESET"}]
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_QUESTIONORDERMANUALRESET_NOTE"}]
                    </div>
                    <div>
                        <input type="submit" value="[{oxmultilang ident="AZ_IPAYMENT_DYN_PERFORMMANUALRESET"}]" />
                    </div>
                </form>
                </li>
                </ul>
            [{/if}]
        [{/if}]

        [{if $eccstatus == "OK" }]
            <div style="border-color:#0a0; border-style:solid; border-width:1px 5px; padding:5px; margin-top:30px; font-weight:bold;">
                [{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMENTSUCCESSFUL"}]
            </div>
        [{elseif $eccstatus == "FAILED" }]
            <div style="border-color:#a00; border-style:solid; border-width:1px 5px; padding:5px; margin-top:30px; font-weight:bold;">
                [{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMENTFAILURE"}]
                [{if $sDescription}]
                    <br /><br />
                    <em>[{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMASTERINFORMATION"}]: [{$sDescription}]</em>
                [{/if}]
            </div>
        [{/if}]
        [{if $ecrsstatus == "OK" }]
            <div style="border-color:#0a0; border-style:solid; border-width:1px 5px; padding:5px; margin-top:30px; font-weight:bold;">
                [{oxmultilang ident="AZ_IPAYMENT_DYN_CANCELATIONSUCCESSFUL"}]
            </div>
        [{elseif $ecrsstatus == "FAILED" }]
            <div style="border-color:#a00; border-style:solid; border-width:1px 5px; padding:5px; margin-top:30px; font-weight:bold;">
                [{oxmultilang ident="AZ_IPAYMENT_DYN_CANCELATIONFAILED"}]
                [{if $sDescription}]
                    <br /><br />
                    <em>[{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMASTERINFORMATION"}]: [{$sDescription}]</em>
                [{/if}]
            </div>
        [{/if}]
        [{if $ecrfstatus == "OK" }]
            <div style="border-color:#0a0; border-style:solid; border-width:1px 5px; padding:5px; margin-top:30px; font-weight:bold;">
                [{oxmultilang ident="AZ_IPAYMENT_DYN_REFUNDSUCCESSFUL"}]
            </div>
        [{elseif $ecrfstatus == "FAILED" }]
            <div style="border-color:#a00; border-style:solid; border-width:1px 5px; padding:5px; margin-top:30px; font-weight:bold;">
                [{oxmultilang ident="AZ_IPAYMENT_DYN_REFUNDNOTHAPPEND"}]
                [{if $sDescription}]
                    <br /><br />
                    <em>[{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMASTERINFORMATION"}]: [{$sDescription}]</em>
                [{/if}]
            </div>
        [{/if}]
        </td>
        <td align="right" valign="top">
            <img src="[{$oViewConf->getImageUrl()}]/az_ipayment_logo.jpg" alt="ipayment" border="0">
        </td>
    </tr>
    </table>

    <style type="text/css">
        .tablecell { padding: 0 5px; empty-cells: show; }
    </style>
    <div id="liste" style="margin-top:50px;">
        <table cellspacing="0" cellpadding="0" border="0">
            <tr class="listitem">
                <td valign="top" class="listfilter first" height="20">
                    <div class="r1"><div class="b1">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_TIME"}]
                    </div></div>
                </td>
                <td valign="top" class="listfilter" height="20">
                    <div class="r1"><div class="b1">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_ORDERNR"}]
                    </div></div>
                </td>
                <td valign="top" class="listfilter" height="20">
                    <div class="r1"><div class="b1">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_TRANSACTIONID"}]
                    </div></div>
                </td>
                <td valign="top" class="listfilter" height="20">
                    <div class="r1"><div class="b1">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_PAYMENTMETHOD"}]
                    </div></div>
                </td>
                <td valign="top" class="listfilter" height="20">
                    <div class="r1"><div class="b1">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_USEREMAIL"}]
                    </div></div>
                </td>
                <td valign="top" class="listfilter" height="20">
                    <div class="r1"><div class="b1">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_TOTAL"}]
                    </div></div>
                </td>
                <td valign="top" class="listfilter" height="20">
                    <div class="r1"><div class="b1">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_SITUATION"}]
                    </div></div>
                </td>
                <td valign="top" class="listfilter" height="20">
                    <div class="r1"><div class="b1">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_ERRORCODE"}]
                    </div></div>
                </td>
                <td valign="top" class="listfilter last" height="20">
                    <div class="r1"><div class="b1">
                        [{oxmultilang ident="AZ_IPAYMENT_DYN_ERRORTEXT"}]
                    </div></div>
                </td>
            </tr>

            <style>
                .tablecell.odd { background-color:#fff; vertical-align:top; }
                .tablecell.even { background-color:#ddf; vertical-align:top; }
            </style>
            [{foreach from=$oView->getIpaymentLogs() item=oItem name="az_ipayment_log_loop"}]
                [{if $sAzLogLineClass == "odd"}]
                    [{assign var="sAzLogLineClass" value="even"}]
                [{else}]
                    [{assign var="sAzLogLineClass" value="odd"}]
                [{/if}]
                <tr>
                    <td class="tablecell [{$sAzLogLineClass }]" style="border-left: 1px solid #c0c0c0;">[{$oItem->getDateTime()}]</td>
                    <td class="tablecell [{$sAzLogLineClass }]" style="text-align:right;">[{$oItem->getOrderNr()}]</td>
                    <td class="tablecell [{$sAzLogLineClass }]">[{$oItem->az_ipayment_logs__oxtransid->value}]</td>
                    <td class="tablecell [{$sAzLogLineClass }]">[{$oItem->getPaymentDesc()|truncate:12}]</td>
                    <td class="tablecell [{$sAzLogLineClass }]">[{$oItem->getUserEmailAddress()}]</td>
                    <td class="tablecell [{$sAzLogLineClass }]" style="text-align:right;">[{$oItem->getFAmount()}] [{$oItem->az_ipayment_logs__oxcurr->value}]</td>
                    <td class="tablecell [{$sAzLogLineClass }]">[{$oItem->getSituation()}]</td>
                    <td class="tablecell [{$sAzLogLineClass }]" style="text-align:right;">[{$oItem->az_ipayment_logs__oxerrcode->value}]</td>
                    <td class="tablecell [{$sAzLogLineClass }]" style="cursor:pointer; border-right: 1px solid #c0c0c0;" onclick="azSwitchDisplay('az_ipayment_errortextshort_[{$oItem->az_ipayment_logs__oxid->value}]','az_ipayment_errortextlong_[{$oItem->az_ipayment_logs__oxid->value}]')">
                        [{assign var=sAzErrorText value=$oItem->az_ipayment_logs__oxerrtext->value|regex_replace:"/[\r\t\n]/":"<br>"}]
                        <div id="az_ipayment_errortextshort_[{$oItem->az_ipayment_logs__oxid->value}]" style="display:inline;" title="[{oxmultilang ident="AZ_IPAYMENT_DYN_CLICK_TO_SHOW"}]">[{$sAzErrorText|truncate:25}]</div>
                        <div id="az_ipayment_errortextlong_[{$oItem->az_ipayment_logs__oxid->value}]" style="display:none;" title="[{oxmultilang ident="AZ_IPAYMENT_DYN_CLICK_TO_HIDE"}]">[{$sAzErrorText}]</div>
                    </td>
                </tr>
            [{/foreach}]

        </table>
    </div>

[{else}]
	[{oxmultilang ident="AZ_IPAYMENT_DYN_NOIPAYMENT"}]
[{/if}]
    
[{include file="bottomnaviitem.tpl" navigation=order}]
[{include file="bottomitem.tpl"}]
