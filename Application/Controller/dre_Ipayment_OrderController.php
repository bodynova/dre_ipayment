<?php

namespace Bender\dre_Ipayment\Application\Controller;


class dre_Ipayment_OrderController extends \OxidEsales\Eshop\Application\Controller\OrderController { //dre_Ipayment_OrderController_parent {
    /**
     * Clears the session challenge if the user tries to submit a pending 3D-Secure order again (e.g. after aborting the 3D-Secure bank website).
     * 
     * @extend render
     * 
     * @return string template
     */
    public function render ()
    {
        $this->_azClearPending3dSecureOrder();
        
        return parent::render();
    }

    /**
     * Show 3D-Secure page if the order received a 3D-Secure redirect from ipayment.
     * 
     * @extend _getNextStep
     * 
     * @param int $iSuccess order success/error code
     * @return string redirect class
     */
    protected function _getNextStep( $iSuccess )
    {
        $oPayment = $this->getPayment();
        
        if($oPayment) {
            $sRedirectData = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_sRedirectData' );
            if ( $sRedirectData ) {
                return 'az_ipayment_redirect3dsecure';
            }
        }
        
        $sNextStep = parent::_getNextStep( $iSuccess );
        if($sNextStep === 'thankyou') {
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_blBrokeOutOfIframe' );
        }
        return $sNextStep;
    }

    /**
     * Executes an order upon successful return from the 3D-Secure page.
     * This is basically the same code that the execute() method uses to execute the order.
     * 
     * @return string class redirect
     */
    public function azExecute3dSecure()
    {
        // additional check if we really really have a user now
        if ( !$oUser= $this->getUser() ) {
            return 'user';
        }

        // get basket contents
        $oBasket = $this->getSession()->getBasket();
        if ($oBasket->getProductsCount()) {
            try {
                $oOrder = oxNew(\OxidEsales\Eshop\Application\Model\Order::class);

                //finalizing ordering process (validating, storing order into DB, executing payment, setting status ...)
                $iSuccess = $oOrder->finalizeOrder($oBasket, $oUser);

                // performing special actions after user finishes order (assignment to special user groups)
                $oUser->onOrderExecute($oBasket, $iSuccess);

                // proceeding to next view
                return $this->_getNextStep($iSuccess);
            } catch (\OxidEsales\Eshop\Core\Exception\OutOfStockException $oEx) {
                $oEx->setDestination('basket');
                \OxidEsales\Eshop\Core\Registry::getUtilsView()->addErrorToDisplay($oEx, false, true, 'basket');
            } catch (\OxidEsales\Eshop\Core\Exception\NoArticleException $oEx) {
                \OxidEsales\Eshop\Core\Registry::getUtilsView()->addErrorToDisplay($oEx);
            } catch (\OxidEsales\Eshop\Core\Exception\ArticleInputException $oEx) {
                \OxidEsales\Eshop\Core\Registry::getUtilsView()->addErrorToDisplay($oEx);
            }
        }

    }

    /**
     * Clean up after a pending 3D-Secure order.
     * 
     * @return null
     */
    protected function _azClearPending3dSecureOrder ()
    {
        $s3dSecureOrderId = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_sRedirectedOrder' );
        if ( $s3dSecureOrderId && $s3dSecureOrderId == \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'sess_challenge' ) )
        {
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'sess_challenge' );
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_sRedirectedOrder' );
        }
    }
}
