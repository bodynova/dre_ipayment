<?php
namespace Bender\dre_Ipayment\Application\Controller;

use Bender\dre_Ipayment\Application\Model\dre_Ipayment;
use Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log;
use OxidEsales\Eshop\Application\Model\Order;

class dre_Ipayment_PaymentController extends \OxidEsales\Eshop\Application\Controller\PaymentController{ //dre_Ipayment_PaymentController_parent {

    /**
     * Country object
     *
     * @var oxCountry
     */
    protected $_oCountry;

    /**
     * State object
     * @var oxState
     */
    protected $_oState;

    /**
     * Default error code
     * @var integer
     */
    protected $_iDefIpaymentErrCode = 999;

    /**
     * Next step class
     * @var string
     */
    protected $_sSuccessAction = "order";

    /**
     * Base transaction type
     * @var string 
     */
    protected $_sTransactionType = "BASE_CHECK";

    /**
     * Transaction ID
     * @var string
     */
    protected $_sTransactionId = null;    

    protected $_sFcCustomError = false;

    /**
     * Executes parent::init
     * 
     * @extend init
     * 
     * @return null
     */
    public function init(){
        parent::init();
        
        // setting parent class
        $this->_sThisAction = "payment";
    }

    protected function _checkIframeLeaveWithExistingOrder() {
        if(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable('sFcIpaymentUserIsOnIframe')) {
            $sOrderId = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable('sess_challenge');
            if($sOrderId) {
                $oOrder = oxNew(Order::class);
                if($oOrder->load($sOrderId)) {
                    $oOrder->delete();
                }
            }
        }
    }
    
    /**
     * Executes parent class, loads ipayment data and passes them to the template engine.
     * Returns the name of the template to render.
     * 
     * @extend render
     * 
     * @return string template file name
     */
    public function render ()
    {
        $blIpaymentError = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_blPaymentError' );
        
        $this->_checkIframeLeaveWithExistingOrder();

        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_blPaymentError' );
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_blBrokeOutOfIframe' );
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'sFcDataIframeRedirectUrl' );
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'sFcIpaymentUserIsOnIframe' );
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'sFcIsDataIframeReturn' );
        
        $sTemplate = parent::render();

        // check whether the current payment is an ipayment payment:
        if($this->_azCheckIpayment()){
            // setup ipayment _aViewData:
            $this->_azSetIpaymentViewData();
        }
        
        // handling payment error codes:
        if ( $blIpaymentError ) {
            $this->_azHandleIpaymentError();
        }elseif(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'fc_ipayment_blHasMismatchError' )){
            $this->_fcSetCustomError(\OxidEsales\Eshop\Core\Registry::getLang()->translateString('AZ_IPAYMENT_ERROR_AMOUNT_MISMATCH'));
        }
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'fc_ipayment_blHasMismatchError' );
        
        return $sTemplate;
    }

    /**
     * Checks whether any ipayment payment methods are shown.
     * 
     * @return bool true if there are ipayment payment methods, false otherwise
     */
    protected function _azCheckIpayment (){
        if ( !$this->getConfig()->getShopConfVar('az_ipayment_blActive')){
            return false;
        }
        
        $this->_aViewData['az_ipayment_blActive'] = true;
        
        $oPayments = $this->getPaymentList();
        return !empty($oPayments);
    }

    protected function _fcGetBasketPrice() {
        $oBasket = $this->getSession()->getBasket();

        $oIpayment = oxNew(dre_Ipayment::class);
        $oCurrency = $this->getConfig()->getActShopCurrencyObject();
        $sBasketPrice = $oIpayment->getIpaymentPrice($oBasket->getPrice()->getBruttoPrice(), $oCurrency->name);
        return $sBasketPrice;
    }
    
    /**
     * Sets up template data (_aViewData) with ipayment information of the current payment.
     *
     * @return null
     */
    protected function _azSetIpaymentViewData (){
    	$oConfig = $this->getConfig();
        $oUser = $this->getUser();
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(dre_Ipayment::getFetchMode());
        $sBasketPrice = $this->_fcGetBasketPrice();
        
        // passing ipayment configuration parameters:
        $this->_aViewData["ipayment_pw"] = $oConfig->getConfigParam('az_ipayment_sPassword');
        $this->_aViewData["ipayment_user"] = $oConfig->getConfigParam('az_ipayment_sUser');
        $this->_aViewData["ipayment_userip"] = \OxidEsales\Eshop\Core\Registry::getUtilsServer()->getRemoteAddress();
        $this->_aViewData["ipayment_posturl"] = "https://ipayment.de/merchant/".$oConfig->getConfigParam('az_ipayment_sAccount')."/processor/2.0/";
        //$this->_aViewData["ipayment_posturl"] = "https://ipayment.de/merchant/".$oConfig->getConfigParam('az_ipayment_sAccount')."/processor.php";
        $this->_aViewData["ipayment_transid"] = $this->_sTransactionId;
        $this->_aViewData["ipayment_transtype"] = strtolower($this->_sTransactionType);
        $this->_aViewData["ipayment_serrorrurl"] = "cl=".$this->getThisAction()."&fnc=processError";
        $this->_aViewData["ipayment_orderprice"] = $sBasketPrice;
        $this->_aViewData["ipayment_htriggerurl"] = "cl=".$this->getThisAction()."&fnc=confirmOrder";
        $this->_aViewData["ipayment_redirecturl"] = "cl=".$this->getThisAction()."&fnc=processResponse";
        $this->_aViewData["ipayment_datastorage_expirydate"] = "+1 day";
        $this->_aViewData["ipayment_error_lang"] = dre_Ipayment::getLanguageCode();
        
        if ( $oUser->oxuser__oxstateid->value ) {
            $this->_aViewData['ipayment_state'] = $oDb->getOne( "SELECT oxisoalpha2 FROM " . getViewName('oxstates') . " WHERE oxid = " . $oDb->quote($oUser->oxuser__oxstateid->value));
        }
        if ( $oUser->oxuser__oxcountryid->value ) {
            $this->_aViewData['ipayment_country'] = $oDb->getOne( "SELECT oxisoalpha2 FROM " . getViewName('oxcountry') . " WHERE oxid = " . $oDb->quote($oUser->oxuser__oxcountryid->value));
        }
        
        // %%PRO_ONLY_BEGIN%% ####################################################################################################
        if ( $oConfig->getConfigParam('az_ipayment_blStoreCcData') || $oConfig->getConfigParam('az_ipayment_blStoreElvData'))
            $this->_aViewData['ipayment_return_paymentdata'] = true;
        
        // %%PRO_ONLY_END%% ######################################################################################################
        // parameters used by ipayment to identify the module:
        $this->_aViewData["ipayment_client_name"]    = dre_Ipayment::getClientName();
        $this->_aViewData["ipayment_client_version"] = dre_Ipayment::getClientVersion();
        $this->_aViewData['ipayment_ppcbedc'] = dre_Ipayment::getIpaymentPpcbedc();
        
        // handle ipayment security key:
        if ( $oConfig->getConfigParam( 'az_ipayment_sSecKey' ) ) {
            $sSecurityStr  = $oConfig->getConfigParam( 'az_ipayment_sUser' );
            $sSecurityStr .= $sBasketPrice;
            $sSecurityStr .= $oConfig->getActShopCurrencyObject()->name;
            $sSecurityStr .= $oConfig->getConfigParam( 'az_ipayment_sPassword' );
            $sSecurityStr .= $oConfig->getConfigParam( 'az_ipayment_sSecKey' );
            $this->_aViewData["ipayment_security"] = md5( $sSecurityStr );
        }
        
        // change link to the order overview page to submit the currently selected payment method form:
        $this->getViewConfig()->setViewConfigParam('orderconfirmlink', 'javascript:submitActiveForm();//');
    }

    /**
     * Handles ipayment errors (sets up error message and error code for the template.
     *
     * @return null
     */
    protected function _azHandleIpaymentError ()
    {
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_blPaymentError');
        
        if ( !$this->getPaymentError() )
            return;
        
        $aIpayment = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_aData');
        if ( isset( $aIpayment ) ) {
            // use error code -1 to show the payment gateway error message:
            $this->_sPaymentError = -1;
        	$this->_sPaymentErrorText = dre_Ipayment::getPaymentErrorText($aIpayment['ret_errorcode'], $aIpayment['ret_fatalerror'] ? true : false, $aIpayment);

            // clear ipayment data on error:
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_aData' );
        }else{
            $sPaymentErrorText = dre_Ipayment::getPaymentErrorText($this->getPaymentError(), false);
            if ($sPaymentErrorText){
            	$this->_sPaymentError = -1;
        	    $this->_sPaymentErrorText = $sPaymentErrorText;
            }
        }
    }

    protected function _fcSetCustomError($sError) {
        $this->_sFcCustomError = $sError;
    }
    
    public function fcGetCustomError() {
        return $this->_sFcCustomError;
    }
    
    /**
     * As we need additional response parameters and do not want to leave them to user output,
     * we should save these parameters and redirect to order class.
     * 
     * @return string class to load for next step (in this case proceed to order step)
     */
    public function processResponse ()
    {
        $oConfig = $this->getConfig();
        $oSession = $this->getSession();
        $oBasket = $oSession->getBasket();

        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_blPaymentError' );
        
        // saving parameters
        $aDataSource = null;
        if (isset($_GET)){
            $aDataSource = & $_GET;
        }elseif(isset($HTTP_GET_VARS)){
            $aDataSource = & $HTTP_GET_VARS;
        }

        $aIpayment = array();
        if (is_array($aDataSource)){
            foreach($aDataSource as $sKey => $sVal) {
                $aIpayment[$sKey] = dre_Ipayment::convertCharset($sVal);
            }
        }

        if(!isset($aIpayment['trx_amount']) || $aIpayment['trx_amount'] != $this->_fcGetBasketPrice()) {
            \OxidEsales\Eshop\Core\Registry::getSession()->setVariable('fc_ipayment_blHasMismatchError', true);
            return 'payment';
        }

        \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( "az_ipayment_aData", $aIpayment );

        // saving chosen payment
        $sPaymentId = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( "paymentid" );
        \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( "paymentid", $sPaymentId );
        $oBasket->setPayment( $sPaymentId );
        $oSession->setBasket( $oBasket );
        
        $sPaymentid = $oBasket->getPaymentId();
        $aDynvalue  = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'dynvalue' );
        // %%PRO_ONLY_BEGIN%% ####################################################################################################
        $aDynvalue = $this->_azGetIpaymentDynValue( $aDynvalue, $aIpayment );
        // %%PRO_ONLY_END%% ######################################################################################################
        $oPayment   = oxNew(\OxidEsales\Eshop\Application\Model\Payment::class);
        if( $sPaymentid && $oPayment->load( $sPaymentid ) && !$oPayment->isValidPayment($aDynvalue, $oConfig->getShopId(), $this->getUser(), $oBasket->getPrice()->getBruttoPrice(), \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'sShipSet'))){
            return 'payment';
        }
		
        // %%PRO_ONLY_BEGIN%% ####################################################################################################
        if(is_array($aDynvalue) && !empty($aDynvalue)){
            \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( 'dynvalue', $aDynvalue);
        }
        
        // logging success response message
        $oIpayment = oxNew(dre_Ipayment::class);
        $sLogMsg = 'SUCCESS';
   		if($this->getConfig()->getShopConfVar('az_ipayment_blLogParams')){
   		    $sLogMsg .= $this->_azGetIpaymentLogData( $aIpayment );
   		}
        dre_Ipayment_Log::log($oIpayment->getPriceFromIpaymentPrice((double)$aIpayment["trx_amount"]), "", $aIpayment["ret_booknr"]." (".$aIpayment["storage_id"].")", $aIpayment["storage_id"], $aIpayment["ret_errorcode"]." NO ERROR", $sLogMsg, dre_Ipayment_Log::SITUATION_PAYMENT, $this->_sTransactionType, $aIpayment["ret_authcode"] );

        // %%PRO_ONLY_END%% ######################################################################################################
        return $this->_sSuccessAction;
    }

    // %%PRO_ONLY_BEGIN%% ####################################################################################################
    /**
     * Set the dynvalue for the payment if this has been activated in the module settings.
     * 
     * @param array $aDynvalue dynvalue
     * @param array $aIpayment ipayment result data
     * 
     * @return array dynvalue (possibly modified by adding the ipayment data)
     */
    protected function _azGetIpaymentDynValue ( $aDynvalue, $aIpayment )
    {
        if(!is_array( $aIpayment ) || !isset( $aIpayment['trx_paymenttyp'] ) ) {
            return $aDynvalue;
        }
        
        $sPaymentType = $aIpayment['trx_paymenttyp'];
        if ( !$sPaymentType ) {
            return $aDynvalue;
        }
        
        if (strtolower($sPaymentType) === 'cc' && $this->getConfig()->getConfigParam('az_ipayment_blStoreCcData')){
            if (!is_array($aDynvalue)){
                $aDynvalue = array();
            }
            $aDynvalue['kkname'] = $aIpayment['paydata_cc_cardowner'];
            $aDynvalue['kktype'] = $aIpayment['paydata_cc_typ'];
            $aDynvalue['kknumber'] = $aIpayment['paydata_cc_number'];
            $aDynvalue['kkmonth'] = substr( $aIpayment['paydata_cc_expdate'], 0, 2 );
            $aDynvalue['kkyear'] = substr( $aIpayment['paydata_cc_expdate'], 2 );
            if(strlen($aDynvalue['kkyear']) > 0 && strlen($aDynvalue['kkyear']) < 4){
                $aDynvalue['kkyear'] = '20' . $aDynvalue['kkyear'];
            }
            $aDynvalue['kkpruef'] = '';
            unset($aDynvalue['lsktoinhaber']);
            unset($aDynvalue['lsktonr']);
            unset($aDynvalue['lsbankname']);
            unset($aDynvalue['lsblz']);
        }elseif(strtolower( $sPaymentType ) === 'elv' && $this->getConfig()->getConfigParam( 'az_ipayment_blStoreElvData')){
            if(!is_array($aDynvalue)){
                $aDynvalue = array();
            }
            $aDynvalue['lsktoinhaber'] = $aIpayment['addr_name'];
            $aDynvalue['lsktonr'] = $aIpayment['paydata_bank_iban'];
            $aDynvalue['lsbankname'] = $aIpayment['paydata_bank_name'];
            $aDynvalue['lsblz'] = $aIpayment['paydata_bank_bic'];
            unset($aDynvalue['kkname']);
            unset($aDynvalue['kktype']);
            unset($aDynvalue['kknumber']);
            unset($aDynvalue['kkmonth']);
            unset($aDynvalue['kkyear']);
            unset($aDynvalue['kkpruef']);
        }
        
        return $aDynvalue;
    }

    /**
     * Builds a log message from an ipayment data array.
     * 
     * @param array $aIpayment ipayment data
     * @return string log message
     */
    public function _azGetIpaymentLogData($aIpayment){
        $sLogMsg = '';
        foreach($aIpayment as $sVar => $sValue){
            // mask ip address:
            if($sVar === 'ret_ip'){
                $sValue = preg_replace( '/[0-9a-zA-Z]/', 'x', $sValue);
            }
            $sLogMsg .= "\n" . $sVar . ': ' . $sValue;
        }
        $sLogMsg = dre_Ipayment::convertCharset( $sLogMsg, 'ISO-8859-1', null );
        return $sLogMsg;
    }

    // %%PRO_ONLY_END%% ######################################################################################################
    /**
     * Processes ipayment errors, caches returned data and redirects to payment page.
     * This avoids a long URL in the address bar.
     * 
     * @return string class to load for next step (in this case, stay on payment step)
     */
    public function processError (){
    	// saving parameters
        $aDataSource = null;
        if(isset($_GET)){
            $aDataSource = & $_GET;
        }elseif(isset($HTTP_GET_VARS)){
            $aDataSource = & $HTTP_GET_VARS;
        }
        
        $aIpayment = array();
        if(is_array($aDataSource)){
            foreach($aDataSource as $sKey => $sVal){
                $aIpayment[$sKey] = $sVal;
            }
        }

        \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( "az_ipayment_aData", $aIpayment );
        \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( 'az_ipayment_blPaymentError', true );

        // saving chosen payment
        \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( "paymentid", \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter("paymentid") );
        
        // %%PRO_ONLY_BEGIN%% ####################################################################################################
        // logging error message
        $oIpayment = oxNew(dre_Ipayment::class);
        $sLogMsg = 'ERROR';
        if($aIpayment['ret_fatalerror']){
            $sLogMsg .= ' (fatal)';
        }
        $sLogMsg .= ': ' . $aIpayment['ret_errormsg'] . ' ' . $aIpayment['ret_additionalmsg'];
   		if($this->getConfig()->getShopConfVar( 'az_ipayment_blLogParams')){
   		    $sLogMsg .= $this->_azGetIpaymentLogData( $aIpayment );
   		}
        dre_Ipayment_Log::log( $oIpayment->getPriceFromIpaymentPrice((double)$aIpayment["trx_amount"]), "", $aIpayment["ret_booknr"], $aIpayment['storage_id'], $aIpayment["ret_errorcode"], $sLogMsg, dre_Ipayment_Log::SITUATION_PAYMENT, $this->_sTransactionType, $aIpayment["ret_authcode"]);

        // %%PRO_ONLY_END%% ######################################################################################################
        \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( "payerror", dre_Ipayment::getPaymentErrorCode( $aIpayment['ret_errorcode'], $aIpayment['ret_fatalerror'] ? true : false ) );
        
        return $this->getThisAction();
    }

    /**
     * Converts an ipayment error code to an OXID eShop compatible error code.
     * 
     * @param integer $iErrCode ipayment service error code
     * 
     * @return integer OXID eShop compatible error code
     */
    protected function _getCompatErrCode ( $iErrCode )
    {
        return $iErrCode;
    }
}
