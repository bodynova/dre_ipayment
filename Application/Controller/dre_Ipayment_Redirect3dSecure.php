<?php
namespace Bender\dre_Ipayment\Application\Controller;

use Bender\dre_Ipayment\Application\Model\dre_Ipayment;
use Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log;
use OxidEsales\Eshop\Application\Model\Order;

class dre_Ipayment_Redirect3dSecure extends \OxidEsales\Eshop\Application\Controller\FrontendController{ //dre_ipayment_Redirect3dSecure_parent {
    /**
     * Template filename.
     * @var string
     */
    protected $_sThisTemplate = 'page/checkout/az_ipayment_redirect3dsecure.tpl';

    /**
     * Redirect url for breaking out of the 3D-Secure iframe.
     * @var string
     */
    protected $_sAzBreakIframeUrl;

    protected function _checkForDataIframeReturn() {
        if(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable('sFcDataIframeRedirectUrl') !== '' && \OxidEsales\Eshop\Core\Registry::getSession()->getVariable('sFcIpaymentUserIsOnIframe')) {
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable('sFcIpaymentUserIsOnIframe');
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable('sFcDataIframeRedirectUrl');
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable('az_ipayment_blBrokeOutOfIframe');
            $this->_sAzBreakIframeUrl = str_replace( '&amp;', '&', $this->getConfig()->getShopSecureHomeUrl() ) . 'cl=payment';
            $oOrder = $this->_getOrder();
            $oOrder->delete();
        }
    }
    
    /**
     * Render function, returns the az_ipayment_redirect3dsecure.tpl template.
     * 
     * @extend render
     * 
     * @return string template name
     */
	public function render(){
		$sTemplate = parent::render();
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_blBrokeOutOfIframe');
        
        // Ipayment redirects to the HTTP-REFERER therefore creating an iframe in iframe bug - break out to payment then
        $this->_checkForDataIframeReturn();
        
		if($this->_sAzBreakIframeUrl){
		    $this->_aViewData['az_ipayment_sBreakIframeUrl'] = $this->_sAzBreakIframeUrl;
            \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( 'az_ipayment_blBrokeOutOfIframe', true );
		    $sTemplate = 'page/checkout/inc/dre_Ipayment_3dsecureIframe.tpl';
		    $this->_sThisTemplate = $sTemplate;
		}elseif(\OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'az_ipayment_3dsecure_iframe')){
		    $this->_aViewData['az_ipayment_s3dSecureHtml' ] = $this->_azGet3dSecureHtml();
		    $this->_aViewData['az_ipayment_bl3dSecureJavascript'] = true;
		    $sTemplate = 'page/checkout/inc/dre_Ipayment_3dsecureIframe.tpl';
		    $this->_sThisTemplate = $sTemplate;
		}elseif(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable('az_ipayment_blDataIframe')){
            \OxidEsales\Eshop\Core\Registry::getSession()->setVariable('sFcIpaymentUserIsOnIframe', true);
            $this->_aViewData['az_ipayment_s3dDataIframeUrl'] = $this->_getIpaymentIframeUrl();
            $this->_sThisTemplate = $sTemplate;
		}elseif(\OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam( 'az_ipayment_bl3DSecureIframe')){
		    $this->_aViewData['az_ipayment_s3dSecureIframeUrl'] = $this->_azGet3dSecureIframeUrl();
		    $this->_aViewData['az_ipayment_bl3dSecureJavascript'] = false;
        }else{
		    $this->_aViewData['az_ipayment_s3dSecureHtml'] = $this->_azGet3dSecureHtml();
		    $this->_aViewData['az_ipayment_bl3dSecureJavascript'] = true;
		}
		return $sTemplate;
	}
    
    protected function _getOrder(){
        $sOrderId = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable('sess_challenge');
        if($sOrderId){
            $oOrder = oxNew(Order::class);
            if($oOrder->load($sOrderId)){
                return $oOrder;
            }
        }
        return false;
    }
    
    protected function _getIpaymentIframeUrl() {
        $oOrder = $this->_getOrder();
        if($oOrder) {
            $iReturnToken = rand(1, 999999);
            \OxidEsales\Eshop\Core\Registry::getSession()->setVariable('sFcIsDataIframeReturn', md5($iReturnToken));

            $oOrder->fcSetOrderStatus('NOT_FINISHED');
            $oOrder->fcSetOrderFolder($this->getConfig()->getConfigParam('az_ipayment_sIframeFolder'));

            $oConfig = \OxidEsales\Eshop\Core\Registry::getConfig();
            $oUser = $this->getUser();
            $oCountry = $oUser->getUserCountry();


            $sAddParams = '&fnc=execute';
            if(\OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter('sDeliveryAddressMD5')) {
                $sAddParams .= '&sDeliveryAddressMD5='. \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter('sDeliveryAddressMD5');
            }
            $sAddParams .= '&rettoken='.$iReturnToken;

            $sRToken = '&rtoken='. \OxidEsales\Eshop\Core\Registry::getSession()->getRemoteAccessToken();
            $sSid = \OxidEsales\Eshop\Core\Registry::getSession()->sid(true);
            if($sSid != ''){
                $sSid = '&'.$sSid;
            }
            $sTrueReturnUrl = str_replace('&amp;', '&', $this->getConfig()->getShopSecureHomeUrl() ).'cl=order&ord_agb=1&stoken='. \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter('stoken').$sSid.$sAddParams.$sRToken;
            \OxidEsales\Eshop\Core\Registry::getSession()->setVariable('sFcDataIframeRedirectUrl', $sTrueReturnUrl);

            $sReturnUrl = str_replace('&amp;', '&', $this->getConfig()->getShopSecureHomeUrl() ). 'az_ipayment_3dsecure_return=1&az_ipayment_3dsecure_iframe=1&sDeliveryAddressMD5='.$this->_getDeliveryAddressMD5();
            $sErrorUrl = str_replace('&amp;', '&', $this->getConfig()->getShopSecureHomeUrl() ). 'cl=payment&fnc=processError';

            $aParams = array();
            $aParams['trxuser_id'] = $oConfig->getConfigParam('az_ipayment_sUser');
            $aParams['trxpassword'] = $oConfig->getConfigParam('az_ipayment_sPassword');
            #$aParams['adminactionpassword'] = $oConfig->getConfigParam('az_ipayment_sAdminPassword');
            $aParams['trx_paymenttyp'] = 'cc';
            $aParams['trx_typ'] = $oConfig->getConfigParam('az_ipayment_blProcInAdmin') ? 'preauth' : 'auth';
            $aParams['trx_amount'] = number_format($oOrder->oxorder__oxtotalordersum->value, 2, '.', '') * 100;
            $aParams['trx_currency'] = $oOrder->oxorder__oxcurrency->value;
            $aParams['order_id'] = $oOrder->oxorder__oxordernr->value;
            #$aParams['transaction_id'] = '1';
            $aParams['shopper_id'] = $oUser->oxuser__oxcustnr->value;
            $aParams['addr_name'] = $oUser->oxuser__oxfname->value.' '.$oUser->oxuser__oxlname->value;
            $aParams['addr_street'] = $oUser->oxuser__oxstreet->value.' '.$oUser->oxuser__oxstreetnr->value;
            $aParams['addr_zip'] = $oUser->oxuser__oxzip->value;
            $aParams['addr_city'] = $oUser->oxuser__oxcity->value;
            $aParams['addr_country'] = $oCountry->oxcountry__oxisoalpha2->value;
            $aParams['addr_email'] = $oUser->oxuser__oxusername->value;
            if($oConfig->getConfigParam('az_ipayment_blStoreCcData')) {
                $aParams['return_paymentdata_details'] = 1;
            }
            $aParams['invoice_text'] = $this->_fcGetInvoiceText($oOrder, $oUser);
            $aParams['redirect_url'] = $sReturnUrl;
            $aParams['silent_error_url'] = $sErrorUrl;
            $aParams['redirect_action'] = 'REDIRECT';

            // handle ipayment security key:
            if ( $oConfig->getConfigParam( 'az_ipayment_sSecKey' ) ) {
                $sSecurityStr  = $oConfig->getConfigParam( 'az_ipayment_sUser' );
                $sSecurityStr .= $aParams['trx_amount'];
                $sSecurityStr .= $oConfig->getActShopCurrencyObject()->name;
                $sSecurityStr .= $oConfig->getConfigParam( 'az_ipayment_sPassword' );
                $sSecurityStr .= $oConfig->getConfigParam( 'az_ipayment_sSecKey' );
                $aParams['trx_securityhash'] = md5($sSecurityStr);
            }

            $sUrl = "https://ipayment.de/merchant/{$oConfig->getConfigParam('az_ipayment_sAccount')}/processor.php";
            $sDelimiter = '?';
            $sLogMsg = '';
            foreach ($aParams as $sKey => $sValue) {
                $sUrl .= $sDelimiter.$sKey.'='.urlencode($sValue);
                if($sDelimiter === '?'){
                    $sDelimiter = '&';
                }
                $sLogMsg .= "\n" . $sKey . ': ' . $sValue;
            }

            $sLogMsg = dre_Ipayment::convertCharset( $sLogMsg, 'ISO-8859-1', null );

            dre_Ipayment_Log::log( $oOrder->oxorder__oxtotalordersum->value, $oOrder, "", "", 0, $sLogMsg, dre_Ipayment_Log::SITUATION_IFRAME );

            return $sUrl;
        }
        return false;
    }
    
    
    /**
     * Returns an invoice text for an ipayment transaction.
     *
     * @param oxOrder $oOrder order object
     * @return string invoice text
     */
    protected function _fcGetInvoiceText( $oOrder, $oUser )
    {
       	$oConfig = $this->getConfig();
    	$sInvoiceText = $oConfig->getConfigParam('az_ipayment_sInvoiceText');
    	if($sInvoiceText){
    	   $sInvoiceText = trim($sInvoiceText);
        }
        if(empty($sInvoiceText)){
            $sInvoiceText = "#SHOP# - BestNr. #ORDERNR#";
        }
        $aReplace = array(
            '#SHOP#' => $oConfig->getActiveShop()->oxshops__oxname->value,
            '#ORDERNR#' => $oOrder->oxorder__oxordernr->value,
            '#CUSTNR#' => '',
            '#CUSTFIRSTNAME#' => '',
            '#CUSTLASTNAME#' => '',
            '#CUSTEMAIL#' => '',
        );
        
		if($oUser){
		    $aReplace[ '#CUSTNR#' ] = $oUser->oxuser__oxcustnr->value;
		    $aReplace[ '#CUSTFIRSTNAME#' ] = $oUser->oxuser__oxfname->value;
		    $aReplace[ '#CUSTLASTNAME#' ] = $oUser->oxuser__oxlname->value;
		    $aReplace[ '#CUSTEMAIL#' ] = $oUser->oxuser__oxusername->value;
		}
        
        $sFinalInvoiceText = str_ireplace(array_keys($aReplace), array_values($aReplace), $sInvoiceText);
        
		return $sFinalInvoiceText;
    }
    

    /**
     * Return users setted delivery address md5
     *
     * @return string
     */
    protected function _getDeliveryAddressMD5() {
        // bill address
        $oUser = $this->getUser();
        
        // OXID changed this mechanism in version 4.7.3/5.0.3
        if(method_exists($oUser, 'getEncodedDeliveryAddress')){
            $sDelAddress = $oUser->getEncodedDeliveryAddress();
        }else{
            $sDelAddress = '';

            $sDelAddress .= $oUser->oxuser__oxcompany;
            $sDelAddress .= $oUser->oxuser__oxusername;
            $sDelAddress .= $oUser->oxuser__oxfname;
            $sDelAddress .= $oUser->oxuser__oxlname;
            $sDelAddress .= $oUser->oxuser__oxstreet;
            $sDelAddress .= $oUser->oxuser__oxstreetnr;
            $sDelAddress .= $oUser->oxuser__oxaddinfo;
            $sDelAddress .= $oUser->oxuser__oxustid;
            $sDelAddress .= $oUser->oxuser__oxcity;
            $sDelAddress .= $oUser->oxuser__oxcountryid;
            $sDelAddress .= $oUser->oxuser__oxstateid;
            $sDelAddress .= $oUser->oxuser__oxzip;
            $sDelAddress .= $oUser->oxuser__oxfon;
            $sDelAddress .= $oUser->oxuser__oxfax;
            $sDelAddress .= $oUser->oxuser__oxsal;
        }

        // delivery address
        if(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'deladrid')){
            $oDelAdress = oxNew(\OxidEsales\Eshop\Application\Model\Address::class);
            $oDelAdress->load(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'deladrid'));

            // OXID changed this mechanism in version 4.7.3/5.0.3
            if(method_exists($oDelAdress, 'getEncodedDeliveryAddress')) {
                $sDelAddress .= $oDelAdress->getEncodedDeliveryAddress();
            }else{
                $sDelAddress .= $oDelAdress->oxaddress__oxcompany;
                $sDelAddress .= $oDelAdress->oxaddress__oxfname;
                $sDelAddress .= $oDelAdress->oxaddress__oxlname;
                $sDelAddress .= $oDelAdress->oxaddress__oxstreet;
                $sDelAddress .= $oDelAdress->oxaddress__oxstreetnr;
                $sDelAddress .= $oDelAdress->oxaddress__oxaddinfo;
                $sDelAddress .= $oDelAdress->oxaddress__oxcity;
                $sDelAddress .= $oDelAdress->oxaddress__oxcountryid;
                $sDelAddress .= $oDelAdress->oxaddress__oxstateid;
                $sDelAddress .= $oDelAdress->oxaddress__oxzip;
                $sDelAddress .= $oDelAdress->oxaddress__oxfon;
                $sDelAddress .= $oDelAdress->oxaddress__oxfax;
                $sDelAddress .= $oDelAdress->oxaddress__oxsal;
            }
        }
        if(!method_exists($oUser, 'getEncodedDeliveryAddress')) {
            $sDelAddress = md5($sDelAddress);
        }
        return $sDelAddress;
    }
    
	/**
	 * Returns HTML code for the redirection to the 3D-Secure website.
	 * Reads the az_ipayment_sRedirectData session variable and replaces the shop url in the HTML data.
	 *
	 * @return string HTML code for 3D-Secure redirect
	 */
	protected function _azGet3dSecureHtml(){
		$sReturnUrl = str_replace('&amp;', '&', $this->getConfig()->getShopSecureHomeUrl() ). 'az_ipayment_3dsecure_return=1&sDeliveryAddressMD5='.$this->_getDeliveryAddressMD5();
		if(\OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'az_ipayment_3dsecure_iframe')){
		    $sReturnUrl .= '&az_ipayment_3dsecure_iframe=1';
		}
		
		$s3dSecure = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_sRedirectData');
		$s3dSecure = str_replace( '%REDIRECT_RETURN_SCRIPT%?', $sReturnUrl, $s3dSecure);
        \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_sRedirectData');
       	$sOrderId = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_sOrderId');
        \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( 'az_ipayment_sRedirectedOrder', $sOrderId);
		
        // %%PRO_ONLY_BEGIN%% ####################################################################################################
       	$oOrder = oxNew(Order::class);
   		$oOrder->Load($sOrderId);
   		$sLogMsg = 'showing 3D-Secure redirect page';
   		if($this->getConfig()->getShopConfVar( 'az_ipayment_blLogParams')){
   		    $sLogMsg .= "\nRETURN_URL: $sReturnUrl\nHTML: $s3dSecure";
   		}
        dre_Ipayment_Log::log( $oOrder->getTotalOrderSum(), $oOrder, null, null, 0, $sLogMsg, dre_Ipayment_Log::SITUATION_3DSECURE );
        // %%PRO_ONLY_END%% ######################################################################################################
		return $s3dSecure;
	}

	/**
	 * Returns the url for showing the 3D-Secure website within an iframe.
	 * 
	 * @return string shop url for showing the 3D-Secure website
	 */
	protected function _azGet3dSecureIframeUrl(){
	    $sUrl = str_replace( '&amp;', '&', $this->getConfig()->getShopSecureHomeUrl() ). 'cl=az_ipayment_redirect3dsecure&az_ipayment_3dsecure_iframe=1';
	    return $sUrl;
	}

	/**
	 * Breaks out of the 3D-Secure iframe and then calls the order view module to execute the 3D-Secure order.
	 * This method sets the property $this->_sAzBreakIframeUrl, making the template break out of the iframe.
	 * 
	 * @return null
	 */
	public function azLeave3dSecureIframe(){
	    $oConfig = $this->getConfig();
        $sRedirectDataId = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'az_ipayment_redirect_id');
        
        $sRedirectUrl = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable('sFcDataIframeRedirectUrl');
        if($sRedirectUrl){
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable('sFcDataIframeRedirectUrl');
            $this->_sAzBreakIframeUrl = $sRedirectUrl;
        }else{
            $this->_sAzBreakIframeUrl = str_replace( '&amp;', '&', $oConfig->getShopSecureHomeUrl() ) . 'cl=order&fnc=azExecute3dSecure&az_ipayment_redirect_id=' . $sRedirectDataId.'&sDeliveryAddressMD5='.$this->_getDeliveryAddressMD5();
        }
        // %%PRO_ONLY_BEGIN%% ####################################################################################################
        if(!$sOrderId) {
            $sOrderId = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'sess_challenge' );
        }
        $oOrder = null;
        $dAmount = null;
        if(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_sIframeOrderId' )) {
            $oOrder = oxNew(Order::class);
            $oOrder->load(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_sIframeOrderId'));
            $dAmount = $oOrder->getTotalOrderSum();
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable( 'az_ipayment_sIframeOrderId');
        }
        
        $sLogMsg = 'Breaking out of 3D-Secure iframe';
   		if ( $this->getConfig()->getShopConfVar( 'az_ipayment_blLogParams')){
   		    $sLogMsg .= "\nredirect data id: $sRedirectDataId\nredirecting to url: " . $this->_sAzBreakIframeUrl;
   		}
        dre_Ipayment_Log::log( $dAmount, $oOrder, null, null, 0, $sLogMsg, dre_Ipayment_Log::SITUATION_BREAK_IFRAME );
        // %%PRO_ONLY_END%% ######################################################################################################
	}
}
