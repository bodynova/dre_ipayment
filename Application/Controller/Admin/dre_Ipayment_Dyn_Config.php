<?php

namespace Bender\dre_Ipayment\Application\Controller\Admin;


use OxidEsales\Eshop\Application\Model\Payment;
use OxidEsales\Eshop\Application\Model\PaymentList;
use Bender\dre_Ipayment\Application\Model\dre_Ipayment;
use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Field;

class dre_Ipayment_Dyn_Config extends \OxidEsales\Eshop\Application\Controller\Admin\ShopConfiguration { //dre_Ipayment_Dyn_Config_parent {
    /**
     * Current class template name.
     * @var string
     */
	protected $_sThisTemplate = 'dre_ipayment_dyn_config.tpl';

	/**
	 * Inactive or missing template blocks.
	 * @see getTemplateBlockProblems()
	 * @var array
	 */
	protected $_aAzTemplateBlockProblems = null;


    /**
     * Render method, shows ipayment configuration.
     * 
     * @return string template name ("dre_ipayment_dyn_config.tpl")
     */    
    public function render ()
    {
        $this->_aViewData[ 'oxid' ] = $this->getConfig()->getShopId();
        $this->_aViewData["afolder"] = $this->getConfig()->getConfigParam('aOrderfolder');

        parent::render();
        
        return $this->_sThisTemplate;
    }

    /**
     * Saves association of payments to ipayment (table dre_object2ipayment) from the admin page form.
     * 
     * @return string redirect class
     */
    public function save(){
        $sRet = parent::save();
        
    	$aIpaymentTypes = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'ipaymentTypes' );
    	foreach($aIpaymentTypes as $sPaymentId => $sIpaymentType){
    		$oPayment = oxNew(Payment::class);
    		$oPayment->load( $sPaymentId );
    		if($oPayment->getIpaymentType() !== $sIpaymentType){
                $oPayment->setIpaymentType($sIpaymentType);
            }
    	}
    	return $sRet;
    }

    /**
     * Always show settings for current shop.
     * 
     * @extend getEditObjectId
     * 
     * @return string shop id
     */
    public function getEditObjectId(){
        return $this->getConfig()->getShopId();
    }

    /**
     * Loads and returns a list of payment methods.
     * 
     * @return PaymentList (oxList) list of payment methods (oxPayment objects)
     */
    public function getPayments(){
        $oPayList = oxNew(PaymentList::class);
        $sSelect  = "SELECT * FROM " . getViewName( 'oxpayments' );
        $oPayList->selectString( $sSelect );
        
        return $oPayList;
    }

    /**
     * Returns an array of oxtplblocks with problems (missing or inactive).
     * 
     * @return array template blocks with problems
     */
    public function getTemplateBlockProblems(){
        return $this->_aAzTemplateBlockProblems;


        if ( $this->_aAzTemplateBlockProblems === null ) {
            $this->_aAzTemplateBlockProblems = array();
            
            $aRequiredBlocks = array(
                'admin_order_overview_billingaddress'   => array(
                    'oxtplblocks__oxtemplate' => 'order_overview.tpl',
                    'oxtplblocks__oxblockname' => 'admin_order_overview_billingaddress',
                    'oxtplblocks__oxfile' => 'dre_ipayment_admin_order_overview_billingaddress',
                    'oxtplblocks__oxpos' => '996'
                ),
                'admin_order_list_item'                 => array(
                    'oxtplblocks__oxtemplate' => 'order_list.tpl',
                    'oxtplblocks__oxblockname' => 'admin_order_list_item',
                    'oxtplblocks__oxfile' => 'dre_ipayment_admin_order_list_item',
                    'oxtplblocks__oxpos' => '997'
                ),
                'select_payment'                        => array(
                    'oxtplblocks__oxtemplate' => 'page/checkout/payment.tpl',
                    'oxtplblocks__oxblockname' => 'select_payment',
                    'oxtplblocks__oxfile' => 'dre_ipayment_select_payment',
                    'oxtplblocks__oxpos' => '998'
                ),
                'change_payment'                        => array(
                    'oxtplblocks__oxtemplate' => 'page/checkout/payment.tpl',
                    'oxtplblocks__oxblockname' => 'change_payment',
                    'oxtplblocks__oxfile' => 'dre_ipayment_change_payment',
                    'oxtplblocks__oxpos' => '999'
                ),
            );
            
            // these blocks dont exist before oxid 4.6
            //unset($aRequiredBlocks['admin_order_overview_billingaddress']);
            //unset($aRequiredBlocks['admin_order_list_item']);

            
            $oConfig = \OxidEsales\Eshop\Core\Registry::getConfig();
            $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(\OxidEsales\Eshop\Core\DatabaseProvider::FETCH_MODE_ASSOC);
            $aBlocks = array();
            $sQuery = "SELECT oxid FROM " . getViewName( 'oxtplblocks' ) . " WHERE oxshopid = " . $oDb->quote( $oConfig->getShopId() ) . " AND oxmodule = 'dre_ipayment' ORDER BY oxpos ASC";



            $rs = $oDb->getAll( $sQuery );
            if ( $rs && count($rs) > 0 ) {



                echo '<pre>';
                foreach($rs as $key => $value) {
                    echo $value[0] . '<br/>';
                    print_r($value);
                    print_r($rs);


                    //$oBlock = oxNew(BaseModel::class);
                    //$oBlock->init( 'oxtplblocks' );

                    //$oBlock->load($value[0]);

                    //echo $oBlock->buildSelectString('oxtplblocks', $value[0]);

                    echo '<pre>';
                   // print_r($oBlock->load($value[0]));
                    die();

                    $oBlock->sAzBlockTemplateFile = $oBlock->oxtplblocks__oxfile->value ? 'modules/bender/dre_ipayment/Application/views/blocks/' . $oBlock->oxtplblocks__oxfile->value . '.tpl' : '';

                    // check if the block is required and has problems:
                    if(array_key_exists( $oBlock->oxtplblocks__oxblockname->value, $aRequiredBlocks )) {
                        // remove block from the list of required blocks:
                        unset( $aRequiredBlocks[ $oBlock->oxtplblocks__oxblockname->value ] );

                        // if the block is inactive, then add it to the problems list:
                        if(!$oBlock->oxtplblocks__oxactive->value){
                            $this->_aAzTemplateBlockProblems[] = $oBlock;
                        }
                    }
                }
            }

            // check for missing blocks and add them to the problems list:
            foreach ( $aRequiredBlocks as $sBlockName => $aBlockData ) {
                $oBlock = oxNew(BaseModel::class);
                $oBlock->init( 'oxtplblocks', true );
                $oBlock->blAzMissing = true;
                $oBlock->oxtplblocks__oxactive = new Field( '1' );
                $oBlock->oxtplblocks__oxshopid = new Field( $oConfig->getShopId() );
                $oBlock->oxtplblocks__oxmodule = new Field( 'dre_ipayment' );
                
                foreach($aBlockData as $sField => $sValue){
                    $oBlock->$sField = new Field($sValue);
                }
                
                $oBlock->sAzBlockTemplateFile = $oBlock->oxtplblocks__oxfile->value ? 'modules/bender/dre_ipayment/Application/views/blocks/' . $oBlock->oxtplblocks__oxfile->value . '.tpl' : '';
                
                $this->_aAzTemplateBlockProblems[] = $oBlock;
            }
        }
        return $this->_aAzTemplateBlockProblems;
    }

    /**
     * Inserts or updates a template block entry into the oxtplblocks database table.
     * 
     * @return string redirect class
     */
    public function azSaveTemplateBlock(){
        $aBlock = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'aAzBlock' );
        if (empty($aBlock)){
            return;
        }
        $oBlock = oxNew(BaseModel::class);
        $oBlock->init( 'oxtplblocks');
        if(!empty($aBlock['oxtplblocks__oxid'])){
            $oBlock->load( $aBlock['oxtplblocks__oxid'] );
        }
        foreach($aBlock as $sField => $sValue){
            $oBlock->$sField = new Field($sValue);
        }
        $oBlock->save();
    }
}