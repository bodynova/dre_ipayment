<?php

namespace Bender\dre_Ipayment\Application\Controller\Admin;

use Bender\dre_Ipayment\Application\Model\dre_Ipayment;
use Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log;
use Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log_List;
// oxuserlist => \OxidEsales\Eshop\Application\Model\UserList
use OxidEsales\Eshop\Application\Model\UserList;
// oxpaymentlist => \OxidEsales\Eshop\Application\Model\PaymentList
use OxidEsales\Eshop\Application\Model\PaymentList;

class dre_Ipayment_Dyn_Log extends \OxidEsales\Eshop\Application\Controller\Admin\AdminListController { //dre_Ipayment_Dyn_Log_parent {

    /**
     * Current class template name.
     * @var string
     */
	protected $_sThisTemplate = 'dre_ipayment_dyn_log.tpl';

    /**
     * Name of chosen object class (default null).
     * @var string
     */
    protected $_sListClass = dre_Ipayment_Log::class; //'dre_Ipayment_Log';
    //protected $_sListClass = 'dre_Ipayment_Log';

    /**
     * Type of list.
     * @var string
     */
    protected $_sListType = dre_Ipayment_Log_List::class;
    //protected $_sListType = 'dre_Ipayment_Log_List';

    /**
     * Enable/disable sorting by DESC (SQL) (default false - disable).
     * @var bool
     */
    protected $_blDesc = true;

    /**
     * Default SQL sorting parameter (default null).
     * @var string
     */
    protected $_sDefSortField = 'oxtime';

    /**
     * array of user ids that have been found by searching for email
     * @var array
     */
    protected $_aFcSearchUsers = array();

    /**
     * search term the user entered
     * @var array
     */
    protected $_sFcSearchUsers;

    /**
     * array of payment ids that have been found by searching for email
     * @var array
     */
    protected $_aFcSearchPayments = array();

    /**
     * search term the user entered
     * @var array
     */
    protected $_sFcSearchPayments;

    /**
     * array of order ids that have been found by searching for email
     * @var array
     */
    protected $_aFcSearchOrders = array();

    /**
     * search term the user entered
     * @var array
     */
    protected $_sFcSearchOrders;

	/**
     * array of situations that have been found by comparing with available values
     * @var array
     */
    protected $_aFcSearchSituations = array();

    /**
     * search term the user entered
     * @var array
     */
    protected $_sFcSearchSituations;

	/**
     * array of situations that have been found by comparing with available values
     * @var array
     */
    protected $_aAvailableSituations = array(
		'payment'=>'AZ_IPAYMENT_SITUATION_PAYMENT',
		'oxpaymentgateway'=>'AZ_IPAYMENT_SITUATION_OXPAYMENTGATEWAY',
		'3d-secure'=>'AZ_IPAYMENT_SITUATION_3DSECURE',
        'iframe'=>'AZ_IPAYMENT_SITUATION_IFRAME',
        'iframe-return'=>'AZ_IPAYMENT_SITUATION_IFRAME_RETURN',
		'admin-capture'=>'AZ_IPAYMENT_SITUATION_ADMIN_CAPTURE',
		'admin-reverse'=>'AZ_IPAYMENT_SITUATION_ADMIN_REVERSE',
		'admin-refund'=>'AZ_IPAYMENT_SITUATION_ADMIN_REFUND',
	);



	/**
     * Extends parent render method to allow to switch between ascending and descending sorting of columns by clicking them multiple times.
     *
     * @extend render
     *
     * @return string template file to render
     */
    public function render(){
        parent::render();

		$sVersion = \OxidEsales\Eshop\Core\ShopVersion::getVersion();
		$sNameConcat = $this->_fcGetConcatByVersion($sVersion);
        //$this->_azMaintainSortOrder();
		$this->_aViewData['shopversion'] = $sVersion;
		$this->_aViewData['nameconcat'] = $sNameConcat;
		$this->_aViewData['where'] = $this->_fcGetEnteredValues();
        // standard sorting
        return $this->_sThisTemplate;
    }

    /**
     * Includes the sort order information (ascending / descending) in the template variables (_aViewData).
     *
     * @return null
     */
    protected function _azMaintainSortOrder (){
        // keep the adminorder param in the view data:
        $sAdminOrder = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'adminorder');
        if($sAdminOrder){
            $sAdminOrder = '1';
        }else{
            $sAdminOrder = '0';
        }
        $this->_aViewData['adminorder'] = $sAdminOrder;

        // trick to append adminorder to the folder data, so that the page navigation snippet includes the adminorder value when jumping between pages:
        $this->_aViewData['folder'] = $this->_aViewData['folder'] . '&adminorder=' . $sAdminOrder;
    }

    /**
     * Builds and returns array of SQL WHERE conditions.
     * Makes sure that only ipayment logs are loaded and that searches for amounts in the log
     * are matched correctly (oxamount is stored as cents, not euro).
     *
     * @return array SQL WHERE conditions
     */
    public function buildWhere (){
        $this->_aWhere = parent::buildWhere();
        if(!is_array($this->_aWhere)) {
            return $this->_aWhere;
        }
        foreach($this->_aWhere as $sKey => $sValue){
        	if($sKey === 'dre_ipayment_logs.oxamount'){
				$sAmount = trim($sValue, '%');
				$sAmount = trim(str_replace( ',', '.', $sAmount));
				$oIpayment = oxNew(dre_Ipayment::class);
				$dAmount = (double)$oIpayment->getIpaymentPrice((double)$sAmount);
				$this->_aWhere[ $sKey ] = '%' . $dAmount . '%';
			}else if($sKey === 'dre_ipayment_logs.oxtime'){
				// if user entered a time filter he usually uses date format he perfers
				// for that we need to translate it first into a unix timestamp and afterwards
				// using unique database compatible format
				$sValue = str_replace("%", "", $sValue);
				$sTime = strtotime($sValue);
				$sDate =  date("Y-m-d", $sTime);
				// trim with database joker
				$this->_aWhere[ $sKey ] = '%' . $sDate . '%';
			}else if($sKey === 'dre_ipayment_logs.oxuid' && !empty($sValue)){
				// due to customers don't search for a userid but for an email address
				// (as it was implemented!!!) we have to determine the userid to be able to filter results
				//$aPossibleUsers = array();
				$aPossibleUsers = $this->_fcGetUserIdsByEmailSearch($sValue);
				$this->_aFcSearchUsers = $aPossibleUsers;
				// delete email part from where-array before filling it with userids
				$this->_aWhere[ 'dre_ipayment_logs.oxuid' ] = '';
				$this->_sFcSearchUsers = str_replace("%", "", $sValue);
			}else if($sKey === 'dre_ipayment_logs.oxpayid' && !empty($sValue)){
				// due to customers don't search for a paymentid but for a payment name
				// (as it was implemented!!!) we have to determine the paymentid to be able to filter results
				// $aPossiblePayments = array();
				$aPossiblePayments = $this->_fcGetPaymentIdsByNameSearch($sValue);
				$this->_aFcSearchPayments = $aPossiblePayments;
				// delete email part from where-array before filling it with userids
				$this->_aWhere[ 'dre_ipayment_logs.oxpayid' ] = '';
				$this->_sFcSearchPayments = str_replace("%", "", $sValue);
			}else if($sKey === 'dre_ipayment_logs.oxorderid' && !empty($sValue)){
				// due to customers don't search for a paymentid but for a payment name
				// (as it was implemented!!!) we have to determine the paymentid to be able to filter results
				// $aPossibleOrders = array();
				$aPossibleOrders = $this->_fcGetOrderIdsByOrderNr($sValue);
				$this->_aFcSearchOrders = $aPossibleOrders;
				// delete email part from where-array before filling it with userids
				$this->_aWhere[ 'dre_ipayment_logs.oxorderid' ] = '';
				$this->_sFcSearchOrders = str_replace("%", "", $sValue);
			}else if($sKey === 'dre_ipayment_logs.oxsituation' && !empty($sValue)){
				// due to customers don't search for the value which is inside the database but
				// for translated term which is displayed we have to determine possible database values
				// $aPossibleSituations = array();
				$aPossibleSituations = $this->_fcGetSituationValuesByName($sValue);
				$this->_aFcSearchSituations = $aPossibleSituations;
				// delete email part from where-array before filling it with situations
				$this->_aWhere[ 'dre_ipayment_logs.oxsituation' ] = '';
				$this->_sFcSearchSituations = str_replace("%", "", $sValue);
			}
        }
        // only load ipayment logs:
        $this->_aWhere[ 'dre_ipayment_logs.oxgwtype' ] = 'ipay';
		// the reset of the internal pointer is nessassary due there is one value too much and
		// oxadminlist prefer using list-each construct than using foreach
        reset($this->_aWhere);
        return $this->_aWhere;
    }

    /**
     * returns an array of values that the user entered
	 *
     * @return array
     */
	protected function _fcGetEnteredValues() {
		$aReturn = array();
		$aWhere = $this->buildWhere();
		foreach ($aWhere as $sKey=>$sValue) {
			$aSplittedKey = explode(".", $sKey);
			$sValue = str_replace("%", "", $sValue);
			if ($aSplittedKey[1] === 'oxamount') {
				// reform to common price entry
				$sValue = number_format($sValue/10000, 2, ",","");
			}else if($aSplittedKey[1] === 'oxuid' && !empty($this->_sFcSearchUsers)){
				$sValue = $this->_sFcSearchUsers;
			}else if($aSplittedKey[1] === 'oxpayid' && !empty($this->_sFcSearchPayments)){
				$sValue = $this->_sFcSearchPayments;
			}else if($aSplittedKey[1] === 'oxorderid' && !empty($this->_sFcSearchOrders)){
				$sValue = $this->_sFcSearchOrders;
			}else if($aSplittedKey[1] === 'oxsituation' && !empty($this->_sFcSearchSituations)){
				$sValue = $this->_sFcSearchSituations;
			}
			$aReturn[$aSplittedKey[0]][$aSplittedKey[1]] = $sValue;
		}
		return $aReturn;
	}

	protected function _fcGetConcatByVersion( $sVersion ) {
		$aSplittedVersion = explode(".", $sVersion);
		if($aSplittedVersion[0] > '4' || ($aSplittedVersion[0] === '4' && $aSplittedVersion[1] >= '5')){
			$sConcat = "][";
		}else{
			$sConcat = ".";
		}
		return $sConcat;
	}

	protected function _fcGetUserIdsByEmailSearch($sEmailSearch){
		$oUserList = oxNew(UserList::class);
		$sSql = "SELECT * FROM ".getViewName('oxuser')." WHERE oxusername LIKE ". \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sEmailSearch);
		$oUserList->selectString($sSql);
		$aUsers = $oUsers = array();
		$oUsers = $oUserList->getArray();
		foreach ($oUsers as $oUser){
			$aUsers[] = "'".$oUser->oxuser__oxid->value."'";
		}
		if(\count($aUsers) === 0){
			$aUsers = array("'$sEmailSearch'");
		}
		return $aUsers;
	}

	protected function _fcGetOrderIdsByOrderNr($sOrderNr){
		$myDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(dre_Ipayment::getFetchMode());
		$params = array($sOrderNr);
		$sSql = "SELECT * FROM ".getViewName('oxorder')." WHERE OXORDERNR LIKE ? LIMIT 0,10";
		$oOrders = $myDb->getAll($sSql, $params);


		if ($oOrders !== false && \count($oOrders) > 0){
		    foreach ($oOrders as $order){
                $aOrders[] = "'".$order[0]."'";
            }
		}
		if (\count($aOrders) === 0){
			$aOrders = array("'$sOrderNr'");
		}
		return $aOrders;
	}

	protected function _fcGetPaymentIdsByNameSearch( $sNameSearch ) {
		$oPaymentList = oxNew(PaymentList::class);
        //$myDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb( );
		$sSql = "SELECT * FROM ".getViewName('oxpayments')." WHERE oxdesc LIKE ". \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sNameSearch);
        $oPaymentList->selectString($sSql);
		$aPayments = $oPayments = array();
		$oPayments = $oPaymentList->getArray();
		foreach ($oPayments as $oPayment) {
			$aPayments[] = "'".$oPayment->oxpayments__oxid->value."'";
		}
        if(\count($aPayments) === 0){
			$aPayments = array("'$sNameSearch'");
		}
		return $aPayments;
	}

	protected function _fcGetSituationValuesByName( $sNameSearch ) {
		$sNameSearch = str_replace( "%", "", $sNameSearch);
		$aSituationsMatched = array();
		foreach($this->_aAvailableSituations as $sSituationDb=>$sTranslate){
			// translate string
			$sTranlatedSituation = \OxidEsales\Eshop\Core\Registry::getLang()->translateString($sTranslate);
			// check if entered search term matches the translation of current situation
			if(preg_match( "/$sNameSearch/i", $sTranlatedSituation)){
				$aSituationsMatched[] = "'".$sSituationDb."'";
			}
		}
		if(\count($aSituationsMatched) === 0){
			$aSituationsMatched = array("'$sNameSearch'");
		}
		return $aSituationsMatched;
	}

	protected function _prepareWhereQuery( $aWhere, $sQ ) {
		$sQ = parent::_prepareWhereQuery( $aWhere, $sQ );
		if(\is_array($this->_aFcSearchUsers) && \count($this->_aFcSearchUsers) > 0){
			$sQ .= " AND dre_ipayment_logs.oxuid IN (". implode( ',', $this->_aFcSearchUsers) . ") ";
		}
		if(\is_array($this->_aFcSearchPayments) && \count($this->_aFcSearchPayments) > 0){
			$sQ .= " AND dre_ipayment_logs.oxpayid IN (". implode( ',', $this->_aFcSearchPayments) . ") ";
		}
		if(\is_array($this->_aFcSearchOrders) && \count($this->_aFcSearchOrders) > 0){
			$sQ .= " AND dre_ipayment_logs.oxorderid IN (". implode( ',', $this->_aFcSearchOrders) . ") ";
		}
		if(\is_array( $this->_aFcSearchSituations ) && \count($this->_aFcSearchSituations) > 0){
			$sQ .= " AND dre_ipayment_logs.oxsituation IN (". implode( ',', $this->_aFcSearchSituations) . ") ";
		}
		return $sQ;
	}


     protected function _prepareOrderByQuery( $sSql = null){
         // sorting
         $aSortFields = $this->getListSorting();

         if(\is_array($aSortFields) && \count($aSortFields)){

             // only add order by at full sql not for count(*)
             $sSql .= ' order by ';
             $blSep = false;

             $oListItem = $this->getItemListBaseObject();
             $iLangId = $oListItem->isMultilang() ? $oListItem->getLanguage() : \OxidEsales\Eshop\Core\Registry::getLang()->getBaseLanguage();

             $blSortDesc = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'adminorder' );
             $blSortDesc = $blSortDesc !== null ? (bool) $blSortDesc : $this->_blDesc;
             foreach ( $aSortFields as $sTable => $aFieldData ) {

                 $sTable = $sTable ? (getViewName( $sTable, $iLangId ) . '.' ) : '';
                 foreach ( $aFieldData as $sColumn => $sSortDir ) {
                     $sField = $sTable . $sColumn;
                     //add table name to column name if no table name found attached to column name
                     $sSql .= ((($blSep) ? ', ' : '')) . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sField);

                     //V oxactive field search always DESC
                     if ( $blSortDesc || $sColumn === 'oxactive' || $sColumn === 'oxtime' || strcasecmp( $sSortDir, 'desc' ) === 0 ) {
                         $sSql .= ' desc ';
                     }
                     $blSep = true;
                 }
             }
         }
		 return $sSql;
	 }
}
