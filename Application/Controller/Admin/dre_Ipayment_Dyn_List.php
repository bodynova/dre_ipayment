<?php

namespace Bender\dre_Ipayment\Application\Controller\Admin;


class dre_Ipayment_Dyn_List extends \OxidEsales\Eshop\Application\Controller\Admin\AdminListController { //dre_Ipayment_Dyn_List_parent {
    /**
     * Current class template name.
     * @var string
     */
    protected $_sThisTemplate = 'dre_ipayment_dyn_list.tpl';
}
