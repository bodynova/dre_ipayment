<?php

namespace Bender\dre_Ipayment\Application\Controller\Admin;

use Bender\dre_Ipayment\Application\Model\dre_Ipayment;

class dre_Ipayment_Dyn_Main extends \OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController { //dre_Ipayment_Dyn_Main_parent {
    /**
     * Current class template name.
     * @var string
     */
	protected $_sThisTemplate = 'dre_ipayment_dyn_main.tpl';

	/**
	 * Returns the ipayment module version.
	 *
	 * @return string module version
	 */
	public function getIpaymentVersion ()
	{
		return dre_Ipayment::getModuleVersion();
	}

	/**
	 * Returns the ipayment module revision number.
	 *
	 * @return integer module revision number
	 */
	public function getIpaymentRevision ()
	{
		return dre_Ipayment::getModuleRevision();
	}
}
