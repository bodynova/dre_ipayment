<?php

namespace Bender\dre_Ipayment\Application\Controller\Admin;

use Bender\dre_Ipayment\Application\Model\dre_Ipayment;
use Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log_List;
use OxidEsales\Eshop\Application\Model\Order;

class dre_Ipayment_AdminDetailsController extends \OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController {//dre_Ipayment_AdminDetailsController_parent {
    /**
     * Executes parent class, loads selected order and passes to template engine.
     * Returns name of template to render.
     * 
     * @return string template file name
     */
	public function render ()
    {
        parent::render();
        
        $this->_loadOrder();

        return 'dre_ipayment_admin_order.tpl';
    }

    /**
     * Loads selected order object and sets up _aViewData.
     *
     * @return null
     */
    protected function _loadOrder (){
        $sOrderId = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'oxid' );
        if( $sOrderId == '-1' || !isset( $sOrderId ) )
        	return;
		
        // load object
		//$oOrder = oxNew( 'oxorder' );
        $oOrder = oxNew(\OxidEsales\Eshop\Application\Model\Order::class);
		$oOrder->load( $sOrderId );
		$this->_aViewData[ 'edit' ] =  $oOrder;

		if ( $oOrder->oxorder__oxpaymenttype->value ){
			//$oPayment = oxNew( 'oxpayment' );
			$oPayment = oxNew(\OxidEsales\Eshop\Application\Model\Payment::class);
			$oPayment->load( $oOrder->oxorder__oxpaymenttype->value );
			$this->_aViewData[ 'payment' ] = $oPayment;
		}
		
		$sSelect = "select count(*) from dre_ipayment_logs where oxorderid = '$sOrderId' and oxsituation = '3d-secure'";

		if ( \OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() )->getOne( $sSelect ) > 0 )
		    $this->_aViewData[ 'blIs3dSecure' ] = true;
    }

    /**
     * Returns ipayment logs (from dre_ipayment_logs table) for selected order.
     *
     * @return dre_Ipayment_Log_List ipayment logs for selected order
     */
    public function getIpaymentLogs (){
        $sOrderId = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'oxid' );
        if( $sOrderId == '-1' || !isset( $sOrderId ) )
        	return array();
        
        $oLogs = oxNew( dre_Ipayment_Log_List::class);
        $oLogs->loadIpaymentLogsForOrder( $sOrderId );
        return $oLogs;
    }

    /**
     * Capture (charge/debit) ipayment payment for selected order.
     * 
     * @return null
     */
    public function captureNow(){
        $sOrderId = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'oxid' );
        if ($sOrderId !== '-1'){
        	// load object
            $oOrder = oxNew(Order::class);
            $oOrder->load( $sOrderId );

	        $oPayTransaction = oxNew(\OxidEsales\Eshop\Application\Model\PaymentGateway::class);
            
	        if(!$oPayTransaction->captureOrder($oOrder->getTotalOrderSum(), $oOrder)){
                $this->_aViewData['eccstatus'] = 'FAILED';
                $oOrder->oxorder__oxtransstatus = new \OxidEsales\Eshop\Core\Field( 'FAILED' , \OxidEsales\Eshop\Core\Field::T_RAW);
		        $oOrder->save();
		        $this->_aViewData['sDescription'] = $oPayTransaction->getLastError();
	        }else{
                $this->_aViewData['eccstatus'] = 'OK';
            }
        }    
    }

    /**
     * Reverse (cancel) ipayment payment for selected order.
     * 
     * @return null
     */
    public function reverseNow(){
        $sOrderId = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'oxid' );
        if($sOrderId !== '-1'){
        	// load object
            $oOrder = oxNew(Order::class);
            $oOrder->load($sOrderId);

	        $oPayTransaction = oxNew(\OxidEsales\Eshop\Application\Model\PaymentGateway::class);
            if(!$oPayTransaction->cancelOrder( $oOrder->getTotalOrderSum(), $oOrder)){
                $this->_aViewData['ecrsstatus'] = 'FAILED';
                $oOrder->oxorder__oxtransstatus = new \OxidEsales\Eshop\Core\Field( 'FAILED',\OxidEsales\Eshop\Core\Field::T_RAW);
                $oOrder->save();
                $this->_aViewData['sDescription'] = $oPayTransaction->getLastError();
            }else{
                $this->_aViewData['ecrsstatus'] = 'OK';
            }
        }
    }

    /**
     * Refund captured ipayment order total for selected order.
     * 
     * @return null
     */
    public function refundNow(){
        $sOrderId = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'oxid' );
        if($sOrderId !== '-1'){
        	// load object
            $oOrder = oxNew(Order::class);
            $oOrder->load( $sOrderId );

            $oPayTransaction = oxNew(\OxidEsales\Eshop\Application\Model\PaymentGateway::class);
            if(!$oPayTransaction->refundOrder( $oOrder->getTotalOrderSum(), $oOrder)){
                $this->_aViewData['ecrfstatus'] = 'FAILED';
                $oOrder->oxorder__oxtransstatus = new \OxidEsales\Eshop\Core\Field( 'FAILED', \OxidEsales\Eshop\Core\Field::T_RAW);
                $oOrder->save();
                $this->_aViewData['sDescription'] = $oPayTransaction->getLastError();
            }else{
                $this->_aViewData['ecrfstatus'] = 'OK';
            }
        }    
    }

    /**
     * Manually reset selected order to unpaid status.
     *
     * @return null
     */
    public function reverseManual(){
        $sOrderId = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'oxid' );
        if( $sOrderId !== '-1'){

            // load object
            $oOrder = oxNew(Order::class);
            $oOrder->load($sOrderId);

            $oOrder->oxorder__oxpaid = new \OxidEsales\Eshop\Core\Field( '0000-00-00 00:00:00', \OxidEsales\Eshop\Core\Field::T_RAW);
            $oOrder->save();
        }
    }
}
