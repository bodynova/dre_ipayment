<?php
namespace Bender\dre_Ipayment\Application\Component;

use Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log;
use Bender\dre_Ipayment\Application\Model\dre_Ipayment;

class dre_Ipayment_UtilsComponent extends dre_Ipayment_UtilsComponent_parent { //dre_Ipayment_UtilsComponent_parent{
    /**
     * Checks whether the shop has just been called through a return call from a 3D-Secure server.
     * 
     * @extend init
     * 
     * @return null
     */
    public function init(){
        $this->_azCheck3dSecureReturn();
        
        return parent::init();
    }

    /**
     * Checks whether the shop has just been called through a return call from a 3D-Secure server.
     * In that case the result is stored in the
     * az_ipayment_redirectdata table and a redirect to execute the 3D-Secure order will be done.
     * 
     * @return null (if a 3D-Secure return is detected, then a redirect is done, so this method will not return in that case)
     */
    protected function _azCheck3dSecureReturn(){

        // check if this is a return from a 3D-Secure website:
        $i3dSecureReturn = $_GET['az_ipayment_3dsecure_return'];
        if ( !$i3dSecureReturn ) {
            return;
        }

        // store MD and PaRes in database:
        $oConfig = \OxidEsales\Eshop\Core\Registry::getConfig();
        $sMd = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'MD' );
        $sDeliveryAddressMD5 = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'sDeliveryAddressMD5' );
        $sPaRes = \OxidEsales\Eshop\Core\Registry::getRequest()->getRequestEscapedParameter( 'PaRes' );
        $sRedirectDataId = \OxidEsales\Eshop\Core\UtilsObject::getInstance()->generateUId();
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb( dre_Ipayment::getFetchMode() );
        $sQuery = 'INSERT INTO dre_ipayment_redirectdata (oxid,aztimestamp,azredirectdata)'
        	. ' VALUES(' . $oDb->quote( $sRedirectDataId ) . ','
        	. $oDb->quote( strftime( '%Y-%m-%d %H:%M:%S', \OxidEsales\Eshop\Core\Registry::getUtils()->getTime() ) ) . ','
        	. $oDb->quote( serialize( array( 'MD' => $sMd, 'PaRes' => $sPaRes ) ) ) . ')';
        $oDb->execute($sQuery);
        
        // redirect to process 3d secure result (restore session if necessary):
        $sOrderSessionId = $_GET['force_sid'] ? $_GET['force_sid'] : $_GET['sid'];
        
        $sRedirectUrl = str_replace( '&amp;', '&', $oConfig->getShopSecureHomeUrl() );
        if ( $_GET['az_ipayment_3dsecure_iframe'] ) {
            // break out of the iframe before executing the 3D-Secure order:
            $sRedirectUrl .= 'cl=dre_Ipayment_Redirect3dSecure&fnc=azLeave3dSecureIframe';
        }
        else {
            // execute the 3D-Secure order:
            $sRedirectUrl .= 'cl=order&fnc=azExecute3dSecure';
        }
        // append redirect data id to url:
        $sRedirectUrl .= '&az_ipayment_redirect_id=' . $sRedirectDataId;

		// append MD5 Hashed delivery address for compatibility with oxid >= 4.6.1
        $sRedirectUrl .= '&sDeliveryAddressMD5=' . $sDeliveryAddressMD5;

        
        // try to reactivate session if it has been lost:
        if ( $sOrderSessionId && $sOrderSessionId !== \OxidEsales\Eshop\Core\Registry::getSession()->getId() ) {
            $sRedirectUrl = str_replace(\OxidEsales\Eshop\Core\Registry::getSession()->getId(), $sOrderSessionId, $sRedirectUrl );
        }
        // %%PRO_ONLY_BEGIN%% ####################################################################################################
        
        // write log:
        if(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable('az_ipayment_blDataIframe')) {
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable('sFcIpaymentUserIsOnIframe');
            \OxidEsales\Eshop\Core\Registry::getSession()->deleteVariable('az_ipayment_blDataIframe');

            $sLogMsg = "Returned from iframe-data-entry\n";
            foreach ($_GET as $sKey => $sValue) {
                $sLogMsg .= "\n" . $sKey . ': ' . $sValue;
            }
            $sLogMsg = dre_Ipayment::convertCharset( $sLogMsg, 'ISO-8859-1', null );
            $sTransId = '';
            if(array_key_exists('ret_trx_number', $_GET) !== false) {
                $sTransId = $_GET['ret_trx_number'];
            }

            if(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'sess_challenge' )) {
                $oOrder = oxNew(\OxidEsales\Eshop\Application\Model\Order::class);
                $oOrder->load(\OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'sess_challenge' ) );

                $oOrder->dreHandleIframeReturn();
                \OxidEsales\Eshop\Core\Registry::getSession()->setVariable( 'az_ipayment_sIframeOrderId', $oOrder->getId() );
            }
            dre_Ipayment_Log::log( $oOrder->getTotalOrderSum(), $oOrder, $sTransId, null, 0, $sLogMsg, dre_Ipayment_Log::SITUATION_IFRAME_RETURN );
        } else {
            $sOrderId = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable( 'az_ipayment_sRedirectedOrder' );
            $oOrder = oxNew(\OxidEsales\Eshop\Application\Model\Order::class);
            $oOrder->load( $sOrderId );
            $sLogMsg = 'Returned from 3D-Secure bank website';
            if (\OxidEsales\Eshop\Core\Registry::getConfig()->getShopConfVar( 'az_ipayment_blLogParams' ) ) {
                $sLogMsg .= "\norder-id: $sOrderId\naz_ipayment_3dsecure_return: $i3dSecureReturn\nredirect data id: $sRedirectDataId\nredirecting to url: $sRedirectUrl";
            }
            dre_Ipayment_Log::log( $oOrder->getTotalOrderSum(), $oOrder, null, null, 0, $sLogMsg, dre_Ipayment_Log::SITUATION_3DSECURE );
        }
        // %%PRO_ONLY_END%% ######################################################################################################

        // redirect:
        \OxidEsales\Eshop\Core\Registry::getUtils()->redirect( $sRedirectUrl, false, 302 );
        exit( 0 );
    }
}
