<?php

namespace Bender\dre_Ipayment\Core;

use Bender\dre_Ipayment\Application\Controller\Admin\dre_Ipayment_Dyn_Main;
use SoapClient;
use Exception;
use Bender\dre_Ipayment\Application\Model\dre_Ipayment;

class dre_Ipayment_Soap {

    protected $_sWsdlUrl;
	protected $_oSoapClient;

	/**
	 * Connects the SoapClient to the ipayment webservice.
	 *
	 * @return null
	 */
	protected function _connect ()
	{
		try
        {
			if ( !class_exists( 'SoapClient' ) )
        		throw new Exception( 'SOAP extension is not available (SoapClient missing)' );
        	
        	// prevent warnings during SoapClient creation:
        	$iOldErrorReporting = ini_get( 'error_reporting' );
        	error_reporting( 1 );

            $blTrace = false;
            if(\OxidEsales\Eshop\Core\Registry::getConfig()->getShopConfVar('az_ipayment_blLogSoapExceptions')) {
                $blTrace = true;
            }
            $this->_oSoapClient = new dre_Ipayment_SoapClient(
                $this->getWsdlUrl(),
                array(
                    'exceptions' => true, 
                    'trace' => $blTrace,
                    'encoding' => \OxidEsales\Eshop\Core\Registry::getLang()->translateString( 'charset' ),
                    'soap_version'=> SOAP_1_1,
                    'stream_context' => stream_context_create(
                        array(
                            'ssl' => array(
//                              'ciphers' => 'DHE-RSA-AES256-SHA:DHE-DSS-AES256-SHA:AES256-SHA:KRB5-DES-CBC3-MD5:KRB5-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:EDH-DSS-DES-CBC3-SHA:DES-CBC3-SHA:DES-CBC3-MD5:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA:AES128-SHA:RC2-CBC-MD5:KRB5-RC4-MD5:KRB5-RC4-SHA:RC4-SHA:RC4-MD5:RC4-MD5:KRB5-DES-CBC-MD5:KRB5-DES-CBC-SHA:EDH-RSA-DES-CBC-SHA:EDH-DSS-DES-CBC-SHA:DES-CBC-SHA:DES-CBC-MD5:EXP-KRB5-RC2-CBC-MD5:EXP-KRB5-DES-CBC-MD5:EXP-KRB5-RC2-CBC-SHA:EXP-KRB5-DES-CBC-SHA:EXP-EDH-RSA-DES-CBC-SHA:EXP-EDH-DSS-DES-CBC-SHA:EXP-DES-CBC-SHA:EXP-RC2-CBC-MD5:EXP-RC2-CBC-MD5:EXP-KRB5-RC4-MD5:EXP-KRB5-RC4-SHA:EXP-RC4-MD5:EXP-RC4-MD5',
                                'crypto_method' => STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT,
//                              'allow_self_signed' => false,
//                              'cafile' => getShopBasePath().'modules/az_ipayment/cacert.pem',
                            )
                        )
                    )
                )
            );
	
            error_reporting( $iOldErrorReporting );
            
            if ( !is_object( $this->_oSoapClient ) )
            {
                $oException = new \OxidEsales\Eshop\Core\Exception\ConnectionException( 'SOAP client is null after creation' );
                $oException->setAddress( $this->getWsdlUrl() );
                throw $oException;
            }
        }catch ( Exception $e ){
            $oException = new \OxidEsales\Eshop\Core\Exception\ConnectionException( 'Could not create ipayment SOAP client' );
            $oException->setAdress( $this->getWsdlUrl() );
            $oException->setConnectionError( $e->getMessage() );
            
            $this->_oSoapClient = null;
            
            throw $oException;
        }
	}

	/**
	 * Checks whether the soap client is connected.
	 * 
	 * @return boolean true if the soap client is connected, false if not connected
	 */
    public function isConnected (){
        if ( $this->_oSoapClient )
            return true;
        else
            return false;
    }

	/**
	 * Sets the ipayment webservice WSDL URL and checks the connection.
	 * Note: if you supply an invalid URL, you will get a fatal error and no exception (this is
	 * the way SoapClient seems to handle some errors even when set to use exceptions...).
	 *
	 * @param string $sWsdlUrl the ipayment webservice's WSDL URL
	 */
    public function setWsdlUrl ( $sWsdlUrl = null ){
        if ( $sWsdlUrl )
            $this->_sWsdlUrl = $sWsdlUrl;
        else
        	$this->_sWsdlUrl = 'https://ipayment.de/service/3.0/?wsdl';

        $this->_connect();
    }

    /**
     * Returns the ipayment webservice WSDL URL used by this connection.
     *
     * @return string the ipayment webservice WSDL URL
     */
    public function getWsdlUrl ()
    {
        return $this->_sWsdlUrl;
    }

    /**
     * Calls an ipayment webservice function.
     * 
     * @param string $sFunction the webservice function to call
     * @param array $aParams parameters for the webservice call (in the order in
     *   which they are expected by the webservice function)
     * 
     * @return mixed the result of the webservice function call
     */
    public function call ( $sFunction, $aParams = array() ){
        if ( !is_string($sFunction) || strlen($sFunction) < 1 ) {
            throw new \OxidEsales\Eshop\Core\Exception\ConnectionException( "ipayment SOAP: empty function" );
        }
        
        try {
            $mResult = $this->_oSoapClient->__soapCall( $sFunction, $aParams );
        }
        catch ( Exception $e ) {
            $oException = new \OxidEsales\Eshop\Core\Exception\ConnectionException( "ipayment SOAP call failed (function: $sFunction)" );
            $oException->setAdress( $this->getWsdlUrl() );
            $oException->setConnectionError( $e->getMessage() );
            throw $oException;
        }
        return $mResult;
    }

    /**
     * Returns the webservice action/function to call for payment authorization.
     * 
     * @param string $sAuthorizationMethod desired authorization method
     * @param string $sPaymentType ipayment payment type ('cc' or 'elv')
     * 
     * @return string the webservice action/function to call for payment authorization
     */
    public function getAuthorizationMethod ( $sAuthorizationMethod, $sPaymentType )
    {
        return $sAuthorizationMethod;
    }

    /**
     * Mixes relevant variables into the parameter list for a webservice call.
     * Currently, the only variable that is mixed into the parameter list is 'fromDatastorageId'.
     *
     * @param array $aParams parameter list for a webservice call
     * @param array $aVariables additional variables for a webservice call
     * 
     * @return array new parameter list for the webservice call
     */
    public function applyParamVariables ( $aParams, $aVariables )
    {
        if ( !is_array($aParams) || !is_array( $aVariables ) || empty( $aVariables ) )
            return $aParams;
        foreach ( $aVariables as $sKey => $mValue )
        {
            switch ( $sKey )
            {
                case 'fromDatastorageId':
                    if ( count($aParams) >= 2 && is_array( $aParams[1] ) )
                    {
                        $aParams[1]['storageData'] = array( 'fromDatastorageId' => $mValue );
                    }
                    break;
            }
        }
        
        return $aParams;
    }

    public function addClientParams ( $aParams ){
        if ( !is_array( $aParams ) )
            $aParams = array();
        while ( count( $aParams ) < 4 )
            $aParams[] = null;
        if ( !is_array( $aParams[ 3 ] ) )
            $aParams[ 3 ] = array();
        
        // parameters used by ipayment to identify the module (make sure they are included in the call):
        if ( !isset( $aParams[ 3 ][ 'otherOptions' ] ) || !is_array( $aParams[ 3 ][ 'otherOptions' ] ) )
            $aParams[ 3 ][ 'otherOptions' ] = array();
        $blFoundPpcbedc = false;
        foreach ( $aParams[ 3 ][ 'otherOptions' ] as $aOption )
        {
            if ( !is_array( $aOption ) || count( $aOption ) < 2 )
                continue;
            if ( $aOption[ 'key' ] == 'ppcbedc' )
            {
                $blFoundPpcbedc = true;
                break;
            }
        }
        if ( !$blFoundPpcbedc )
            $aParams[ 3 ][ 'otherOptions' ][] = array( 'key' => 'ppcbedc', 'value' => dre_Ipayment::getIpaymentPpcbedc() );
        
        if ( !isset( $aParams[ 3 ][ 'clientData' ] ) || !is_array( $aParams[ 3 ][ 'clientData' ] ) )
            $aParams[ 3 ][ 'clientData' ] = array();
        if ( !isset( $aParams[ 3 ][ 'clientData' ][ 'clientName' ] ) )
            $aParams[ 3 ][ 'clientData' ][ 'clientName' ] = dre_Ipayment::getClientName();
        if ( !isset( $aParams[ 3 ][ 'clientData' ][ 'clientVersion' ] ) )
            $aParams[ 3 ][ 'clientData' ][ 'clientVersion' ] = dre_Ipayment::getClientVersion();
        return $aParams;
    }

    /**
     * Returns the ipayment transaction number from a webservice call response.
     * 
     * @param mixed $oResponse a response from an ipayment webservice call
     * 
     * @return string the transaction number from the response
     */
    public function getTransactionNumberFromResponse ( $oResponse )
    {
        if ( isset( $oResponse->successDetails->retTrxNumber ) )
            return $oResponse->successDetails->retTrxNumber;
        return null;
    }
    
    
///////
    /**
     *  new by Andre
     * @param $oResponse
     * @return string the AuthCode from the response
     */
    public function getAuthCodeFromResponse ( $oResponse )
    {
        if ( isset( $oResponse->successDetails->retAuthCode ) )
            return $oResponse->successDetails->retAuthCode;
        // Testzwecke:
        #return 'TEST';
        return null;
    }
///////
    
    /**
     * Performs a 3D-Secure call for payment authentication and returns the result.
     * 
     * @param array $a3dSecure 3D-Secure parameters
     * 
     * @return mixed payment authentication result
     */
    public function getPaymentAuthenticationReturn ( $a3dSecure )
    {
    	return $this->_oSoapClient->paymentAuthenticationReturn( $a3dSecure );
    }
}
