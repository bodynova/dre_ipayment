<?php

namespace Bender\dre_Ipayment\Core;

use SoapClient;
use Exception;
use Bender\dre_Ipayment\Application\Model\dre_Ipayment_Log;

class dre_Ipayment_SoapClient extends SoapClient {

    public function __doRequest ($request, $location, $action, $version, $one_way = 0) {
        $sXml = parent::__doRequest($request, $location, $action, $version, $one_way);
        $sXml = preg_replace('/^(\x00\x00\xFE\xFF|\xFF\xFE\x00\x00|\xFE\xFF|\xFF\xFE|\xEF\xBB\xBF)/', "", $sXml);// remove BOM
        $sXml = preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', '', $sXml);// remove other XML-crashing unicode-characters
	    
	    return $sXml;
     
    }
    
    public function __soapCall ($function_name, array $arguments, array $options = null, $input_headers = null, array &$output_headers = null) {
        try {
            return parent::__soapCall($function_name, $arguments, $options, $input_headers, $output_headers);
        } catch (Exception $e) {
            if (\OxidEsales\Eshop\Core\Registry::getConfig()->getShopConfVar('az_ipayment_blLogSoapExceptions')) {
                $sMessage  = "Exception during SOAP call '".$function_name."' - {$e->getMessage()}\n";
                $sMessage .= "Request: ".str_replace(array("\r\n", "\r", "\n"), "", $this->__getLastRequest())."\n";
                $sMessage .= "Response: ".str_replace(array("\r\n", "\r", "\n"), "", $this->__getLastResponse());
                dre_Ipayment_Log::debugLog($sMessage);
            }
            throw $e;
        }
    }
    
}
