<?php

namespace Bender\dre_Ipayment\Core;

use OxidEsales\Eshop\Core\ConfigFile;
use OxidEsales\Eshop\Core\FileCache;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\DbMetaDataHandler;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\ExceptionHandler;
use Symfony\Component\Config\Definition\Exception\Exception;

class Events{
    /**
     * zusätzlich benötigte Tables
     */
    /*
    protected $arrTables = array(
        'dre_ipayment_logs',
        'dre_object2ipayment',
        'dre_ipayment_currencies',
        'dre_ipayment_redirectdata',
    );
    */
    /**
     * zusätzlich benötigte Felder
     */
    /*
    protected $arrFields = array(
        'oxorder' => 'DRE_3DCHECKUNAVAILABLE'
    );
    */

    /**
     * Bei Modul-Aktivierung
     */
    public static function onActivate(){

        $var = \OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam('az_ipayment_blActive');

        \OxidEsales\Eshop\Core\Registry::getLogger()->notice($var);
        $var = \OxidEsales\Eshop\Core\Registry::getConfig()->setConfigParam('az_ipayment_blActive', true);
        \OxidEsales\Eshop\Core\Registry::getLogger()->notice($var);
        return;
        //$oDb = DatabaseProvider::getDb();
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $results = null;
        $meta = oxNew( DbMetaDataHandler::class );
        //$exception = 0;
        //$exeptionHandler = oxNew( ExceptionHandler::class );
        //$dependencies = array();
        //$count = 0 ;


        $installdre_ipayment_logs = "CREATE TABLE IF NOT EXISTS `dre_ipayment_logs` (
`OXID` varchar(32) character set utf8 COLLATE utf8_general_ci NOT NULL default '',
`OXSHOPID` varchar(32) character set utf8 COLLATE utf8_general_ci NOT NULL default '',
`OXSESSID` varchar(32) character set utf8 COLLATE utf8_general_ci NOT NULL default '',
`OXTIME` timestamp NOT NULL,
`OXORDERID` varchar(32) character set utf8 COLLATE utf8_general_ci NOT NULL default '',
`OXTRANSID` varchar(32) NOT NULL default '',
`OXSTORAGEID` varchar(32) NOT NULL default '',
`OXPAYID` varchar(32) character set utf8 COLLATE utf8_general_ci NOT NULL default '',
`OXUID` varchar(32) character set utf8 COLLATE utf8_general_ci NOT NULL default '',
`OXAMOUNT` double NOT NULL default '0',
`OXCURR` varchar(32) NOT NULL default '',
`OXGWTYPE` varchar(32) NOT NULL default '',
`OXSITUATION` varchar(20) NOT NULL default '',
`OXERRCODE` varchar(32) NOT NULL default '',
`OXERRTEXT` text NOT NULL,
`OXREVISION` int NOT NULL default '0',
PRIMARY KEY (`OXID`)
) ENGINE = InnoDB;";

        //echo '<pre>';
        //print_r($meta->tableExists('dre_ipayment_logs'));
        //die();

        writeToLog($meta->tableExists('dre_ipayment_logs'));
        if($meta->tableExists('dre_ipayment_logs') <= 0){
            /*
            $updateOxPayments = "UPDATE `oxpayments` SET `OXACTIVE` = '0' WHERE `OXID` = 'oxidipayment';";
            $oDb->execute($updateOxPayments);
            */
            try {
                $results = $oDb->execute($installdre_ipayment_logs);
            }catch (Exception $e){
                writeToLog($e);
                //d($e);
            }
        }

        writeToLog($results);
        return $results;



        /*
        $installdre_object2ipayment = "CREATE TABLE IF NOT EXISTS `dre_object2ipayment` (
`OXID` varchar(32) character set utf8 COLLATE utf8_general_ci NOT NULL default '',
`OXPAYMENTID` varchar(32) character set utf8 COLLATE utf8_general_ci NOT NULL default '',
`OXSHOPID` varchar(32) character set utf8 COLLATE utf8_general_ci NOT NULL default '',
`OXTYPE` varchar(32) NOT NULL default '',
PRIMARY KEY (`OXID`)
) ENGINE = InnoDB;";

        if(!$meta->tableExists('dre_object2ipayment')){
            $updateOxPayments = "UPDATE `oxpayments` SET `OXACTIVE` = '0' WHERE `OXID` = 'oxidipayment';";
            $oDb->execute($updateOxPayments);
            $oDb->execute($installdre_object2ipayment);
        }

        $insert_elv_dre_object2ipayment = "INSERT IGNORE INTO `dre_object2ipayment` VALUES ('c8943789bdb2e1ba1.96380151', 'oxiddebitnote', '1', 'elv');";
        if($oDb->getOne("SELECT * FROM dre_object2ipayment where OXID = 'c8943789bdb2e1ba1.96380151'") == false){
            $oDb->execute($insert_elv_dre_object2ipayment);
        }

        $insert_cc_dre_object2ipayment  = "INSERT IGNORE INTO `dre_object2ipayment` VALUES ('c894378a7416eec74.89531379', 'oxidcreditcard', '1', 'cc');";
        if($oDb->getOne("SELECT * FROM dre_object2ipayment where OXID = 'c894378a7416eec74.89531379'") == false){
            $oDb->execute($insert_cc_dre_object2ipayment);
        }

        $alterTableOxOrder = "ALTER TABLE oxorder ADD COLUMN DRE_3DCHECKUNAVAILABLE TINYINT(1) DEFAULT '0' NOT NULL;";
        if(!$meta->fieldExists('DRE_3DCHECKUNAVAILABLE', 'oxorder')){
            $updateOxPayments = "UPDATE `oxpayments` SET `OXACTIVE` = '0' WHERE `OXID` = 'oxidipayment';";
            $oDb->execute($updateOxPayments);
            $oDb->execute($alterTableOxOrder);
        }

        $installdre_ipayment_redirectdata = "CREATE TABLE IF NOT EXISTS `dre_ipayment_redirectdata` (
  `OXID` VARCHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `AZTIMESTAMP` DATETIME NOT NULL,
  `AZREDIRECTDATA` MEDIUMBLOB NOT NULL,
  PRIMARY KEY ( `OXID` )
) ENGINE = InnoDB;";
        if(!$meta->tableExists('dre_ipayment_redirectdata')){
            $oDb->execute($installdre_ipayment_redirectdata);
        }


        if(!$meta->tableExists('dre_ipayment_currencies')){
            $installdre_ipayment_currencies = "CREATE TABLE IF NOT EXISTS `dre_ipayment_currencies` (
  `AZ_ISO3` varchar(3) NOT NULL,
  `AZ_DECIMALS` tinyint(4) NOT NULL default '2',
  PRIMARY KEY  (`AZ_ISO3`)
) ENGINE = InnoDB;";
            $oDb->execute($installdre_ipayment_currencies);


            $values_dre_ipayment_currencies = "INSERT INTO `dre_ipayment_currencies` (`AZ_ISO3`, `AZ_DECIMALS`) VALUES
('AED', 2),
('AFN', 2),
('ALL', 2),
('AMD', 2),
('ANG', 2),
('AOA', 2),
('ARS', 2),
('AUD', 2),
('AWG', 2),
('AZN', 2),
('BAM', 2),
('BBD', 2),
('BDT', 2),
('BGN', 2),
('BHD', 2),
('BIF', 2),
('BMD', 2),
('BND', 2),
('BOB', 2),
('BOV', 2),
('BRL', 2),
('BSD', 2),
('BTN', 2),
('BWP', 2),
('BYR', 2),
('BZD', 2),
('CAD', 2),
('CDF', 2),
('CHE', 2),
('CHF', 2),
('CHW', 2),
('CLF', 2),
('CLP', 2),
('CNY', 2),
('COP', 2),
('COU', 2),
('CRC', 2),
('CSD', 2),
('CUP', 2),
('CVE', 2),
('CYP', 2),
('CZK', 2),
('DJF', 2),
('DKK', 2),
('DOP', 2),
('DZD', 2),
('EEK', 2),
('EGP', 2),
('ERN', 2),
('ETB', 2),
('EUR', 2),
('FJD', 2),
('FKP', 2),
('GBP', 2),
('GEL', 2),
('GHC', 2),
('GIP', 2),
('GMD', 2),
('GNF', 2),
('GTQ', 2),
('GWP', 2),
('GYD', 2),
('HKD', 2),
('HNL', 2),
('HRK', 2),
('HTG', 2),
('HUF', 0),
('IDR', 0),
('ILS', 2),
('INR', 2),
('IQD', 2),
('IRR', 2),
('ISK', 2),
('JMD', 2),
('JOD', 2),
('JPY', 0),
('KES', 2),
('KGS', 2),
('KHR', 2),
('KMF', 2),
('KPW', 2),
('KRW', 0),
('KWD', 2),
('KYD', 2),
('KZT', 2),
('LAK', 2),
('LBP', 2),
('LKR', 2),
('LRD', 2),
('LSL', 2),
('LTL', 2),
('LVL', 2),
('LYD', 2),
('MAD', 2),
('MDL', 2),
('MGA', 2),
('MKD', 2),
('MLF', 2),
('MMK', 2),
('MNT', 2),
('MOP', 2),
('MRO', 2),
('MTL', 2),
('MUR', 2),
('MVR', 2),
('MWK', 2),
('MXN', 2),
('MXV', 2),
('MYR', 2),
('MZN', 2),
('NAD', 2),
('NGN', 2),
('NIO', 2),
('NOK', 2),
('NPR', 2),
('NZD', 2),
('OMR', 2),
('PAB', 2),
('PEN', 2),
('PGK', 2),
('PHP', 2),
('PKR', 2),
('PLN', 2),
('PYG', 2),
('QAR', 2),
('ROL', 2),
('RON', 2),
('RUB', 2),
('RWF', 2),
('SAR', 2),
('SBD', 2),
('SCR', 2),
('SDD', 2),
('SEK', 2),
('SGD', 2),
('SHP', 2),
('SKK', 2),
('SLL', 2),
('SOS', 2),
('SRD', 2),
('STD', 2),
('SVC', 2),
('SYP', 2),
('SZL', 2),
('THB', 2),
('TJS', 2),
('TMM', 2),
('TND', 2),
('TOP', 2),
('TRY', 2),
('TTD', 2),
('TWD', 2),
('TZS', 2),
('UAH', 2),
('UGX', 2),
('USD', 2),
('USN', 2),
('USS', 2),
('UYU', 2),
('UZS', 2),
('VEB', 2),
('VND', 2),
('VUV', 2),
('WST', 2),
('XAF', 2),
('XCD', 2),
('XDR', 2),
('XOF', 2),
('XPF', 2),
('YER', 2),
('ZAR', 2),
('ZMK', 2),
('ZWD', 2);";
            $oDb->execute("TRUNCATE TABLE `dre_ipayment_currencies`;");
            $oDb->execute($values_dre_ipayment_currencies);
        }
        */
        self::clearCache();
    }


    /**
     * Bei Modul-Deaktivierung
     */
    public static function onDeactivate(){
        /*
        $oDb = DatabaseProvider::getDb();
        $updateOxPayments = "UPDATE `oxpayments` SET `OXACTIVE` = '0' WHERE `OXID` = 'oxidipayment';";
        $oDb->execute($updateOxPayments);
        */

        self::clearCache();
    }

    /**
     * Löscht den TMP-Ordner sowie den Smarty-Ordner
     */
    protected static function clearCache()
    {
        /** @var FileCache $fileCache */
        $fileCache = oxNew(FileCache::class);
        $fileCache::clearCache();

        /** Smarty leeren */
        $tempDirectory = Registry::get(ConfigFile::class)->getVar("sCompileDir");
        $mask = $tempDirectory . "/smarty/*.php";
        $files = glob($mask);
        if (is_array($files)) {
            foreach ($files as $file) {
                if (is_file($file)) {
                    @unlink($file);
                }
            }
        }
    }
}