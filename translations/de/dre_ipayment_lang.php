<?php
$sLangName  = "Deutsch";
$iLangNr    = 0;

$aLang = array(
'charset'                                       => 'UTF-8',

'AZ_IPAYMENT_3DSECURE_NOT_SUPPORTED'       => 'Das 3D-Secure Verfahren (Verified by Visa, MasterCard SecureCode) wird nicht unterstützt',
'AZ_IPAYMENT_SELECT_PAYMENT'               => '%PAYMENTMETHOD% wählen',
'AZ_IPAYMENT_BACK_TO_SHOP'                 => 'Zurück zum Shop',
'AZ_IPAYMENT_INVALID_DATA'                 => 'Bitte prüfen und korrigieren Sie Ihre Daten.',
'AZ_IPAYMENT_BREAK_IFRAME'                 => 'Falls Sie nicht automatisch in einigen Sekunden weitergeleitet werden, dann klicken Sie bitte hier um den Bestellvorgang abzuschließen.',
'AZ_IPAYMENT_ERROR_CARD_TYPE_MISMATCH'     => 'Der angegebene Kartentyp passt nicht.',
'AZ_IPAYMENT_ERROR_INVALID_CARD_NUMBER'    => 'Die angegebene Kreditkartennummer ist fehlerhaft.',
'AZ_IPAYMENT_ERROR_INVALID_EXPIRY_DATE'    => 'Das angegebene Verfallsdatum der Kreditkarte ist fehlerhaft.',
'AZ_IPAYMENT_ERROR_INVALID_START_DATE'     => 'Das angegebene Startdatum der Kreditkarte ist fehlerhaft.',
'AZ_IPAYMENT_ERROR_INVALID_ISSUE_NUMBER'   => 'Die angegebene Issue-Nummer der Kreditkarte ist fehlerhaft.',
'AZ_IPAYMENT_ERROR_INVALID_SECURITY_CODE'  => 'Die angegebene Prüfnummer der Kreditkarte ist fehlerhaft oder leer.',
'AZ_IPAYMENT_ERROR_UNSUPPORTED_CARD_TYPE'  => 'Dieser Kreditkartentyp ist nicht erlaubt.',
'AZ_IPAYMENT_ERROR_INVALID_CARD_DATA'      => 'Die angegebenen Daten der Kreditkarte ist fehlerhaft.',
'AZ_IPAYMENT_ERROR_CARDHOLDER_MISSING'     => 'Der Karteninhaber ist nicht angegeben.',
'AZ_IPAYMENT_ERROR_COUNTRY_UNSUPPORTED'    => 'Das angegebene Land kann nicht für eine Zahlung verwendet werden.',
'AZ_IPAYMENT_ERROR_INVALID_BIC'            => 'Die BIC ist fehlerhaft.',
'AZ_IPAYMENT_ERROR_INVALID_IBAN'           => 'Die angegebene IBAN ist fehlerhaft.',
'AZ_IPAYMENT_ERROR_INVALID_ADDRESS'        => 'Die Addressdaten sind ungültig. Bitte überpruefen Sie Name, E-Mail und Addressinformationen.',
'AZ_IPAYMENT_ERROR_CARD_NEARLY_EXPIRED'    => 'Die Gültigkeit der Kreditkarte ist bald abgelaufen. Bitte benutzen Sie eine andere Kreditkarte.',
'AZ_IPAYMENT_ERROR_PAYMENT_REFUSED'        => 'Die Zahlung wurde abgelehnt. Bitte überprüfen Sie Ihre Adressdaten.',
'AZ_IPAYMENT_ERROR_MULTIPLE_FAILURES'      => 'Es wurde drei mal vergeblich versucht, die Zahlung abzuwickeln. Bitte versuchen Sie es in einigen Minuten nochmals.',
'AZ_IPAYMENT_ERROR_CARD_REFUSED'           => 'Die Kreditkarte wurde abgelehnt.',
'AZ_IPAYMENT_ERROR_CARD_REFUSED_CHECK'     => 'Die Kreditkarte wurde abgelehnt. Bitte prüfen Sie die Kartennummer und das Gültigkeitsdatum.',
'AZ_IPAYMENT_ERROR_INVALID_AMOUNT'         => 'Der angegebene Betrag ist fehlerhaft.',
'AZ_IPAYMENT_ERROR_INVALID_CURRENCY'       => 'Die angegebene Währung ist unbekannt.',
'AZ_IPAYMENT_ERROR_INVALID_ACCOUNTNR'      => 'Die angegebene Kontonummer ist fehlerhaft.',
'AZ_IPAYMENT_ERROR_INVALID_BANKCODE'       => 'Die angegebene Bankleitzahl ist fehlerhaft.',
    
'AZ_IPAYMENT_ERROR_AMOUNT_MISMATCH'        => 'Die angeforderte und die tatsächliche Bestellmenge passten nicht zusammen. Bitte versuchen Sie es noch mal.',

'PAYMENT_ACCOUNTNUMBER'                    => 'IBAN:',
'PAYMENT_ROUTINGNUMBER'                    => 'BIC:',
'PAGE_CHECKOUT_PAYMENT_ACCOUNTNUMBER'      => 'IBAN:',
'PAGE_CHECKOUT_PAYMENT_ROUTINGNUMBER'      => 'BIC:',
'BANK_ACCOUNT_NUMBER'                      => 'IBAN',
'BANK_ACCOUNT_NUMBER_2'                    => 'IBAN:',
'BANK_CODE'                                => 'BIC',
'BANK_CODE_2'                              => 'BIC:',
'BANK_CODE_3'                              => 'BIC:',

);
