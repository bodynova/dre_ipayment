<?php
$sLangName  = "Français";
$iLangNr    = 2;

$aLang = array(
'charset'                                       => 'UTF-8',

'AZ_IPAYMENT_3DSECURE_NOT_SUPPORTED'       => 'L\'utilisation du procédé 3D-Secure (vérifié par Visa, MasterCard SecureCode) n\'est pas autorisée',
'AZ_IPAYMENT_SELECT_PAYMENT'               => 'Choose %PAYMENTMETHOD%',
'AZ_IPAYMENT_BACK_TO_SHOP'                 => 'Retour au shop',
'AZ_IPAYMENT_INVALID_DATA'                 => 'Vérifiez et corrigez vos données svp !',
'AZ_IPAYMENT_BREAK_IFRAME'                 => 'If you are not automatically redirected within a few seconds then please click here to complete your order.',
'AZ_IPAYMENT_ERROR_CARD_TYPE_MISMATCH'     => 'Ce type de carte n\'est pas accepté.',
'AZ_IPAYMENT_ERROR_INVALID_CARD_NUMBER'    => 'Le numéro de la carte de crédit comporte une erreur.',
'AZ_IPAYMENT_ERROR_INVALID_EXPIRY_DATE'    => 'La date d\'expiration donnée comporte une erreur.',
'AZ_IPAYMENT_ERROR_INVALID_START_DATE'     => 'La date d\'émission de la carte de crédit comporte une erreur.',
'AZ_IPAYMENT_ERROR_INVALID_ISSUE_NUMBER'   => 'Le numéro d\'émission de la carte de crédit comporte une erreur.',
'AZ_IPAYMENT_ERROR_INVALID_SECURITY_CODE'  => 'Le cryptogramme de la carte de crédit comporte une erreur ou le champ n\'a pas été rempli.',
'AZ_IPAYMENT_ERROR_UNSUPPORTED_CARD_TYPE'  => 'Ce type de carte de crédit n\'est pas autorisé.',
'AZ_IPAYMENT_ERROR_INVALID_CARD_DATA'      => 'Les données de la carte de crédit comportent une erreur.',
'AZ_IPAYMENT_ERROR_CARDHOLDER_MISSING'     => 'Il manque le nom du titulaire de la carte de crédit.',
'AZ_IPAYMENT_ERROR_COUNTRY_UNSUPPORTED'    => 'Le pays entré ne peut pas être utilisé pour une transaction bancaire.',
'AZ_IPAYMENT_ERROR_INVALID_BIC'            => 'Le code BIC comporte une erreur.',
'AZ_IPAYMENT_ERROR_INVALID_IBAN'           => 'Le code IBAN comporte une erreur.',
'AZ_IPAYMENT_ERROR_INVALID_ADDRESS'        => 'L\'adresse n\'a pas été entrée correctement. Vérifiez svp le nom, l\'adresse e-mail et les informations concernant l\'adresse.',
'AZ_IPAYMENT_ERROR_CARD_NEARLY_EXPIRED'    => 'La validité de la carte de crédit utilisée expire bientôt. Utilisez une autre carte svp !',
'AZ_IPAYMENT_ERROR_PAYMENT_REFUSED'        => 'La paiement a été refusé. Vérifiez les données concernant votre adresse svp !',
'AZ_IPAYMENT_ERROR_MULTIPLE_FAILURES'      => 'Trois tentatives de régler le paiement ont échoué. Merci de réessayer dans quelques minutes.',
'AZ_IPAYMENT_ERROR_CARD_REFUSED'           => 'La carte de crédit a été refusée.',
'AZ_IPAYMENT_ERROR_CARD_REFUSED_CHECK'     => 'La carte de crédit a été refusée. Vérifiez le numéro de la carte et sa date de validité svp !',
'AZ_IPAYMENT_ERROR_INVALID_AMOUNT'         => 'Le montant entré comporte une erreur.',
'AZ_IPAYMENT_ERROR_INVALID_CURRENCY'       => 'La devise entrée n\'existe pas.',
'AZ_IPAYMENT_ERROR_INVALID_ACCOUNTNR'      => 'Le numéro de compte entré comporte une erreur.',
'AZ_IPAYMENT_ERROR_INVALID_BANKCODE'       => 'Le code de banque entré comporte une erreur.',

'AZ_IPAYMENT_ERROR_AMOUNT_MISMATCH'        => 'The requested and die actual order-sum did not match. Please try again.',

'PAYMENT_ACCOUNTNUMBER'                    => 'IBAN:',
'PAYMENT_ROUTINGNUMBER'                    => 'BIC:',
'PAGE_CHECKOUT_PAYMENT_ACCOUNTNUMBER'      => 'IBAN:',
'PAGE_CHECKOUT_PAYMENT_ROUTINGNUMBER'      => 'BIC:',
'BANK_ACCOUNT_NUMBER'                      => 'IBAN',
'BANK_ACCOUNT_NUMBER_2'                    => 'IBAN:',
'BANK_CODE'                                => 'BIC',
'BANK_CODE_2'                              => 'BIC:',
'BANK_CODE_3'                              => 'BIC:',

);
